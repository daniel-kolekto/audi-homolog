@isTest
public class AccountBuild {
	
	public static final String DEALER_DEFAULT_NAME = 'Audi Center Pinheiros';
	public static final Id DEALER_DEFAULT_SALESMANAGER = getDefaultSalesManager();
	public static final String DEALER_DEFAULT_CODE = '9052';
	
	
	public static Account createDealer(String dealerName, Id managerId, String dealerCode){
		return getNewDealer(dealerName, managerId, dealerCode);
	}
	
	public static Account createDealer(String dealerName, String dealerCode){
		return getNewDealer(dealerName, DEALER_DEFAULT_SALESMANAGER, dealerCode);
	}
	
	public static Account createDealer(Id managerId){
		return getNewDealer(DEALER_DEFAULT_NAME, managerId, DEALER_DEFAULT_CODE);
	}
	
	public static Account createDealer(String dealerCode){
		return getNewDealer(DEALER_DEFAULT_NAME, DEALER_DEFAULT_SALESMANAGER, dealerCode);
	}
	
	public static Account createDealer(){
		return getNewDealer(DEALER_DEFAULT_NAME, DEALER_DEFAULT_SALESMANAGER, DEALER_DEFAULT_CODE);
	}
	
	
	public static Account getNewDealer(String dealerName, Id managerId, String dealerCode){
        return new Account(
            RecordTypeId = Utils.getRecordTypeId('Account', 'Dealer'),
            IsDealerActive__c = true,
            Name = dealerName,
            CpfCnpj__c = Utils.generateCnpj(),
            DealerCode__c = dealerCode,
            DealerSalesManager__c = managerId,
            DealerAfterSalesManager__c = managerId,
            AudiSalesManager__c = managerId,
            AudiAfterSalesManager__c = managerId,
            Phone = '11'+Utils.randomNumberAsString(8),
            BillingLatitude = -23.501187000000000,
            BillingLongitude = -23.501187000000000,
            BillingCity = 'Barueri',
            BillingState = 'SP',
            BillingStreet = 'Alameda Araguaia, 2011',
            BillingPostalCode = '06455-000'
        );
	}
	
	public static Id getDefaultSalesManager(){
		User salesManager = UserBuild.createUser();
		
        try {
            Database.insert(salesManager);
        } catch(Exception e) {
            System.debug('Exception on creating User AccountBuild.getDefaultSalesManager() - ' + e.getMessage());
            return null;
        }
		
		return salesManager.Id;
	}
	
	
	
	
	
	public static String CUSTOMER_DEFAULT_COMPANYNAME;
	public static String CUSTOMER_DEFAULT_FIRSTNAME;
	public static String CUSTOMER_DEFAULT_LASTNAME;
	public static String USER_DEFAULT_EMAIL;
	
	private static void setRandomCustomer(){
		String randomNumber = Utils.randomNumberAsString(5);
		CUSTOMER_DEFAULT_COMPANYNAME = 'Company'+randomNumber;
		CUSTOMER_DEFAULT_FIRSTNAME = 'Customer';
		CUSTOMER_DEFAULT_LASTNAME = 'Teste' + randomNumber;
		USER_DEFAULT_EMAIL = 'customer'+randomNumber+'@audi.com';
	}
	
	
	public static Account createCustomer(String rt){
        if('PJ'.equalsIgnoreCase(rt)) {
            return getNewCustomerPJ(Utils.generateCnpj(), 'Legal person', 'ActiveClient');
        } else {
            return getNewCustomerPF(Utils.generateCpf(), 'Natural person', 'ActiveClient');
        }
	}
	
	public static Account createCustomer(String rt, String cpfCnpj){
        if('PJ'.equalsIgnoreCase(rt)) {
            return getNewCustomerPJ(cpfCnpj, 'Legal person', 'ActiveClient');
        } else {
            return getNewCustomerPF(cpfCnpj, 'Natural person', 'ActiveClient');
        }
	}
	
	public static Account createCustomer(String rt, String cpfCnpj, String cType, String accType){
        if('PJ'.equalsIgnoreCase(rt)) {
            return getNewCustomerPJ(cpfCnpj, cType, accType);
        } else {
            return getNewCustomerPF(cpfCnpj, cType, accType);
        }
	}
	
	
	public static Account getNewCustomerPF(String cpfCnpj, String cType, String accType){
		AccountBuild.setRandomCustomer();
		return new Account(
			RecordTypeId = Utils.getRecordTypeId('Account', 'NaturalPerson'),
            FirstName = CUSTOMER_DEFAULT_FIRSTNAME,
            LastName = CUSTOMER_DEFAULT_LASTNAME,
            CpfCnpj__c = cpfCnpj,
            CustomerType__c = cType,
            Type = accType,
            PersonMobilePhone = '119'+Utils.randomNumberAsString(8),
            Phone = '11'+Utils.randomNumberAsString(8),
            PersonEmail = USER_DEFAULT_EMAIL,
            PersonBirthdate = Date.newInstance(1990, 5, 8),
            Events__c = true,
            Campaigns__c = true,
            BillingCity = 'SP',
            BillingCountry = 'Brasil',
            BillingPostalCode = '05424010',
            BillingState = 'SP',
            BillingStreet = 'Rua Paes Leme, 136'
		);
	}
	
	public static Account getNewCustomerPJ(String cpfCnpj, String cType, String accType){
        return new Account(
            RecordTypeId = Utils.getRecordTypeId('Account', 'LegalPerson'),
            Name = CUSTOMER_DEFAULT_COMPANYNAME,
            CpfCnpj__c = cpfCnpj,
            CustomerType__c = cType,
            Type = accType,
            Phone = '11'+Utils.randomNumberAsString(8),
            Events__c = true,
            Campaigns__c = true,
            BillingCity = 'SP',
            BillingCountry = 'Brasil',
            BillingPostalCode = '05424010',
            BillingState = 'SP',
            BillingStreet = 'Rua Paes Leme, 136'//,
            //Contacts = new List<Contact>{new Contact(FirstName = CUSTOMER_DEFAULT_FIRSTNAME, LastName = CUSTOMER_DEFAULT_LASTNAME)}
        );
	}
    
}