public class AccountDAO {
	
	/* Maps */
	
	public static Map<String, Account> getDealerMapByCode(Set<String> dealerCodeSet){
		Map<String, Account> mapDealer = new Map<String, Account>();
		
		for(Account acct : getDealerByCode(dealerCodeSet)){
            if(String.isNotEmpty(acct.DealerCode__c))
                mapDealer.put(acct.DealerCode__c, acct);
		}
		
		return mapDealer;
	}
	
	public static Map<String, Account> getAccountMapByCpfCnpj(Set<String> cpfCnpjSet){
		Map<String, Account> mapCustomer = new Map<String, Account>();
		
		for(Account acct : getAccountByCpfCnpj(cpfCnpjSet, false)){
            if(String.isNotEmpty(acct.CpfCnpj__c))
                mapCustomer.put(acct.CpfCnpj__c, acct);
		}
		
		return mapCustomer;
	}
	
	public static Map<Id, Account> getAccountMapById(Set<Id> acctIdSet){
		return new Map<Id, Account>(getAccountById(acctIdSet, false));
	}
	
	
	/* Lists */
	
	public static List<Account> getDealerByCode(Set<String> dealerCodeSet){
		return [
			SELECT 	CpfCnpj__c,
					DealerCode__c,
					LeadRouting__c,
					VerifyAccount__c,
					DealerSalesManager__c,
					DealerAfterSalesManager__c,
					AudiSalesManager__c,
					AudiAfterSalesManager__c,
					DealerSalesManager__r.Email,
					DealerAfterSalesManager__r.Email
			
			FROM 	Account
			WHERE 	DealerCode__c in :dealerCodeSet
		];
	}
	
	public static List<Account> getDealerById(Set<Id> dealerIdSet){
		return [
			SELECT 	CpfCnpj__c,
					DealerCode__c,
					LeadRouting__c,
					VerifyAccount__c,
					DealerSalesManager__c,
					DealerAfterSalesManager__c,
					AudiSalesManager__c,
					AudiAfterSalesManager__c,
					DealerSalesManager__r.Email,
					DealerAfterSalesManager__r.Email
			
			FROM 	Account
			WHERE 	Id in :dealerIdSet
		];
	}
	
	public static List<Account> getAccountByIdOrCpfCnpj(Set<Id> acctIdSet, Set<String> cpfCnpjSet, Boolean allFields){
        if( (acctIdSet == null || acctIdSet.isEmpty()) && (cpfCnpjSet == null || cpfCnpjSet.isEmpty()) ) return new List<Account>();
        
        if(allFields) {
            String ids = Utils.buildIdSetAsStringForQuery(acctIdSet);
            String cpfCnpjCondition = Utils.buildStringSetAsStringForQuery(cpfCnpjSet);
            
            if(String.isNotEmpty(ids) && String.isNotEmpty(cpfCnpjCondition)) {
                return getAllFields('WHERE Id in ' + ids + ' OR CpfCnpj__c in ' + cpfCnpjCondition);
                
            } else if(String.isNotEmpty(ids) && String.isEmpty(cpfCnpjCondition)) {
                return getAllFields('WHERE Id in ' + ids);
                
            } else if(String.isEmpty(ids) && String.isNotEmpty(cpfCnpjCondition)) {
                return getAllFields('WHERE CpfCnpj__c in ' + cpfCnpjCondition);
            }
        }
        
        return [
            SELECT 	FirstName,
		            LastName,
		            Name,
		            RecordTypeId,
		            RecordType.Name,
		            CpfCnpj__c,
		            PersonMobilePhone,
		            PersonContactId,
		            BillingStreet,
		            BillingCity,
		            BillingState,
		            BillingPostalCode,
		            DealerCode__c,
		            QueueId__c,
		            LeadRouting__c,
		            VerifyAccount__c,
		            DealerSalesManager__c,
		            DealerAfterSalesManager__c,
		            AudiSalesManager__c,
		            AudiAfterSalesManager__c,
		            DealerSalesManager__r.Email,
		            DealerAfterSalesManager__r.Email,
		            (SELECT Email, MobilePhone, Phone FROM Contacts)
		            
            FROM 	Account
            WHERE 	Id in :acctIdSet OR CpfCnpj__c in :cpfCnpjSet
        ];
	}
	
	public static List<Account> getAccountByCpfCnpj(Set<String> cpfCnpjSet, Boolean allFields){
		return getAccountByIdOrCpfCnpj(new Set<Id>(), cpfCnpjSet, allFields);
	}
	
	public static List<Account> getAccountById(Set<Id> acctIdSet, Boolean allFields){
		return getAccountByIdOrCpfCnpj(acctIdSet, new Set<String>(), allFields);
	}
    
    public static List<Account> getDealerAccountForMap(){
    	return [SELECT id, BillingLatitude, BillingLongitude, Name, IsDealerActive__c,
    			BillingCity,BillingState,BillingStreet,Phone,BillingPostalCode,DealerCode__c
    			FROM Account
    			WHERE RecordType.DeveloperName = 'Dealer'
    			AND IsDealerActive__c = true
				AND BillingLatitude != null
				AND BillingLongitude != null
				AND BillingCity != null
				AND BillingState != null
				AND BillingStreet != null
				AND BillingPostalCode != null 
				AND DealerCode__c != null
    	];
    }
	
	
	/* Single Records */
	
	public static Account getDealerByCode(String dealerCode){
		if(String.isEmpty(dealerCode)) return null;
		List<Account> acctList = getDealerByCode(new Set<String>{dealerCode});
		try {
    		return acctList.get(0);
    	} catch(Exception e){
    		System.debug('AccountDAO.getDealerByCode - Could not retrieve Account: ' + e.getMessage());
    		return null;
    	}
	}
	
	public static Account getDealerById(Id dealerId){
		if(String.isEmpty(dealerId)) return null;
		List<Account> acctList = getDealerById(new Set<Id>{dealerId});
		try {
    		return acctList.get(0);
    	} catch(Exception e){
    		System.debug('AccountDAO.getDealerById - Could not retrieve Account: ' + e.getMessage());
    		return null;
    	}
	}
	
	public static Account getAccountByCpfCnpj(String cpfCnpj, Boolean allFields){
		if(String.isEmpty(cpfCnpj)) return null;
		List<Account> acctList = getAccountByCpfCnpj(new Set<String>{cpfCnpj}, allFields);
		try {
			return acctList.get(0);
		} catch(Exception e){
			System.debug('AccountDAO.getAccountByCpfCnpj - Could not retrieve Account: ' + e.getMessage());
			return null;
		}
	}
    
    public static Account getAccountById(Id accountId, Boolean allFields) {
		if(String.isEmpty(accountId)) return null;
    	List<Account> acctList = getAccountById(new Set<Id>{accountId}, allFields);
    	try {
    		return acctList.get(0);
    	} catch(Exception e){
    		System.debug('AccountDAO.getAccountById - Could not retrieve Account: ' + e.getMessage());
    		return null;
    	}
    }
    
    public static Account getPersonAccountAddress(String accountId) {
    	return getAccountById( (id) accountId, false );
    }
	
	
	/* Get all fields */
	
	public static List<Account> getAllFields(String whereCondition){
        Map<String, Schema.SObjectType> globalMap = Schema.getGlobalDescribe();
        Map<String, Schema.SObjectField> fieldMap = globalMap.get('Account').getDescribe().fields.getMap();
        
        String query = 'SELECT ';
        
        query += 'RecordType.Name,DealerSalesManager__r.Email,DealerAfterSalesManager__r.Email,';
        
        for(String fieldName : fieldMap.keySet())
            query += fieldName + ',';
        
        query = query.substring(0, query.length()-1) + ' FROM Account ' + whereCondition;
        
        System.debug('@@@ getAllFields query: ' + query);
        
        return Database.query(query);
    }
    
}