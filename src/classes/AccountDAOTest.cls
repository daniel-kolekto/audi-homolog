@isTest
private class AccountDAOTest {
	
	@testSetup static void createTestData(){
		Account dealerTest = AccountBuild.createDealer('9090');
		Account customer01 = AccountBuild.createCustomer('PF', '38477608903');
		Account customer02 = AccountBuild.createCustomer('PJ', '47285892000188');
		
		Database.insert(new List<Account>{dealerTest, customer01, customer02});
	}
	
	
	@isTest static void shouldQueryAccounts() {
		Test.startTest();
		
		Account dealerTest = AccountDAO.getDealerByCode('9090');
		Account customerPF = AccountDAO.getAccountByCpfCnpj('38477608903', true);
		Account customerPJ = AccountDAO.getAccountByCpfCnpj('47285892000188', true);
		
		System.assert(dealerTest != null);
		System.assert(customerPF != null);
		System.assert(customerPJ != null);
		
		AccountDAO.getDealerById(dealerTest.Id);
		AccountDAO.getAccountById(customerPF.Id, true);
		AccountDAO.getPersonAccountAddress(String.valueOf(customerPF.Id));
		AccountDAO.getAccountByIdOrCpfCnpj(new Set<Id>{customerPJ.Id}, new Set<String>{'47285892000188'}, true);
		AccountDAO.getDealerAccountForMap();
		
		AccountDAO.getDealerMapByCode(new Set<String>{'9090'});
		AccountDAO.getAccountMapByCpfCnpj(new Set<String>{'47285892000188', '38477608903'});
		AccountDAO.getAccountMapById(new Set<Id>{customerPF.Id, customerPJ.Id});
		
		Test.stopTest();
	}
	
}