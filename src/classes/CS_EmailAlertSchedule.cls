global class CS_EmailAlertSchedule implements Schedulable {
	
	public static List<EmailAlertQueue__c> queueList;
	
	
    global void execute(SchedulableContext sc) {
        CS_EmailAlertSchedule.executeSchedulable(sc.getTriggerId());
    }
    
    public static void executeSchedulable(Id jobId){
        queueList = new List<EmailAlertQueue__c>();
        EmailAlertQueue__c queue;
        
        try {
            queue = [
                SELECT Name, Case__c, Status__c, ErrorMessage__c, AlertAbout__c
                FROM EmailAlertQueue__c
                WHERE Name = :jobId
            ];
            
        } catch(Exception e) {
            System.debug('Could not retrieve Job information: ' + e.getMessage());
        }
        
        if(queue != null) {
            Case queueCase = CaseDAO.getCaseById(queue.Case__c, true);
            Boolean updateCase = false;
            
            System.debug('@@@ queue.AlertAbout__c: ' + queue.AlertAbout__c);
            System.debug('@@@ queueCase.SendCustomerCareEmailAlert__c: ' + queueCase.SendCustomerCareEmailAlert__c);
            
            try {
                if('CustomerCare'.equals(queue.AlertAbout__c) && queueCase.SendCustomerCareEmailAlert__c) {
                    queueCase.TriggerCustomerCareEmailAlert__c = true;
                    CS_EmailAlertSchedule.scheduleNextJob(queueCase.Id, CustomMetadataDAO.getMilestoneViolationEmailInterval('EmailIntervalCustomerCare'), 'CustomerCare');
                    updateCase = true;
                    
                } else if('Dealer'.equals(queue.AlertAbout__c) && queueCase.SendDealerEmailAlert__c) {
                    queueCase.TriggerDealerEmailAlert__c = true;
                    CS_EmailAlertSchedule.scheduleNextJob(queueCase.Id, CustomMetadataDAO.getMilestoneViolationEmailInterval('EmailIntervalDealer'), 'Dealer');
                    updateCase = true;
                }
                
                if(updateCase) Database.update( queueCase );
                
                queueList.add( new EmailAlertQueue__c(Id = queue.Id, Status__c = 'Processed') );
                
            } catch(Exception e) {
                String errorMsg = e.getMessage() + ' - line: ' + e.getLineNumber();
                System.debug('CS_EmailAlertSchedule exception: ' + errorMsg);
                queueList.add( new EmailAlertQueue__c(Id = queue.Id, Status__c = 'Failed', ErrorMessage__c = errorMsg) );
            }
            
            if(!queueList.isEmpty())
                Database.upsert(queueList);
        }
    }
	
    
    public static void scheduleCaseQueue(List<Case> triggerNew) {
        try {
            queueList = new List<EmailAlertQueue__c>();
            
            for(Case caseNew : triggerNew) {
                if(caseNew.ScheduleCustomerCareEmailAlert__c) {
                    scheduleNextJob(caseNew.Id, CustomMetadataDAO.getMilestoneViolationEmailInterval('EmailIntervalCustomerCare'), 'CustomerCare');
                    caseNew.SendCustomerCareEmailAlert__c = true;
                    caseNew.ScheduleCustomerCareEmailAlert__c = false;
                }
                
                if(caseNew.ScheduleDealerEmailAlert__c) {
                    scheduleNextJob(caseNew.Id, CustomMetadataDAO.getMilestoneViolationEmailInterval('EmailIntervalDealer'), 'Dealer');
                    caseNew.SendDealerEmailAlert__c = true;
                    caseNew.ScheduleDealerEmailAlert__c = false;
                }
            }
            
            if(!queueList.isEmpty())
                Database.insert(queueList);
            
        } catch(Exception e) {
            System.debug('scheduleCaseQueue exception: ' + e.getMessage() + ' - line: ' + e.getLineNumber());
        }
    }
    
    
    public static Id scheduleNextJob(Id caseId, Integer minutes, String recipient) {
        Id bhId = Utils.getDefaultBusinessHoursId();
        
        Datetime nextJobTime = BusinessHours.add( bhId, Datetime.now(), (minutes * 60 * 1000) );
        
        String minute = String.valueOf( nextJobTime.minute() );
        String hour = String.valueOf( nextJobTime.hour() );
        String day = String.valueOf( nextJobTime.day() );
        String month = String.valueOf( nextJobTime.month() );
        String year = String.valueOf( nextJobTime.year() );
        
        String jobDay = day.length() == 1 ? '0'+day : day;
        String jobMonth = month.length() == 1 ? '0'+month : month;
        String jobHour = hour.length() == 1 ? '0'+hour : hour;
        String jobMinute = minute.length() == 1 ? '0'+minute : minute;
        
        String jobName = 'Send email alert ' + jobDay+'/'+jobMonth+'/'+year+' - '+jobHour+':'+jobMinute;
        String scheduleTime = '0 '+minute+' '+hour+' '+day+' '+month+' ? '+year;
        
        System.debug('@@@ CS_EmailAlertSchedule next jobName: ' + jobName);
        System.debug('@@@ CS_EmailAlertSchedule next scheduleTime: ' + scheduleTime);
        
        Id jobId = System.schedule(jobName, scheduleTime, new SLACalculationBatch());
        queueList.add( new EmailAlertQueue__c(Name = jobId, Case__c = caseId, Status__c = 'Pending', AlertAbout__c = recipient) );
        
        return jobId;
    }
	
}