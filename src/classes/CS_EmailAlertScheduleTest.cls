@isTest
private class CS_EmailAlertScheduleTest {
	
	@testSetup static void createTestData(){
        Account dealerAcct01 = AccountBuild.createDealer('9055');
        Account dealerAcct02 = AccountBuild.createDealer('9066');
        Account customer01 = AccountBuild.createCustomer('PF');
        Database.insert(new List<Account>{dealerAcct01, dealerAcct02, customer01});
        
        Product2 vehicleProduct = ProductBuild.createVehicle('6FG9RT');
        Database.insert(vehicleProduct);
        
        Vehicle__c vehicle = VehicleBuild.createVehicle('1GTDC14N2FJ506808', 'EMT3194', vehicleProduct.Id);
        Database.insert(vehicle);
        
        Database.insert(new Ownership__c(Name = 'test', Vehicle__c = vehicle.Id, Customer__c = customer01.Id, Status__c = 'ACTIVE'));
        
        
        Case case01 = CaseBuild.createCustomerCareCase();
        case01.AccountId = customer01.Id;
        case01.Dealer__c = dealerAcct01.Id;
        case01.Vehicle__c = vehicle.Id;
        
        Database.insert(case01);
    }
    
    
    @isTest static void shouldScheduleCustomerCareEmailAlert() {
		List<Case> lstCase = [SELECT Id, CaseNumber FROM Case];
		Case caseTest = !lstCase.isEmpty() ? lstCase.get(0) : null;
		System.assert(caseTest != null);
		
		Test.startTest();
		
		caseTest.ScheduleCustomerCareEmailAlert__c = true;
		Database.update(caseTest);
		
		List<EmailAlertQueue__c> alertQueueList = [SELECT Name FROM EmailAlertQueue__c];
		System.assertEquals(1, alertQueueList.size());
		
		CS_EmailAlertSchedule.executeSchedulable(alertQueueList.get(0).Name);
		
		Test.stopTest();
		
		Case caseAssert = CaseDAO.getCaseById(caseTest.Id, true);
		System.assert(caseAssert != null);
		
		System.assert(caseAssert.SendCustomerCareEmailAlert__c);
	}
	
	@isTest static void shouldScheduleDealerEmailAlert() {
		List<Case> lstCase = [SELECT Id, CaseNumber FROM Case];
		Case caseTest = !lstCase.isEmpty() ? lstCase.get(0) : null;
		System.assert(caseTest != null);
		
		Test.startTest();
		
		caseTest.ScheduleDealerEmailAlert__c = true;
		Database.update(caseTest);
		
		List<EmailAlertQueue__c> alertQueueList = [SELECT Name FROM EmailAlertQueue__c];
		System.assertEquals(1, alertQueueList.size());
		
		CS_EmailAlertSchedule.executeSchedulable(alertQueueList.get(0).Name);
		
		Test.stopTest();
		
		Case caseAssert = CaseDAO.getCaseById(caseTest.Id, true);
		System.assert(caseAssert != null);
		
		System.assert(caseAssert.SendDealerEmailAlert__c);
	}
	
	/*@isTest static void shouldExecuteEmailAlertSchedule() {
		List<Case> lstCase = [SELECT Id, CaseNumber FROM Case];
		Case caseTest = !lstCase.isEmpty() ? lstCase.get(0) : null;
		System.assert(caseTest != null);
		
		Test.startTest();
		
		
		
		CS_EmailAlertSchedule
		
		Test.stopTest();
		
		Case caseAssert = CaseDAO.getCaseById(caseTest.Id, true);
		System.assert(caseAssert != null);
		
		System.assert(caseAssert.SendDealerEmailAlert__c);
	}*/
	
}