/**
* Project: Customer Service
*/
public with sharing class CS_ForwardCaseController {
    
	public Case caseObj {get;set;}
    public Account caseDealer {get;set;}
    public Task dealerTask {get;set;}
    public String taskUserId {
        get;
        set{
            taskUserId = value;
            if(this.dealerTask != null)
                this.dealerTask.OwnerId = value;
        }
    }
    public String forwardTo {get;set;}
    public String internalArea {get;set;}
    public String comment {get;set;}
    public Boolean isInternalArea { get { return 'internal'.equals(forwardTo); } }
    public Boolean isDealer { get { return 'dealer'.equals(forwardTo); } }
    public Boolean isEmptyDealer { get { return this.caseDealer == null; } }
    public String errorMsg {get;set;}
    public Map<String, InternalAreas__c> mapInternalAreas = InternalAreas__c.getAll();
    
    public List<SelectOption> internalAreasOptions {
        get {
            return CustomSettingsDAO.getInternalAreaOptions();
        }
    }
    
    public List<SelectOption> userOptions {
        get {
            List<SelectOption> optReturn = new List<SelectOption>();
            
            if(isEmptyDealer){
                optReturn.add(new SelectOption(UserInfo.getUserId(), UserInfo.getUserName()));
                return optReturn;
            }
            
            
            List<AccountContactRelation> relList = [
                SELECT AccountId, ContactId
                FROM AccountContactRelation
                WHERE AccountId = :this.caseDealer.Id
            ];
            //System.debug('@@@ relList: ' + JSON.serializePretty(relList));
            
            Set<Id> dealerContIdSet = new Set<Id>();
            for(AccountContactRelation acr : relList){
                dealerContIdSet.add(acr.ContactId);
            }
            //System.debug('@@@ dealerContIdSet: ' + dealerContIdSet);
            
            
            List<User> userList = [
                SELECT FirstName, LastName
                FROM User
                WHERE ContactId in :dealerContIdSet
                AND ProfileId = :Utils.getProfileId('Audi Dealer')
            ];
            //System.debug('@@@ userList: ' + JSON.serializePretty(userList));
            
            for(User us : userList){
                optReturn.add( new SelectOption(us.Id, (us.FirstName + ' ' + us.LastName)) );
            }
            
            return optReturn;
        }
    }
    
    
    public CS_ForwardCaseController(ApexPages.StandardController stdController) {
        this.caseObj = CaseDAO.getCaseById((Id)stdController.getId(), false);
        this.caseDealer = AccountDAO.getAccountById(caseObj.Dealer__c, false);
        this.dealerTask = initTask();
        
        if(String.isNotEmpty(caseObj.Subject) && String.isNotEmpty(caseObj.Description)){
            comment = 'Caso: ' + caseObj.CaseNumber + '\nAssunto: ' + caseObj.Subject + '\nDescrição: ' + caseObj.Description + 
                '\n\n[ ESCREVA AQUI INFORMAÇÕES ADICIONAIS ]';
        }
    }
    
    
    public Task initTask(){
        Task tsk = new Task();
        Id ownerId = caseDealer != null && caseDealer.DealerAfterSalesManager__c != null ? caseDealer.DealerAfterSalesManager__c : UserInfo.getUserId();
        
        this.taskUserId = ownerId;
        tsk.OwnerId = ownerId;
        tsk.Subject = 'Solicitação para a concessionária';
        tsk.Priority = 'Normal';
        tsk.Status = 'Open';
        tsk.WhatId = caseObj.Id;
        
        return tsk;
    }
    
    public PageReference forwardCase(){
        Savepoint sp = Database.setSavepoint();
        
        try {
            if(String.isEmpty(forwardTo)){
                throw new Utils.GenericException('Escolha uma opção para encaminhar o Caso');
            }
            
            if('dealer'.equals(forwardTo)) {
                if(isEmptyDealer)
                    throw new Utils.GenericException('Você só pode encaminhar após relacionar este Caso com uma concessionária');
                    
                if(!manualShareCaseRead(caseObj.Id, this.dealerTask.OwnerId) && !Test.isRunningTest())
                    throw new Utils.GenericException('Não foi possível compartilhar o Caso com o usuário escolhido');
                
                if(caseObj.NotPunctuation__c && String.isEmpty(caseObj.WhyNotPunctuation__c))
                    throw new Utils.GenericException('Para marcar um caso como não pontuação, você deve descrever o motivo');
                
                Database.insert(this.dealerTask);
                
                caseObj.TreatmentStatus__c = 'Waiting Dealer';
                caseObj.DealerUserForwarded__c = this.dealerTask.OwnerId;
                caseObj.EmailAlert01__c = [SELECT Email FROM User WHERE Id = :this.dealerTask.OwnerId].Email;
                caseObj.SendSlaDealerAlert__c = '1';
                Database.update(caseObj);
                
            } else if('internal'.equals(forwardTo)) {
                if(String.isEmpty(internalArea)) {
                    throw new Utils.GenericException('Escolha uma Área Interna para encaminhar o Caso');
                }
                if(String.isEmpty(comment)) {
                    throw new Utils.GenericException('Escreva um comentário para encaminhar a Área Interna');
                }
                
                FeedItem feedComment = new FeedItem(
                    ParentId = internalArea,
                    Body = comment
                );
                
                try {
                    Database.insert(feedComment);
                } catch(Exception e){
                    if(e.getMessage().contains('INSUFFICIENT_ACCESS_ON_CROSS_REFERENCE_ENTITY')) {
                        throw new Utils.GenericException('Você não tem permissão para escrever para esta Área Interna');
                    } else {
                        throw e;
                    }
                }
                
                caseObj.TreatmentStatus__c = 'Waiting Internal Area';
                Database.update(caseObj);
            }
            
            
        } catch(Exception e){
            System.debug('forwardCase Exception: ' + e.getMessage() + ' - Line: ' + e.getLineNumber());
            this.errorMsg = e.getMessage();
            Database.rollback(sp);
            return null;
        }
        
        return new PageReference('/' + caseObj.Id);
    }
    
    public PageReference cancel(){
        return new PageReference('/' + caseObj.Id);
    }
    
    
    public Boolean manualShareCaseRead(Id recordId, Id userOrGroupId) {
        CaseShare caseShr = new CaseShare(
            CaseId = recordId,
            UserOrGroupId = userOrGroupId,
            CaseAccessLevel = 'Read',
            RowCause = Schema.CaseShare.RowCause.Manual
        );
        
        // Insert the sharing record and capture the save result.
        // The false parameter allows for partial processing if multiple records passed
        // into the operation.
        Database.SaveResult sr = Database.insert(caseShr, false);
        
        // Process the save results.
        if(sr.isSuccess()) {
            // Indicates success
            return true;
        } else {
            // Get first save result error.
            Database.Error err = sr.getErrors()[0];
            System.debug('@@@ err: ' + err.getMessage());
            
            // Check if the error is related to trival access level.
            // Access level must be more permissive than the object's default.
            // These sharing records are not required and thus an insert exception is acceptable.
            if(err.getStatusCode() == StatusCode.FIELD_FILTER_VALIDATION_EXCEPTION  &&
            err.getMessage().contains('AccessLevel')) {
                // Indicates success.
                return true;
            } else {
                // Indicates failure.
                return false;
            }
        }
    }
    
}