@isTest
private class CS_ForwardCaseControllerTest {
	
	@testSetup static void createTestData(){
        System.runAs(new User(Id = UserInfo.getUserId())) {
            User analystUser = UserBuild.createAnalystCRA('54321');
            Database.insert(analystUser);
            
            try {
                Database.insert(new PermissionSetAssignment(
                    AssigneeId = analystUser.Id,
                    PermissionSetId = Utils.getPermissionSetId('CRA_Analyst')
                ));
            } catch(Exception e) {
            	System.debug('Could not assign permission set: ' + e.getMessage());
            }
            
            Database.insert(new List<GroupMember> {
                new GroupMember(
                    UserOrGroupId = analystUser.Id,
                    GroupId = Utils.getQueueId('AudiCustomerCare')
                ), new GroupMember(
                    UserOrGroupId = analystUser.Id,
                    GroupId = Utils.getQueueId('AudiAG')
                ), new GroupMember(
                    UserOrGroupId = analystUser.Id,
                    GroupId = Utils.getQueueId('ReclameAqui')
                )
            });
            
            
            CollaborationGroup chatterGroup = new CollaborationGroup(
                Name = 'Group Test Salabim Salaha Itabum Benibu',
                CollaborationType = 'Private'
            );
            Database.insert(chatterGroup);
            
            Database.insert(new CollaborationGroupMember(
                CollaborationGroupId = chatterGroup.Id,
                CollaborationRole = 'Standard',
                MemberId = analystUser.Id
            ));
            
            Database.insert(new InternalAreas__c(
                Name = 'Group Test',
                GroupId__c = chatterGroup.Id
            ));
        }
        
		
		Account dealerAcct01 = AccountBuild.createDealer('9055');
        Account dealerAcct02 = AccountBuild.createDealer('9066');
        Account customer01 = AccountBuild.createCustomer('PF');
        Database.insert(new List<Account>{dealerAcct01, dealerAcct02, customer01});
        
        Product2 vehicleProduct = ProductBuild.createVehicle('6FG9RT');
        Database.insert(vehicleProduct);
        
        Vehicle__c vehicle = VehicleBuild.createVehicle('1GTDC14N2FJ506808', 'EMT3194', vehicleProduct.Id);
        Database.insert(vehicle);
        
        Database.insert(new Ownership__c(Name = 'test', Vehicle__c = vehicle.Id, Customer__c = customer01.Id, Status__c = 'ACTIVE'));
        
        
        Case case01 = CaseBuild.createCustomerCareCase();
        case01.AccountId = customer01.Id;
        case01.Dealer__c = dealerAcct01.Id;
        case01.Vehicle__c = vehicle.Id;
        
        Case case02 = CaseBuild.createAudiAgCase();
        case02.AccountId = customer01.Id;
        case02.Dealer__c = dealerAcct01.Id;
        case02.Vehicle__c = vehicle.Id;
        
        Case case03 = CaseBuild.createReclameAquiCase();
        case03.AccountId = customer01.Id;
        case03.Dealer__c = dealerAcct01.Id;
        case03.Vehicle__c = vehicle.Id;
        
        Database.insert(new List<Case>{case01, case02, case03});
    }
	
	
	@isTest static void shouldStartPage() {
		User analystTest = [SELECT Id FROM User WHERE Username = 'usuario54321@audi.com.teste'];
		
		List<Case> lstCase = [
			SELECT Id
			FROM Case
			WHERE RecordTypeId = :Utils.getRecordTypeId('Case', 'CustomerCare')
		];
		Case caseTest = !lstCase.isEmpty() ? lstCase.get(0) : null;
		System.assert(caseTest != null);
		
		Test.startTest();
		System.runAs(analystTest){
			
            CS_ForwardCaseController cont = new CS_ForwardCaseController(new ApexPages.StandardController(caseTest));
            
            System.assert(cont.caseObj != null);
            //System.assert(cont.caseDealer != null);
            System.assert(cont.dealerTask != null);
            System.assert(String.isNotEmpty(cont.comment));
            
		}
		Test.stopTest();
	}
	
	@isTest static void shouldCancel() {
		User analystTest = [SELECT Id FROM User WHERE Username = 'usuario54321@audi.com.teste'];
		
		List<Case> lstCase = [
			SELECT Id
			FROM Case
			WHERE RecordTypeId = :Utils.getRecordTypeId('Case', 'CustomerCare')
		];
		Case caseTest = !lstCase.isEmpty() ? lstCase.get(0) : null;
		System.assert(caseTest != null);
		
		Test.startTest();
        System.runAs(analystTest) {
        	
            CS_ForwardCaseController cont = new CS_ForwardCaseController(new ApexPages.StandardController(caseTest));
            cont.cancel();
            
        }
		Test.stopTest();
	}
	
	@isTest static void shouldBuildInternalAreasOptions() {
		User analystTest = [SELECT Id FROM User WHERE Username = 'usuario54321@audi.com.teste'];
		
		List<Case> lstCase = [
			SELECT Id
			FROM Case
			WHERE RecordTypeId = :Utils.getRecordTypeId('Case', 'CustomerCare')
		];
		Case caseTest = !lstCase.isEmpty() ? lstCase.get(0) : null;
		System.assert(caseTest != null);
		
		Test.startTest();
        System.runAs(analystTest) {
        	
            CS_ForwardCaseController cont = new CS_ForwardCaseController(new ApexPages.StandardController(caseTest));
            
            System.assertEquals(cont.internalAreasOptions.size(), InternalAreas__c.getAll().values().size());
            System.assert(!cont.internalAreasOptions.isEmpty());
            
        }
		Test.stopTest();
	}
	
	@isTest static void shouldBuildUsersOptions() {
		User analystTest = [SELECT Id FROM User WHERE Username = 'usuario54321@audi.com.teste'];
		
		List<Case> lstCase = [
			SELECT Id
			FROM Case
			WHERE RecordTypeId = :Utils.getRecordTypeId('Case', 'CustomerCare')
		];
		Case caseTest = !lstCase.isEmpty() ? lstCase.get(0) : null;
		System.assert(caseTest != null);
		
		Test.startTest();
        System.runAs(analystTest) {
        	
            CS_ForwardCaseController cont = new CS_ForwardCaseController(new ApexPages.StandardController(caseTest));
            
            //System.assert(!cont.userOptions.isEmpty());
            //System.assertEquals(cont.userOptions.size(), 1);
            
        }
		Test.stopTest();
	}
	
	@isTest static void shouldValidateNoForwardOption() {
		User analystTest = [SELECT Id FROM User WHERE Username = 'usuario54321@audi.com.teste'];
		
		List<Case> lstCase = [
			SELECT Id
			FROM Case
			WHERE RecordTypeId = :Utils.getRecordTypeId('Case', 'CustomerCare')
		];
		Case caseTest = !lstCase.isEmpty() ? lstCase.get(0) : null;
		System.assert(caseTest != null);
		
		Test.startTest();
        System.runAs(analystTest) {
        	
            CS_ForwardCaseController cont = new CS_ForwardCaseController(new ApexPages.StandardController(caseTest));
            
            cont.forwardCase();
            
            System.assertEquals('Escolha uma opção para encaminhar o Caso', cont.errorMsg);
            
        }
		Test.stopTest();
	}
	
	/*@isTest static void shouldValidateDealerSharingException() {
		User analystTest = [SELECT Id FROM User WHERE Username = 'usuario54321@audi.com.teste'];
		
		List<Case> lstCase = [
			SELECT Id
			FROM Case
			WHERE RecordTypeId = :Utils.getRecordTypeId('Case', 'CustomerCare')
		];
		Case caseTest = !lstCase.isEmpty() ? lstCase.get(0) : null;
		System.assert(caseTest != null);
		
		Test.startTest();
        System.runAs(analystTest) {
        	
            CS_ForwardCaseController cont = new CS_ForwardCaseController(new ApexPages.StandardController(caseTest));
            
            cont.forwardTo = 'dealer';
            cont.dealerTask.OwnerId = null;
            cont.forwardCase();
            
            System.assertEquals('Não foi possível compartilhar o Caso com o usuário escolhido', cont.errorMsg);
            
        }
		Test.stopTest();
	}*/
	
	@isTest static void shouldValidateDealerWhyNotPunctuationRequired() {
        User analystTest = [SELECT Id FROM User WHERE Username = 'usuario54321@audi.com.teste'];
        
        List<Case> lstCase = [
            SELECT Id
            FROM Case
            WHERE RecordTypeId = :Utils.getRecordTypeId('Case', 'CustomerCare')
        ];
        Case caseTest = !lstCase.isEmpty() ? lstCase.get(0) : null;
        System.assert(caseTest != null);
        
        Test.startTest();
        
        CS_ForwardCaseController cont = new CS_ForwardCaseController(new ApexPages.StandardController(caseTest));
        
        cont.forwardTo = 'dealer';
        cont.caseObj.NotPunctuation__c = true;
        cont.forwardCase();
        
        System.assertEquals('Para marcar um caso como não pontuação, você deve descrever o motivo', cont.errorMsg);
        
        Test.stopTest();
        
	}
	
	@isTest static void shouldForwardDealerSuccess() {
		User analystTest = [SELECT Id FROM User WHERE Username = 'usuario54321@audi.com.teste'];
		
		List<Case> lstCase = [
			SELECT Id
			FROM Case
			WHERE RecordTypeId = :Utils.getRecordTypeId('Case', 'CustomerCare')
		];
		Case caseTest = !lstCase.isEmpty() ? lstCase.get(0) : null;
		System.assert(caseTest != null);
		
		Test.startTest();
		
        CS_ForwardCaseController cont = new CS_ForwardCaseController(new ApexPages.StandardController(caseTest));
        
        cont.forwardTo = 'dealer';
        cont.forwardCase();
        
        System.assertEquals(null, cont.errorMsg);
        System.assert(cont.dealerTask.Id != null);
        System.assertEquals('Waiting Dealer', cont.caseObj.TreatmentStatus__c);
        
        System.assert(cont.isDealer);
        
		Test.stopTest();
	}
	
	@isTest static void shouldValidateIANoInternalArea() {
		User analystTest = [SELECT Id FROM User WHERE Username = 'usuario54321@audi.com.teste'];
		
		List<Case> lstCase = [
			SELECT Id
			FROM Case
			WHERE RecordTypeId = :Utils.getRecordTypeId('Case', 'CustomerCare')
		];
		Case caseTest = !lstCase.isEmpty() ? lstCase.get(0) : null;
		System.assert(caseTest != null);
		
		Test.startTest();
        System.runAs(analystTest) {
        	
            CS_ForwardCaseController cont = new CS_ForwardCaseController(new ApexPages.StandardController(caseTest));
            
            cont.forwardTo = 'internal';
            cont.forwardCase();
            
            System.assertEquals('Escolha uma Área Interna para encaminhar o Caso', cont.errorMsg);
            
        }
		Test.stopTest();
	}
	
	@isTest static void shouldValidateIANoComment() {
		User analystTest = [SELECT Id FROM User WHERE Username = 'usuario54321@audi.com.teste'];
		
		List<Case> lstCase = [
			SELECT Id
			FROM Case
			WHERE RecordTypeId = :Utils.getRecordTypeId('Case', 'CustomerCare')
		];
		Case caseTest = !lstCase.isEmpty() ? lstCase.get(0) : null;
		System.assert(caseTest != null);
		
		Test.startTest();
        System.runAs(analystTest) {
        	
            CS_ForwardCaseController cont = new CS_ForwardCaseController(new ApexPages.StandardController(caseTest));
            
            cont.forwardTo = 'internal';
            System.assert(cont.internalAreasOptions != null);
            System.assert(!cont.internalAreasOptions.isEmpty());
            cont.internalArea = cont.internalAreasOptions.get(0).getValue();
            cont.comment = '';
            cont.forwardCase();
            
            System.assertEquals('Escreva um comentário para encaminhar a Área Interna', cont.errorMsg);
            
        }
		Test.stopTest();
	}
	
	@isTest static void shouldForwardIASuccess() {
		User analystTest = [SELECT Id FROM User WHERE Username = 'usuario54321@audi.com.teste'];
		
		List<Case> lstCase = [
			SELECT Id
			FROM Case
			WHERE RecordTypeId = :Utils.getRecordTypeId('Case', 'CustomerCare')
		];
		Case caseTest = !lstCase.isEmpty() ? lstCase.get(0) : null;
		System.assert(caseTest != null);
		
		Test.startTest();
        System.runAs(analystTest) {
        	
            CS_ForwardCaseController cont = new CS_ForwardCaseController(new ApexPages.StandardController(caseTest));
            
            cont.forwardTo = 'internal';
            System.assert(cont.internalAreasOptions != null);
            System.assert(!cont.internalAreasOptions.isEmpty());
            cont.internalArea = cont.internalAreasOptions.get(0).getValue();
            cont.forwardCase();
            
            System.assert(String.isEmpty(cont.errorMsg));
            System.assertEquals('Waiting Internal Area', cont.caseObj.TreatmentStatus__c);
            
            System.assert(cont.isInternalArea);
            
        }
		Test.stopTest();
	}
	
}