public class CaseBO {
    
    public static void setCaseContactAndSLA(List<Case> triggerNew){
        List<Account> lstCustomerUpdate = new List<Account>();
        Set<Id> customerDuplicateControl = new Set<Id>();
        
        // Contact Map
        Map<Id, Contact> customerContactMap = ContactDAO.getContactMapByCustomer(AccountDAO.getAccountByIdOrCpfCnpj(SetUtils.getCaseCustomerIdSet(triggerNew), SetUtils.getCaseCpfCnpjSet(triggerNew), false));
        
        for(Case cs : triggerNew){
            if(cs.ContactId == null && customerContactMap.containsKey(cs.AccountId))
                cs.ContactId = customerContactMap.get(cs.AccountId).Id;
            
            if(cs.AccountId != null && !customerDuplicateControl.contains(cs.AccountId)){
                lstCustomerUpdate.add(new Account(
                    Id = cs.AccountId,
                    Campaigns__c = cs.Campaigns__c,
                    Events__c = cs.Events__c
                ));
                customerDuplicateControl.add(cs.AccountId);
            }
            
            
            if(String.isEmpty(cs.TreatmentStatus__c))
                cs.TreatmentStatus__c = 'Reclame Aqui'.equalsIgnoreCase(cs.Origin) ? 'Waiting Reclame Aqui' : 'Waiting CRA';
                
            cs.EntitlementId = Utils.validateId(Label.EntitlementCustomerCare);
            
            if('Waiting Internal Area'.equalsIgnoreCase(cs.TreatmentStatus__c)){
                cs.ActualTreatmentStatus__c = 'Waiting Internal Area';
            } else if('Waiting Dealer'.equalsIgnoreCase(cs.TreatmentStatus__c)){
                cs.ActualTreatmentStatus__c = 'Waiting Dealer';
            } else if('Waiting CRA'.equalsIgnoreCase(cs.TreatmentStatus__c)){
                cs.ActualTreatmentStatus__c = 'Waiting CRA';
            } else if('Waiting Reclame Aqui'.equalsIgnoreCase(cs.TreatmentStatus__c)){
                cs.ActualTreatmentStatus__c = 'Waiting Reclame Aqui';
            }
        }
        
        if(!lstCustomerUpdate.isEmpty())
            Database.update(lstCustomerUpdate, false);
    }
    
    public static void calculateKundentischTime(List<Case> triggerNew, Map<Id, Case> oldMap){
        Id bhId = Utils.getBusinessHoursId('AudiCustomerCare');
        
        for(Case caseNew : triggerNew){
            Boolean isInsert = oldMap == null;
            Case caseOld = !isInsert ? oldMap.get(caseNew.Id) : null;
            
            if(( isInsert || (!isInsert && caseNew.Priority != caseOld.Priority) ) && 'Kundentisch'.equalsIgnoreCase(caseNew.Priority)){
                caseNew.KundentischDatetime__c = Datetime.now();
            }
            
            if(!isInsert && caseNew.Priority != caseOld.Priority && 'Kundentisch'.equalsIgnoreCase(caseOld.Priority)){
                if(caseNew.KundentischTotalTime__c == null) caseNew.KundentischTotalTime__c = 0;
                caseNew.KundentischTotalTime__c += BusinessHours.diff(bhId, caseNew.KundentischDatetime__c, Datetime.now()) / 1000 / 60;
            }
        }
    }
    
    public static void closeMilestones(List<Case> triggerNew, Map<Id, Case> oldMap){
        Set<Id> caseIdSet = new Set<Id>();
        Set<String> pausedStatus = new Set<String>{'Waiting Pieces', 'Waiting diagnosis', 'Waiting Repair'};
        
        for(Case caseNew : triggerNew){
            Case caseOld = oldMap.get(caseNew.Id);
            
            if(caseNew.TreatmentStatus__c != caseOld.TreatmentStatus__c || 'Closed'.equalsIgnoreCase(caseNew.Status)) {
                if(pausedStatus.contains(caseNew.TreatmentStatus__c)) { // case new is paused status
                    caseNew.IsStopped = true;
                    
                } else {
                    caseIdSet.add(caseNew.Id);
                    caseNew.ViolationMinutes__c = '> 60';
                    if(pausedStatus.contains(caseOld.TreatmentStatus__c)) // case old is paused status
                        caseNew.IsStopped = false;
                }
                
                if('Closed'.equalsIgnoreCase(caseNew.Status))
                    caseNew.TreatmentStatus__c = 'Ended';
            }
        }
        
        if(caseIdSet.isEmpty()) return;
        
        
        Map<Id, List<CaseMilestone>> mapMilestonesByCase = CaseMilestoneDAO.getCaseMilestonesMapByCaseId(caseIdSet);
        List<CaseMilestone> milestonesUpdateList = new List<CaseMilestone>();
        
        for(Case caseNew : triggerNew){
            if(!mapMilestonesByCase.containsKey(caseNew.Id)) continue;
            
            for(CaseMilestone cm : mapMilestonesByCase.get(caseNew.Id)){
                System.debug('@@@ cm.MilestoneType.Name: ' + cm.MilestoneType.Name);
                
                if( Label.SLACustomerCare.equals(cm.MilestoneType.Name) && 
                    !'Waiting Customer'.equalsIgnoreCase(caseNew.TreatmentStatus__c) && 
                    !'Waiting Customer Answer'.equalsIgnoreCase(caseNew.TreatmentStatus__c) ) continue;
                
                caseNew.SendCustomerCareEmailAlert__c = !Label.SLACustomerCare.equals(cm.MilestoneType.Name);
                caseNew.SendDealerEmailAlert__c = !Label.SLADealer.equals(cm.MilestoneType.Name);
                
                cm.CompletionDate = Datetime.now();
                milestonesUpdateList.add(cm);
            }
        }
        
        if(!milestonesUpdateList.isEmpty())
            Database.update(milestonesUpdateList);
    }
    
    public static void syncContactEmailsForAlert(List<Case> triggerNew, Map<Id, Case> oldMap){
        Map<Id, List<Contact>> mapContactsByAccount = ContactDAO.getContactsMapByAcctId(SetUtils.getCaseDealerIdSet(triggerNew));
        System.debug('@@@ mapContactsByAccount: ' + JSON.serializePretty(mapContactsByAccount));
        
        for(Case caseNew : triggerNew) {
            Case caseOld = oldMap.get(caseNew.Id);
            
            System.debug('@@@');
            
            if(caseNew.SendSlaDealerAlert__c != caseOld.SendSlaDealerAlert__c && String.isNotEmpty(caseNew.SendSlaDealerAlert__c) && mapContactsByAccount.containsKey(caseNew.Dealer__c)) {
                Set<String> emailSendSet = new Set<String>{caseNew.EmailAlert01__c}; // usuário marcado quando encaminhado
                
                List<Contact> caseContacts = mapContactsByAccount.get(caseNew.Dealer__c);
                System.debug('@@@ mapContactsByAccount.get: ' + caseContacts);
                
                if('1'.equalsIgnoreCase(caseNew.SendSlaDealerAlert__c)) {
                    emailSendSet.addAll( CaseUtils.filterContactEmails(caseContacts, '1') );
                    
                } else if('2'.equalsIgnoreCase(caseNew.SendSlaDealerAlert__c)) {
                    emailSendSet.addAll( CaseUtils.filterContactEmails(caseContacts, '2') );
                    
                } else if('3'.equalsIgnoreCase(caseNew.SendSlaDealerAlert__c)) {
                    emailSendSet.addAll( CaseUtils.filterContactEmails(caseContacts, '3') );
                }
                
                System.debug('@@@ emailSendSet: ' + JSON.serializePretty(emailSendSet));
                
                
                List<String> emailSendList = new List<String>();
                for(String email : emailSendSet){
                    emailSendList.add(email);
                }
                
                caseNew.EmailAlert01__c = emailSendList.size() >= 1 ? emailSendList.get(0) : null;
                caseNew.EmailAlert02__c = emailSendList.size() >= 2 ? emailSendList.get(1) : null;
                caseNew.EmailAlert03__c = emailSendList.size() >= 3 ? emailSendList.get(2) : null;
                caseNew.EmailAlert04__c = emailSendList.size() >= 4 ? emailSendList.get(3) : null;
                caseNew.EmailAlert05__c = emailSendList.size() >= 5 ? emailSendList.get(4) : null;
                caseNew.EmailAlert06__c = emailSendList.size() >= 6 ? emailSendList.get(5) : null;
                caseNew.EmailAlert07__c = emailSendList.size() >= 7 ? emailSendList.get(6) : null;
                caseNew.EmailAlert08__c = emailSendList.size() >= 8 ? emailSendList.get(7) : null;
                caseNew.EmailAlert09__c = emailSendList.size() >= 9 ? emailSendList.get(8) : null;
                caseNew.EmailAlert10__c = emailSendList.size() >= 10 ? emailSendList.get(9) : null;
                
                System.debug('@@@ caseNew.EmailAlert01__c: ' + caseNew.EmailAlert01__c);
                System.debug('@@@ caseNew.EmailAlert02__c: ' + caseNew.EmailAlert02__c);
                System.debug('@@@ caseNew.EmailAlert03__c: ' + caseNew.EmailAlert03__c);
                System.debug('@@@ caseNew.EmailAlert04__c: ' + caseNew.EmailAlert04__c);
                System.debug('@@@ caseNew.EmailAlert05__c: ' + caseNew.EmailAlert05__c);
                System.debug('@@@ caseNew.EmailAlert06__c: ' + caseNew.EmailAlert06__c);
                System.debug('@@@ caseNew.EmailAlert07__c: ' + caseNew.EmailAlert07__c);
                System.debug('@@@ caseNew.EmailAlert08__c: ' + caseNew.EmailAlert08__c);
                System.debug('@@@ caseNew.EmailAlert09__c: ' + caseNew.EmailAlert09__c);
                System.debug('@@@ caseNew.EmailAlert10__c: ' + caseNew.EmailAlert10__c);
            }
        }
    }
	
    public static void cartaEncerramentoTask(List<Case> triggerNew, Map<Id, Case> oldMap){
        Boolean isUpdate = oldMap != null;
        List<Task> taskListUpsert = new List<Task>();
        Map<Id, User> mapUsers;
        Map<Id, Task> mapCETaskByCaseId;
        
        if(isUpdate) {
            mapUsers = UserDAO.getUserMapById(SetUtils.getCaseUserOwnerIdSet(triggerNew));
            mapCETaskByCaseId = new Map<Id, Task>();
            
            for(Task tsk : [SELECT Subject, OwnerId, WhatId FROM Task WHERE WhatId in :SetUtils.getCaseIdSet(triggerNew) AND LetterTask__c = true]) {
                mapCETaskByCaseId.put(tsk.WhatId, tsk);
            }
        }
        
        for(Case caseNew : triggerNew){
            if(isUpdate) {
                Case caseOld = oldMap.get(caseNew.Id);
                Boolean isNewOwnerAnalyst = mapUsers.containsKey(caseNew.OwnerId) && mapUsers.get(caseNew.OwnerId).Profile.Name == Label.CRAProfileName;
                
                if(caseNew.OwnerId != caseOld.OwnerId && isNewOwnerAnalyst && mapCETaskByCaseId.containsKey(caseNew.Id)) {
                    taskListUpsert.add(new Task(
                        Id = mapCETaskByCaseId.get(caseNew.Id).Id,
                        OwnerId = caseNew.OwnerId
                    ));
                }
                
            } else if(!'Reclame Aqui'.equalsIgnoreCase(caseNew.Origin)) {
                Id ownerId = caseNew.OwnerId.getSObjectType() == Schema.User.SObjectType ? caseNew.OwnerId : Label.LetterTaskDefaultOwner;
                taskListUpsert.add( new Task(
                    OwnerId = ownerId,
                    WhatId = caseNew.Id,
                    Subject = 'Enviar Carta de Encerramento',
                    Priority = 'Normal',
                    Status = 'Open',
                    LetterTask__c = true
                ));
            }
            
        }
        
        if(!taskListUpsert.isEmpty())
            Database.upsert(taskListUpsert);
    }
    
}