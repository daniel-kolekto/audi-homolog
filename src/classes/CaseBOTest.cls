@isTest
private class CaseBOTest {
    
    @testSetup static void createTestData(){
        Account dealerAcct01 = AccountBuild.createDealer('9055');
        Account dealerAcct02 = AccountBuild.createDealer('9066');
        Account customer01 = AccountBuild.createCustomer('PF', '66830433614');
        Database.insert(new List<Account>{dealerAcct01, dealerAcct02, customer01});
        
        Product2 vehicleProduct = ProductBuild.createVehicle('6FG9RT');
        Database.insert(vehicleProduct);
        
        Vehicle__c vehicle = VehicleBuild.createVehicle('1GTDC14N2FJ506808', 'EMT3194', vehicleProduct.Id);
        Database.insert(vehicle);
        
        Database.insert(new Ownership__c(Name = 'test', Vehicle__c = vehicle.Id, Customer__c = customer01.Id, Status__c = 'ACTIVE'));
    }
    
    
    @isTest static void shouldSetCaseContactAndSLA() {
        Account customerTest = AccountDAO.getAccountByCpfCnpj('66830433614', true);
        System.assert(customerTest != null);
        Account dealerTest = AccountDAO.getDealerByCode('9055');
        System.assert(dealerTest != null);
        Vehicle__c vehicleTest = VehicleDAO.getVehicleByChassi('1GTDC14N2FJ506808');
        System.assert(vehicleTest != null);
        
        Case case01 = CaseBuild.createCustomerCareCase();
        case01.AccountId = customerTest.Id;
        case01.Dealer__c = dealerTest.Id;
        case01.Vehicle__c = vehicleTest.Id;
        
        Test.startTest();
        
        Database.insert(case01);
        
        Test.stopTest();
        
        Case caseAssert = CaseDAO.getCaseById(case01.Id, true);
        System.assert(caseAssert != null);
        
        System.assertEquals(Utils.validateId(Label.EntitlementCustomerCare), caseAssert.EntitlementId);
        System.assert(caseAssert.ContactId != null);
    }
    
    @isTest static void shouldSetWaitingInternalAreaAsActualTreatmentStatus() {
        Account customerTest = AccountDAO.getAccountByCpfCnpj('66830433614', true);
        System.assert(customerTest != null);
        Account dealerTest = AccountDAO.getDealerByCode('9055');
        System.assert(dealerTest != null);
        Vehicle__c vehicleTest = VehicleDAO.getVehicleByChassi('1GTDC14N2FJ506808');
        System.assert(vehicleTest != null);
        
        Case case01 = CaseBuild.createCustomerCareCase();
        case01.AccountId = customerTest.Id;
        case01.Dealer__c = dealerTest.Id;
        case01.Vehicle__c = vehicleTest.Id;
        case01.TreatmentStatus__c = 'Waiting Internal Area';
        
        Test.startTest();
        
        Database.insert(case01);
        
        Test.stopTest();
        
        Case caseAssert = CaseDAO.getCaseById(case01.Id, true);
        System.assert(caseAssert != null);
        
        System.assertEquals('Waiting Internal Area', caseAssert.ActualTreatmentStatus__c);
    }
    
    @isTest static void shouldSetWaitingDealerAsActualTreatmentStatus() {
        Account customerTest = AccountDAO.getAccountByCpfCnpj('66830433614', true);
        System.assert(customerTest != null);
        Account dealerTest = AccountDAO.getDealerByCode('9055');
        System.assert(dealerTest != null);
        Vehicle__c vehicleTest = VehicleDAO.getVehicleByChassi('1GTDC14N2FJ506808');
        System.assert(vehicleTest != null);
        
        Case case01 = CaseBuild.createCustomerCareCase();
        case01.AccountId = customerTest.Id;
        case01.Dealer__c = dealerTest.Id;
        case01.Vehicle__c = vehicleTest.Id;
        case01.TreatmentStatus__c = 'Waiting Dealer';
        
        Test.startTest();
        
        Database.insert(case01);
        
        Test.stopTest();
        
        Case caseAssert = CaseDAO.getCaseById(case01.Id, true);
        System.assert(caseAssert != null);
        
        System.assertEquals('Waiting Dealer', caseAssert.ActualTreatmentStatus__c);
    }
    
    @isTest static void shouldSetWaitingCRAAsActualTreatmentStatus() {
        Account customerTest = AccountDAO.getAccountByCpfCnpj('66830433614', true);
        System.assert(customerTest != null);
        Account dealerTest = AccountDAO.getDealerByCode('9055');
        System.assert(dealerTest != null);
        Vehicle__c vehicleTest = VehicleDAO.getVehicleByChassi('1GTDC14N2FJ506808');
        System.assert(vehicleTest != null);
        
        Case case01 = CaseBuild.createCustomerCareCase();
        case01.AccountId = customerTest.Id;
        case01.Dealer__c = dealerTest.Id;
        case01.Vehicle__c = vehicleTest.Id;
        case01.TreatmentStatus__c = 'Waiting CRA';
        
        Test.startTest();
        
        Database.insert(case01);
        
        Test.stopTest();
        
        Case caseAssert = CaseDAO.getCaseById(case01.Id, true);
        System.assert(caseAssert != null);
        
        System.assertEquals('Waiting CRA', caseAssert.ActualTreatmentStatus__c);
    }
    
    @isTest static void shouldSetWaitingReclameAquiAsActualTreatmentStatus() {
        Account customerTest = AccountDAO.getAccountByCpfCnpj('66830433614', true);
        System.assert(customerTest != null);
        Account dealerTest = AccountDAO.getDealerByCode('9055');
        System.assert(dealerTest != null);
        Vehicle__c vehicleTest = VehicleDAO.getVehicleByChassi('1GTDC14N2FJ506808');
        System.assert(vehicleTest != null);
        
        Case case01 = CaseBuild.createCustomerCareCase();
        case01.AccountId = customerTest.Id;
        case01.Dealer__c = dealerTest.Id;
        case01.Vehicle__c = vehicleTest.Id;
        case01.TreatmentStatus__c = 'Waiting Reclame Aqui';
        
        Test.startTest();
        
        Database.insert(case01);
        
        Test.stopTest();
        
        Case caseAssert = CaseDAO.getCaseById(case01.Id, true);
        System.assert(caseAssert != null);
        
        System.assertEquals('Waiting Reclame Aqui', caseAssert.ActualTreatmentStatus__c);
    }
    
    @isTest static void shouldSetKundentischDatetime() {
        Account customerTest = AccountDAO.getAccountByCpfCnpj('66830433614', true);
        System.assert(customerTest != null);
        Account dealerTest = AccountDAO.getDealerByCode('9055');
        System.assert(dealerTest != null);
        Vehicle__c vehicleTest = VehicleDAO.getVehicleByChassi('1GTDC14N2FJ506808');
        System.assert(vehicleTest != null);
        
        Case case01 = CaseBuild.createCustomerCareCase();
        case01.AccountId = customerTest.Id;
        case01.Dealer__c = dealerTest.Id;
        case01.Vehicle__c = vehicleTest.Id;
        case01.Priority = 'Kundentisch';
        
        Test.startTest();
        
        Database.insert(case01);
        
        Test.stopTest();
        
        Case caseAssert = CaseDAO.getCaseById(case01.Id, true);
        System.assert(caseAssert != null);
        
        System.assert(caseAssert.KundentischDatetime__c != null);
    }
    
    @isTest static void shouldSetKundentischTotalTime() {
        Account customerTest = AccountDAO.getAccountByCpfCnpj('66830433614', true);
        System.assert(customerTest != null);
        Account dealerTest = AccountDAO.getDealerByCode('9055');
        System.assert(dealerTest != null);
        Vehicle__c vehicleTest = VehicleDAO.getVehicleByChassi('1GTDC14N2FJ506808');
        System.assert(vehicleTest != null);
        
        Case case01 = CaseBuild.createCustomerCareCase();
        case01.AccountId = customerTest.Id;
        case01.Dealer__c = dealerTest.Id;
        case01.Vehicle__c = vehicleTest.Id;
        case01.Priority = 'Kundentisch';
        Database.insert(case01);
        
        Test.startTest();
        
        Case caseUpdate = CaseDAO.getCaseById(case01.Id, true);
        caseUpdate.Priority = 'High';
        Database.update(caseUpdate);
        
        Test.stopTest();
        
        Case caseAssert = CaseDAO.getCaseById(case01.Id, true);
        System.assert(caseAssert != null);
        
        System.assert(caseAssert.KundentischTotalTime__c != null);
    }
    
    @isTest static void shouldCloseMilestones() {
        Account customerTest = AccountDAO.getAccountByCpfCnpj('66830433614', true);
        System.assert(customerTest != null);
        Account dealerTest = AccountDAO.getDealerByCode('9055');
        System.assert(dealerTest != null);
        Vehicle__c vehicleTest = VehicleDAO.getVehicleByChassi('1GTDC14N2FJ506808');
        System.assert(vehicleTest != null);
        
        Case case01 = CaseBuild.createCustomerCareCase();
        case01.AccountId = customerTest.Id;
        case01.Dealer__c = dealerTest.Id;
        case01.Vehicle__c = vehicleTest.Id;
        Database.insert(case01);
        
        Map<Id, List<CaseMilestone>> mapMilestonesByCase = CaseMilestoneDAO.getCaseMilestonesMapByCaseId(new Set<Id>{case01.Id});
        System.assert(mapMilestonesByCase.containsKey(case01.Id));
        
        List<CaseMilestone> caseTestMilestones = mapMilestonesByCase.get(case01.Id);
        System.assertEquals(1, caseTestMilestones.size());
        
        System.assertEquals(Label.SLACustomerCare, caseTestMilestones.get(0).MilestoneType.Name);
        
        Test.startTest();
        
        Case caseUpdate = CaseDAO.getCaseById(case01.Id, true);
        caseUpdate.TreatmentStatus__c = 'Waiting Customer Answer';
        Database.update(caseUpdate);
        
        Test.stopTest();
        
        mapMilestonesByCase = CaseMilestoneDAO.getCaseMilestonesMapByCaseId(new Set<Id>{case01.Id});
        System.assert(mapMilestonesByCase.containsKey(case01.Id));
        
        caseTestMilestones = mapMilestonesByCase.get(case01.Id);
        System.assert(caseTestMilestones.isEmpty());
    }
    
}