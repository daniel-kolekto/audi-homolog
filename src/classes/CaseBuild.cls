@isTest
public class CaseBuild {
	
	public static Case createCustomerCareCase(){
		return getNewCase('AudiCustomerCare', 'CustomerCare', 'Test Customer Care', 'Email', 'Medium');
	}
	
	public static Case createComplaintCase(){
		return getNewCase('AudiCustomerCare', 'Complaint', 'Test Complaint', 'Email', 'Medium');
	}
	
	public static Case createComplimentCase(){
		return getNewCase('AudiCustomerCare', 'Compliment', 'Test Compliment', 'Email', 'Medium');
	}
	
	public static Case createQuestionsCase(){
		return getNewCase('AudiCustomerCare', 'Questions', 'Test Questions', 'Email', 'Medium');
	}
	
	public static Case createRequestCase(){
		return getNewCase('AudiCustomerCare', 'Request', 'Test Request', 'Email', 'Medium');
	}
	
	public static Case createAudiAgCase(){
		return getNewCase('AudiAG', 'Questions', 'Test AudiAG', 'Audi AG', 'Medium');
	}
	
	public static Case createReclameAquiCase(){
		return getNewCase('ReclameAqui', 'Complaint', 'Test Reclame Aqui', 'Reclame Aqui', 'High');
	}
	
	
	public static Case getNewCase(String queueName, String rtName, String subject, String origin, String priority){
		return new Case(
			OwnerId = Utils.getQueueId(queueName),
			RecordTypeId = Utils.getRecordTypeId('Case', rtName),
			Subject = subject,
			Description = 'Створено кейс-тест, привіт, я бозо',
			Status = 'New',
			Origin = origin,
			Priority = priority
		);
	}
	
}