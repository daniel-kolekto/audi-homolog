public without sharing class CaseCommentBO {
	
	public static void notifyAnalystCaseComment(List<CaseComment> triggerNew){
		Map<Id, User> userMap = UserDAO.getUserMapById(SetUtils.getCaseCommentCreatedByIdSet(triggerNew));
		List<Case> caseUpdate = new List<Case>();
		
		for(CaseComment cc : triggerNew){
            if(userMap.get(cc.CreatedById).ProfileId == Utils.getProfileId('Audi Dealer')) {
                caseUpdate.add(new Case(
                    Id = cc.ParentId,
                    CaseCommentBody__c = cc.CommentBody,
                    NotifyCaseCommentToAnalyst__c = true
                ));
            }
		}
		
		if(!caseUpdate.isEmpty())
			Database.update(caseUpdate);
	}
	
}