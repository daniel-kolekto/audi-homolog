@isTest
private class CaseCommentBOTest {
	
	@testSetup static void createTestData(){
        Account dealerAcct01 = AccountBuild.createDealer('9055');
        Account dealerAcct02 = AccountBuild.createDealer('9066');
        Account customer01 = AccountBuild.createCustomer('PF');
        Database.insert(new List<Account>{dealerAcct01, dealerAcct02, customer01});
        
        Product2 vehicleProduct = ProductBuild.createVehicle('6FG9RT');
        Database.insert(vehicleProduct);
        
        Vehicle__c vehicle = VehicleBuild.createVehicle('1GTDC14N2FJ506808', 'EMT3194', vehicleProduct.Id);
        Database.insert(vehicle);
        
        Database.insert(new Ownership__c(Name = 'test', Vehicle__c = vehicle.Id, Customer__c = customer01.Id, Status__c = 'ACTIVE'));
        
        
        Case case01 = CaseBuild.createCustomerCareCase();
        case01.AccountId = customer01.Id;
        case01.Dealer__c = dealerAcct01.Id;
        case01.Vehicle__c = vehicle.Id;
        
        Case case02 = CaseBuild.createAudiAgCase();
        case02.AccountId = customer01.Id;
        case02.Dealer__c = dealerAcct01.Id;
        case02.Vehicle__c = vehicle.Id;
        
        Case case03 = CaseBuild.createReclameAquiCase();
        case03.AccountId = customer01.Id;
        case03.Dealer__c = dealerAcct01.Id;
        case03.Vehicle__c = vehicle.Id;
        
        Database.insert(new List<Case>{case01, case02, case03});
    }
    
	
	@isTest static void shouldNotifyAnalystCaseComment() {
		User dealerUser = [SELECT Id FROM User WHERE IsActive = true AND ProfileId = :Utils.getProfileId('Audi Dealer') LIMIT 1];
		System.assert(dealerUser != null);
		
		List<Case> lstCase = [
			SELECT Id, CaseCommentBody__c, NotifyCaseCommentToAnalyst__c
			FROM Case
			WHERE RecordTypeId = :Utils.getRecordTypeId('Case', 'CustomerCare')
		];
		Case caseTest = !lstCase.isEmpty() ? lstCase.get(0) : null;
		System.assert(caseTest != null);
		
		System.assert(String.isEmpty(caseTest.CaseCommentBody__c));
		System.assert(!caseTest.NotifyCaseCommentToAnalyst__c);
		
		CS_ForwardCaseController forwardController = new CS_ForwardCaseController(new ApexPages.StandardController(caseTest));
		forwardController.manualShareCaseRead(caseTest.Id, dealerUser.Id);
		
		Test.startTest();
        System.runAs(dealerUser) {
        	
            Database.insert(new CaseComment(
                CommentBody = 'Test case comment',
                ParentId = caseTest.Id
            ));
            
        }
		Test.stopTest();
		
		Case caseAssert = CaseDAO.getCaseById(caseTest.Id, true);
		SYstem.assert(caseAssert != null);
		
		System.assertEquals('Test case comment', caseAssert.CaseCommentBody__c);
		System.assert(!caseAssert.NotifyCaseCommentToAnalyst__c);
	}
	
}