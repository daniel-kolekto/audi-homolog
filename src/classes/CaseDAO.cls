public class CaseDAO {
    
    /* Lists */
    public static List<Case> getCasesById(Set<Id> caseIdSet, Boolean allFields){
        if(caseIdSet == null || caseIdSet.isEmpty()) return new List<Case>();
        
        if(allFields) {
            return getAllFields('WHERE Id in ' + Utils.buildIdSetAsStringForQuery(caseIdSet));
            
        } else{
            return [
                SELECT  CaseNumber,
                        TreatmentStatus__c,
                        Status,
                        Subject,
                        Dealer__c,
                        AccountId,
                        Account.Name,
                        Description,
                        NotPunctuation__c,
                        WhyNotPunctuation__c,
                        CaseCommentBody__c,
                        NotifyCaseCommentToAnalyst__c
                
                FROM    Case
                WHERE   Id in :caseIdSet
            ];
        }
    }
    
    public static List<Case> getCasesByNumber(Set<String> caseNumberSet, Boolean allFields){
        if(caseNumberSet == null || caseNumberSet.isEmpty()) return new List<Case>();
        
        if(allFields) {
            return getAllFields('WHERE CaseNumber in ' + Utils.buildStringSetAsStringForQuery(caseNumberSet));
            
        } else{
            return [
                SELECT  CaseNumber,
                        TreatmentStatus__c,
                        Status,
                        Subject,
                        Dealer__c,
                        AccountId,
                        Account.Name,
                        Description,
                        NotPunctuation__c,
                        WhyNotPunctuation__c,
                        CaseCommentBody__c,
                        NotifyCaseCommentToAnalyst__c
                
                FROM    Case
                WHERE   CaseNumber in :caseNumberSet
            ];
        }
    }
    
    
    /* Single Records */
    
    public static Case getCaseById(Id caseId, Boolean allFields){
        if(String.isEmpty(caseId)) return null;
        List<Case> caseList = getCasesById(new Set<Id>{caseId}, allFields);
        
        try {
            return caseList.get(0);
        } catch(Exception e){
            System.debug('CaseDAO.getCaseById - Could not retrieve Case: ' + e.getMessage());
            return null;
        }
    }
    
    public static Case getCaseByNumber(String caseNumber, Boolean allFields){
        if(String.isEmpty(caseNumber)) return null;
        List<Case> caseList = getCasesByNumber(new Set<String>{caseNumber}, allFields);
        
        try {
            return caseList.get(0);
        } catch(Exception e){
            System.debug('CaseDAO.getCaseByNumber - Could not retrieve Case: ' + e.getMessage());
            return null;
        }
    }
    
    
    /* Get all fields */
    
	public static List<Case> getAllFields(String whereCondition){
        Map<String, Schema.SObjectType> globalMap = Schema.getGlobalDescribe();
        Map<String, Schema.SObjectField> fieldMap = globalMap.get('Case').getDescribe().fields.getMap();
        
        String query = 'SELECT ';
        
        for(String fieldName : fieldMap.keySet())
            query += fieldName + ',';
        
        query = query.substring(0, query.length()-1) + ' , Account.PersonMobilePhone FROM Case ' + whereCondition;
        
        System.debug('@@@ CaseDAO getAllFields query: ' + query);
        
        return Database.query(query);
    }
    
}