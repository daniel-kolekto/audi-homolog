public class CaseUtils {
	
	public static Set<String> filterContactEmails(List<Contact> contactList, String index){
		Set<String> emailSet = new Set<String>();
		
        for(Contact cont : contactList) {
            if(String.isNotEmpty(cont.EmailAlertIndex__c) && cont.EmailAlertIndex__c.contains(index)) emailSet.add(cont.Email);
        }
        
        return emailSet;
    }
	
}