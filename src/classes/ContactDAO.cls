public class ContactDAO {
	
	public static Map<Id, Contact> getContactMapByCustomer(List<Account> acctList){
		Map<Id, Contact> contactMapByCustomer = new Map<Id, Contact>();
		Map<Id, Contact> contactMapById = getContactMapById( SetUtils.getAccountPersonContactIdSet(acctList) );
		
		for(Account acct : acctList){
            if(Utils.isPersonRecordType(acct.RecordTypeId) && contactMapById.containsKey(acct.PersonContactId)) {
                contactMapByCustomer.put(acct.Id, contactMapById.get(acct.PersonContactId));
                
            } else if(!Utils.isPersonRecordType(acct.RecordTypeId)) {
            	List<Contact> acctContacts = acct.Contacts;
            	if(!acctContacts.isEmpty())
            		contactMapByCustomer.put(acct.Id, acctContacts.get(0));
            }
		}
		
		return contactMapByCustomer;
	}
	
	public static Map<Id, Contact> getContactMapById(Set<Id> contactIdSet){
		return new Map<Id, Contact>( getContactById(contactIdSet) );
	}
	
	public static Map<Id, List<Contact>> getContactsMapByAcctId(Set<Id> acctIdSet){
		Map<Id, List<Contact>> contsMapByAcct = new Map<Id, List<Contact>>();
		List<Contact> contacts = getContactByAccountId(acctIdSet);
		
		for(Id i : acctIdSet){
			List<Contact> accConts = new List<Contact>();
			for(Contact cont : contacts){
				if(cont.AccountId == i)
					accConts.add(cont);
			}
			contsMapByAcct.put(i, accConts);
		}
		
		return contsMapByAcct;
	}
	
	
	public static List<Contact> getContactById(Set<Id> contactIdSet){
		return [
			SELECT AccountId, EmailAlertIndex__c, Email
			FROM Contact
			WHERE Id in :contactIdSet
		];
	}
	
	public static List<Contact> getContactByAccountId(Set<Id> acctIdSet){
		return [
			SELECT AccountId, EmailAlertIndex__c, Email
			FROM Contact
			WHERE AccountId in :acctIdSet
		];
	}
	
}