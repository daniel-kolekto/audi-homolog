@isTest
private class ContactDAOTest {
	
	@testSetup static void createTestData(){
		Account dealerTest = AccountBuild.createDealer('9090');
		Account customer01 = AccountBuild.createCustomer('PF', '38477608903');
		Account customer02 = AccountBuild.createCustomer('PJ', '47285892000188');
		
		Database.insert(new List<Account>{dealerTest, customer01, customer02});
		
		
		Database.insert(new Contact(
			FirstName = 'Test',
			LastName = 'Contact',
			AccountId = dealerTest.Id
		));
	}
	
	
	@isTest static void shouldQueryContacts() {
		Account dealerTest = AccountDAO.getDealerByCode('9090');
		Account customerPF = AccountDAO.getAccountByCpfCnpj('38477608903', true);
		Account customerPJ = AccountDAO.getAccountByCpfCnpj('47285892000188', true);
		
		System.assert(dealerTest != null);
		System.assert(customerPF != null);
		System.assert(customerPJ != null);
		
		Test.startTest();
		
		Map<Id, Contact> contactMapTest = ContactDAO.getContactMapByCustomer(new List<Account>{customerPF});
		System.assert(contactMapTest.containsKey(customerPF.Id));
		
		ContactDAO.getContactMapById( new Set<Id>{contactMapTest.get(customerPF.Id).Id} );
		ContactDAO.getContactsMapByAcctId( new Set<Id>{dealerTest.Id} );
		
		Test.stopTest();
	}
	
}