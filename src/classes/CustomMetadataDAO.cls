public with sharing class CustomMetadataDAO {
	
	public static Map<String, TimeCountLimit__mdt> mapTimeCountLimitByDeveloperName = new Map<String, TimeCountLimit__mdt>();
	
	static {
		List<TimeCountLimit__mdt> timeCountList = [
			SELECT 	DeveloperName,
					HoursOfDay__c,
					NumberOfDays__c,
					SlaLeadLimit01__c,
					SlaLeadLimit02__c,
					SlaLeadLimit03__c,
					SlaLeadLimit04__c
					
			FROM 	TimeCountLimit__mdt
		];
		
		for(TimeCountLimit__mdt tcl : timeCountList){
			mapTimeCountLimitByDeveloperName.put(tcl.DeveloperName, tcl);
		}
	}
	
	
	public static Integer getDuplicateLeadLimitDays(){
		Integer numberOfDays;
		
		try {
			Decimal num = mapTimeCountLimitByDeveloperName.get('DuplicateLimit').NumberOfDays__c;
			if(num == null) throw new Utils.GenericException('O campo "NumberOfDays__c" dentro de TimeCountLimit__mdt está nulo');
			numberOfDays = Integer.valueOf(num);
			
		} catch(Exception e){
			System.debug('Exception CustomMetadataDAO - Definido 40 dias para a regra de duplicidade do Lead devido ao erro: ' + e.getMessage());
			numberOfDays = 40;
		}
		
		return numberOfDays;
	}
	
	public static Integer getLeadAutoLossDays(){
		Integer numberOfDays;
		
		try {
			Decimal num = mapTimeCountLimitByDeveloperName.get('AutoLossDays').NumberOfDays__c;
			if(num == null) throw new Utils.GenericException('O campo "NumberOfDays__c" dentro de TimeCountLimit__mdt está nulo');
			numberOfDays = Integer.valueOf(num);
			
		} catch(Exception e){
			System.debug('Exception CustomMetadataDAO - Definido 40 dias para perda automática do Lead devido ao erro: ' + e.getMessage());
			numberOfDays = 40;
		}
		
		return numberOfDays;
	}
	
	public static Integer getBusinessHoursDay(){
		Integer hours;
		
		try {
			Decimal num = mapTimeCountLimitByDeveloperName.get('BusinessHoursDay').HoursOfDay__c;
			if(num == null) throw new Utils.GenericException('O campo "HoursOfDay__c" dentro de TimeCountLimit__mdt está nulo');
			hours = Integer.valueOf(num);
			
		} catch(Exception e){
			System.debug('Exception CustomMetadataDAO - Definido 9 horas úteis por dia devido ao erro: ' + e.getMessage());
			hours = 9;
		}
		
		return hours;
	}
	
	public static String getLeadNurturingLimitSLA(){
		String slaNurturingLimits = '20;40;60;90';
		
		try {
			TimeCountLimit__mdt timeCount = mapTimeCountLimitByDeveloperName.get('SLALimitNurturing');
			
			if(timeCount.SlaLeadLimit01__c == null || timeCount.SlaLeadLimit02__c == null || timeCount.SlaLeadLimit03__c == null || timeCount.SlaLeadLimit04__c == null){
                throw new Utils.GenericException('Os campos de SLA do Nurturing não estão definidos corretamente em TimeCountLimit__mdt');
			}
			
			slaNurturingLimits = String.valueOf(timeCount.SlaLeadLimit01__c) + ';' + String.valueOf(timeCount.SlaLeadLimit02__c) + ';' + 
								String.valueOf(timeCount.SlaLeadLimit03__c) + ';' + String.valueOf(timeCount.SlaLeadLimit04__c);
			
		} catch(Exception e){
			System.debug('Exception CustomMetadataDAO - Definido 20;40;60;90 no SLA do Nurturing do Lead devido ao erro: ' + e.getMessage());
		}
		
		return slaNurturingLimits;
	}
	
	public static String getLeadDealerLimitSLA(){
		String slaDealerLimits = '60;90;120;180';
		
		try {
			TimeCountLimit__mdt timeCount = mapTimeCountLimitByDeveloperName.get('SLALimitDealer');
			
			if(timeCount.SlaLeadLimit01__c == null || timeCount.SlaLeadLimit02__c == null || timeCount.SlaLeadLimit03__c == null || timeCount.SlaLeadLimit04__c == null){
                throw new Utils.GenericException('Os campos de SLA do Dealer não estão definidos corretamente em TimeCountLimit__mdt');
			}
			
			slaDealerLimits = String.valueOf(timeCount.SlaLeadLimit01__c) + ';' + String.valueOf(timeCount.SlaLeadLimit02__c) + ';' + 
								String.valueOf(timeCount.SlaLeadLimit03__c) + ';' + String.valueOf(timeCount.SlaLeadLimit04__c);
			
		} catch(Exception e){
			System.debug('Exception CustomMetadataDAO - Definido 60;90;120;180 no SLA do Dealer do Lead devido ao erro: ' + e.getMessage());
		}
		
		return slaDealerLimits;
	}
	
	public static Integer getMilestoneViolationEmailInterval(String name){
		Integer minutes;
		
		try {
			Decimal hours = mapTimeCountLimitByDeveloperName.get(name).HoursOfDay__c;
			if(hours == null) throw new Utils.GenericException('O campo "HoursOfDay__c" dentro de TimeCountLimit__mdt está nulo');
			minutes = Integer.valueOf(hours * 60);
			
		} catch(Exception e){
			System.debug('Exception CustomMetadataDAO - ' + 
				'Definido 660 minutos para o Intervalo de disparo de email do SLA devido ao erro: ' + e.getMessage());
			minutes = 660;
		}
		
		return minutes;
	}
	


	public static Map<String,SLA_Control__mdt> getSLAControl(){
		List<SLA_Control__mdt> metadataList = 	[SELECT DeveloperName,Id,Label,MasterLabel,NamespacePrefix,QualifiedApiName,
												SLAFullGreen__c,SLAFullYellow__c,SLAHalfGreen__c,SLAHalfYellow__c 
												FROM SLA_Control__mdt
												];

		Map<String,SLA_Control__mdt> metadataMap = new Map<String,SLA_Control__mdt> ();
		for(SLA_Control__mdt metadata : metadataList){
			metadataMap.put(metadata.MasterLabel,metadata);
		}
		return metadataMap;
	}

}