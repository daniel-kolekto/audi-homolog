@isTest
private class CustomMetadataDAOTest {
	
	@isTest static void shouldQueryCustomMetadata() {
		Test.startTest();
		
		CustomMetadataDAO.getDuplicateLeadLimitDays();
		CustomMetadataDAO.getLeadAutoLossDays();
		CustomMetadataDAO.getBusinessHoursDay();
		CustomMetadataDAO.getLeadNurturingLimitSLA();
		CustomMetadataDAO.getLeadDealerLimitSLA();
		CustomMetadataDAO.getMilestoneViolationEmailInterval('test');
        CustomMetadataDAO.getSLAControl();
		Test.stopTest();
	}
	
}