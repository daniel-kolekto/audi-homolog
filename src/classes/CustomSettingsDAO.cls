public class CustomSettingsDAO {
	
	public static MarketingCloudConfiguration__c getMarketingCloudConfiguration(){
		return MarketingCloudConfiguration__c.getAll().get('default');
	}
	
	public static List<SelectOption> getInternalAreaOptions(){
        List<SelectOption> opts = new List<SelectOption>();
        
        for(InternalAreas__c ia : InternalAreas__c.getAll().values()) {
            opts.add( new SelectOption(ia.GroupId__c, ia.Name) );
        }
        
        return opts;
	}
}