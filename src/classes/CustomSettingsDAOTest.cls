@isTest
private class CustomSettingsDAOTest {
	
	@testSetup static void createTestData(){
		Database.insert(new MarketingCloudConfiguration__c(
			Name = 'default',
			ClienteId__c = 't6thssn7p8uk0dzs9s82jrp6',
			ClientSecret__c = 'Wpqp8Ra5aXAOaVyjAyHghSaS',
			MessageId__c = 'Mjo3ODow'
		));
	}
	
	
	@isTest static void testCustomSettingsDAO() {
		Test.startTest();
		
		CustomSettingsDAO.getMarketingCloudConfiguration();
		CustomSettingsDAO.getInternalAreaOptions();
		
		Test.stopTest();
	}
	
}