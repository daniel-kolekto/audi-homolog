global class ForwardLeadBatch implements Database.Batchable<sObject>, Schedulable {
	
	private static Id audiB2bQueueId = Label.AudiB2bQueueId;
	private static Id leadNurturingQueueId = Label.LeadNurturingQueueId;
	
	
	global void execute(SchedulableContext SC){
		Database.executeBatch(this, 1);
	}
	
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator([
			SELECT 	FirstName,
					LastName,
					Salutation,
					RecordTypeId,
					OwnerId,
					CreatedDate,
					CpfCnpj__c,
					Birthdate__c,
					Company,
					Email,
					Phone,
					MobilePhone,
			        OptinEmail__c,
			        OptinPhone__c,
					PreferredContact__c,
			        Street,
			        PostalCode,
			        City,
			        State,
			        Country,
			        Interest__c,
			        LeadManager__c,
			        MajorDealerCode__c,
			        MajorDealer__c,
					MajorDealer__r.DealerSalesManager__c,
					MajorDealer__r.DealerAfterSalesManager__c,
					ScheduledDealer__c,
					ScheduledDealer__r.DealerSalesManager__c,
					ScheduledDealer__r.DealerAfterSalesManager__c,
			        OptionalDealerCode__c,
			        OptionalDealer__c,
			        ProductCode__c,
			        ProductInterest__c,
					ModelWeb__c,
					ModelVersionWeb__c,
					Status,
			        LeadType__c,
			        LeadSource,
			        SubLeadSource__c,
			        Rating
					
			FROM 	Lead
			WHERE 	Status not in ('ClosedWon', 'ClosedLost', 'AutomaticallyLost', 'InvalidData')
		]);
	}
	
    global void execute(Database.BatchableContext BC, List<Lead> scope) {
    	System.debug('@@@ ForwardLeadBatch scope @@@\n' + JSON.serializePretty(scope));
    	
    	List<Lead> listLeadUpsert = new List<Lead>();
        Integer dayHours = CustomMetadataDAO.getBusinessHoursDay(); // Quantidade de horas úteis por dia
        Integer autoLossDays = CustomMetadataDAO.getLeadAutoLossDays();
        
        for(Lead lead : scope) {
        	Datetime expireLeadDatetime = BusinessHours.add( Utils.getDefaultBusinessHoursId(), lead.CreatedDate, (dayHours * 3 * 60 * 60 * 1000) );
        	
        	System.debug('@@@ expireLeadDatetime: ' + expireLeadDatetime);
        	System.debug('@@@ 40 days later: ' + lead.CreatedDate.addDays(autoLossDays));
        	System.debug('@@@ lead.CreatedDate: ' + lead.CreatedDate);
        	System.debug('@@@ lead.OwnerId: ' + lead.OwnerId);
        	System.debug('@@@ lead.LeadManager__c: ' + lead.LeadManager__c);
        	
            if(Datetime.now() >= expireLeadDatetime && ('Lead-Nurturing'.equals(lead.LeadManager__c) || 'Fila Lead-Nurturing'.equals(lead.LeadManager__c))) {
            	lead.OwnerId = lead.RecordTypeId == Utils.getRecordTypeId('Lead', 'AfterSales') ? lead.ScheduledDealer__r.DealerAfterSalesManager__c : lead.ScheduledDealer__r.DealerSalesManager__c;
            	lead.LeadForwardingDate__c = Datetime.now();
            	lead.LeadAutomaticallyForwarded__c = true;
            	listLeadUpsert.add(lead);
            	
            } else if(Datetime.now() >= lead.CreatedDate.addDays(autoLossDays)){
            	listLeadUpsert.add( new Lead(Id = lead.Id, ExpiredLead__c = true) );
            	
            } else if(Datetime.now() >= lead.CreatedDate.addDays(autoLossDays-5)){
            	listLeadUpsert.add( new Lead(Id = lead.Id, FiveDaysExpiration__c = true) );
            }
        }
        
        System.debug('@@@ ForwardLeadBatch listLeadUpsert: \n' + JSON.serializePretty(listLeadUpsert));
        
        if(!listLeadUpsert.isEmpty())
        	Database.upsert(listLeadUpsert);
    }
	
	global void finish(Database.BatchableContext BC) {
		
	}
	
}