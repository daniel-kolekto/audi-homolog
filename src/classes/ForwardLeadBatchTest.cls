@isTest
private class ForwardLeadBatchTest {
	
	@testSetup static void createTestData(){
		Account dealerAcct01 = AccountBuild.createDealer('9055');
		Account dealerAcct02 = AccountBuild.createDealer('9066');
		Database.insert(new List<Account>{dealerAcct01, dealerAcct02});
		
		Product2 vehicleProduct = ProductBuild.createVehicle('6FG9RT');
		Database.insert(vehicleProduct);
	}
	
	
	@isTest static void shouldForwardToLeadManager() {
		Account leadDealer01 = AccountDAO.getDealerByCode('9055');
		String leadCpf = Utils.generateCpf();
		
		Lead leadWeb = LeadBuild.createLead(leadCpf, '9055', '9066', '6FG9RT');
		Database.insert(leadWeb);
		
		Test.setCreatedDate(leadWeb.Id, Datetime.now().addDays(-7));
		leadWeb.OwnerId = Label.LeadNurturingQueueId;
		Database.update(leadWeb);
		
		Test.startTest();
		
		Database.executeBatch(new ForwardLeadBatch(), 1);
		
		Test.stopTest();
		
		List<Lead> leadList = LeadDAO.getLeadsByCpfCnpj(new Set<String>{leadCpf}, true);
		System.assert(leadList.size() == 1);
		Lead leadAssert = leadList[0];
		
		System.assertEquals(leadDealer01.DealerSalesManager__c, leadAssert.OwnerId);
	}
	
	@isTest static void shouldCheckLeadAsExpired() {
		Account routingDealer = AccountBuild.createDealer('9077');
		Database.insert(routingDealer);
		
		Account leadDealer01 = AccountDAO.getDealerByCode('9055');
		leadDealer01.LeadRouting__c = routingDealer.Id;
		Database.update(leadDealer01);
		String leadCpf = Utils.generateCpf();
		
		Lead leadWeb = LeadBuild.createLead(leadCpf, '9055', '9066', '6FG9RT');
		Database.insert(leadWeb);
		
		Test.setCreatedDate(leadWeb.Id, Datetime.now().addDays(-50));
		leadWeb.OwnerId = leadDealer01.DealerSalesManager__c;
		Database.update(leadWeb);
		
		Test.startTest();
		
		Database.executeBatch(new ForwardLeadBatch(), 1);
		
		Test.stopTest();
		
		List<Lead> leadList = LeadDAO.getLeadsByCpfCnpj(new Set<String>{leadCpf}, true);
		System.assert(leadList.size() == 1);
		Lead leadAssert = leadList[0];
		System.assert(!leadAssert.ExpiredLead__c); // deve estar falso pois o process builder altera após realizar as ações em cima do lead atrasado
	}
	
	@isTest static void shouldPostAlertToChatter() {
		Account leadDealer01 = AccountDAO.getDealerByCode('9055');
		String leadCpf = Utils.generateCpf();
		
		Lead leadWeb = LeadBuild.createLead(leadCpf, '9055', '9066', '6FG9RT');
		Database.insert(leadWeb);
		
		Test.setCreatedDate(leadWeb.Id, Datetime.now().addDays(-36));
		leadWeb.OwnerId = leadDealer01.DealerSalesManager__c;
		Database.update(leadWeb);
		
		Test.startTest();
		
		Database.executeBatch(new ForwardLeadBatch(), 1);
		
		Test.stopTest();
		
		List<Lead> leadList = LeadDAO.getLeadsByCpfCnpj(new Set<String>{leadCpf}, true);
		System.assert(leadList.size() == 1);
		Lead leadAssert = leadList[0];
		
		System.assert(!leadAssert.FiveDaysExpiration__c); // deve estar falso pois o process builder altera após realizar o post no chatter
	}
	
	@isTest static void shouldScheduleBatch() {
		Test.startTest();
		
		System.schedule('Test job', '20 30 8 10 2 ?', new ForwardLeadBatch());
		
		Test.stopTest();
	}
	
}