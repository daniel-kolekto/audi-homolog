global class LM_generateLeadController {
    public List<Account> dealerAccountList;
	public String salesRecordType {
		get { return Utils.getRecordTypeId('Lead', 'Sales'); }
	}
	public String corpSalesRecordType {
		get { return Utils.getRecordTypeId('Lead', 'CorporateSales'); }
	}
	public String afterSalesRecordType {
		get { return Utils.getRecordTypeId('Lead', 'AfterSales'); }
	}
	public Boolean isSales { get { return lead.RecordTypeId == salesRecordType; } }
    public Boolean isCorporateSales { get { return lead.RecordTypeId == corpSalesRecordType; } }
    public Boolean isAfterSales { get { return lead.RecordTypeId == afterSalesRecordType; } }
	
    public String recordTypeDeveloperNameSelected {get;set;}
    public String showHeader {get;set;}
    public Lead lead {get;set;}
    public Lead leadUpdate {get;set;}
    public Id dealerSelectedId {get;set;}
    private Account customerAcount;
    public Case caso {get;set;}
    public String portalCode {get;set;}
    public String street {get;set;}
    public String city {get;set;}
    public String accountPontsInformation {get{return this.getAccList();} set;}
    public String customerAddress {
        get{
            if(caso == null || (caso != null && String.isEmpty(caso.AccountId))) return ',,,,Brasil';
                
            Account customerAccount = AccountDAO.getPersonAccountAddress(caso.AccountId);
            
            String customerAddress = String.isNotEmpty(customerAccount.BillingStreet ) ? customerAccount.BillingStreet + ',': ',';
            customerAddress += String.isNotEmpty(customerAccount.BillingCity ) ? customerAccount.BillingCity + ',': ',';
            customerAddress += String.isNotEmpty(customerAccount.BillingState ) ? customerAccount.BillingState + ',': ',';
            customerAddress += String.isNotEmpty(customerAccount.BillingPostalCode ) ? customerAccount.BillingPostalCode  + ',': ',';
            customerAddress += 'Brasil';
            
            return customerAddress;
        } set;
    }
    public List<SelectOption> productList {
        get{
            List<SelectOption> optionList = new List<SelectOption>();
            for(Product2 product : ProductDAO.getAllProduct()) {
                optionList.add(new SelectOption(product.ProductCode,product.Name));
            }
            return optionList;
        } set;
    }
    public List<SelectOption> rtOptions {
        get {
            return new List<SelectOption>{
                new SelectOption( salesRecordType, Label.Sales ),
                new SelectOption( corpSalesRecordType, Label.CorporateSales ),
                new SelectOption( afterSalesRecordType, Label.AfterSales )
            };
        }
    }
    
    public String leadLastName {
    	get {
    		return lead.LastName;
    	}
    	set {
    		lead.LastName = value;
    		leadLastName = value;
    	}
    }
    
    
    public LM_generateLeadController() {
        Id caseId = ApexPages.currentPage().getParameters().get('caseId');
        showHeader = ApexPages.currentPage().getParameters().get('header');
        this.caso = setCase(caseId);
        lead = new Lead( RecordTypeId = salesRecordType );
        leadUpdate = new Lead();
        this.dealerAccountList = AccountDAO.getDealerAccountForMap();
        System.debug('@@@ this.dealerAccountList: ' + this.dealerAccountList);
    }
    
    
    public Case setCase(Id caseId) {
        String clasureWhere = 'WHERE Id = \''+caseId+'\'';
        return CaseDAO.getAllFields(clasureWhere)[0];
    }
    
    
    
    public String getAccList() {
        if(dealerAccountList.isEmpty())
            return '';
            
        String accountToString = writeJsonForDealers(dealerAccountList);
        return accountToString;
    }
    
    private String writeJsonForDealers(List<Account> dealerAccountList) {
        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeFieldName('Account');
        gen.writeStartArray();
        
        for(Account acc : dealerAccountList) {
            gen.writeString('new');
            gen.writeStartObject();
            gen.writeNumberField('BillingLatitude', acc.BillingLatitude != null ?acc.BillingLatitude:0);
            gen.writeNumberField('BillingLongitude', acc.BillingLongitude != null ?acc.BillingLongitude:0);
            gen.writeStringField('BillingCity', String.isNotEmpty(acc.BillingCity)?acc.BillingCity:'');
            gen.writeStringField('BillingState', String.isNotEmpty(acc.BillingState)?acc.BillingState:'');
            gen.writeStringField('BillingStreet', String.isNotEmpty(acc.BillingStreet)?acc.BillingStreet:'');
            gen.writeStringField('BillingPostalCode', String.isNotEmpty(acc.BillingPostalCode)?acc.BillingPostalCode:'');
            gen.writeStringField('Phone', String.isNotEmpty(acc.Phone)?acc.Phone:'');
            gen.writeStringField('Name', String.isNotEmpty(acc.Name)?acc.Name:'');
            gen.writeStringField('Id', String.isNotEmpty(acc.Id)?acc.Id :'');
            gen.writeStringField('DealerCode__c', String.isNotEmpty(acc.DealerCode__c)?acc.DealerCode__c :'');
            
            gen.writeEndObject();
        }
        
        gen.writeEndArray();
        gen.writeEndObject();
        String jsonValue = gen.getAsString();
        
        if(jsonValue.contains('\n'))
            jsonValue = jsonValue.replace('\n', '');
            
        return jsonValue;
    }
    
    
    
    @RemoteAction
    global static String saveLead(String leadAsString) {
        try {
            List<Lead> leadList = (List<Lead>)JSON.deserialize(leadAsString, List<Lead>.class);
            Lead leadInsert = leadList.get(0);
            
            Database.DMLOptions dmo = new Database.DMLOptions();
            dmo.AssignmentRuleHeader.useDefaultRule = true;
            
            leadInsert.setOptions(dmo);
            Database.insert(leadInsert);
            return Label.LeadSuccess;
            
        } catch(Exception e) {
            return Utils.parseErrorMessage(e);
        }
        
        return Label.LeadError;
    }
    
    //seleção de concessionária 
    
    public String stateSelected{get;set;}
    public String citySelected{get;set;}
    public String dealerSelected{get;set;}
    
    public class StateSecondDealer{
        public String state{get;set;}
        public String city{get;set;}
        public String accountId{get;set;}
        public String accountName{get;set;}
    }
    
    public List<StateSecondDealer> stateSecondDealer{ 
        get{
            List<StateSecondDealer> stateList = new List<StateSecondDealer>();
            Set<String> stateSet = new Set<String>();
            System.debug('### dealerAccountList '+dealerAccountList);
            for(Account dealer : dealerAccountList){
                if(String.isNotEmpty(dealer.BillingState) && !stateSet.contains(dealer.BillingState)){
                    StateSecondDealer instance = new StateSecondDealer();
                    instance.state = dealer.BillingState ;
                    stateList.add(instance);
                    stateSet.add(dealer.BillingState);
                }
            }
            System.debug('### stateList '+stateList);
            return stateList;
        }
    }
    
    
    public List<StateSecondDealer> citySecondDealer{
        get{
            List<StateSecondDealer> cityList = new List<StateSecondDealer>();
            Set<String> citySet = new Set<String>();
            for(Account dealer : dealerAccountList){
                if(String.isNotEmpty(dealer.BillingState) && String.isNotEmpty(dealer.BillingCity) && String.isNotEmpty(stateSelected)
                   && stateSelected.equals(dealer.BillingState) && !citySet.contains(dealer.BillingCity)
                  ){
                    StateSecondDealer instance = new StateSecondDealer();
                    instance.city = dealer.BillingCity ;
                    cityList.add(instance);
                    citySet.add(dealer.BillingCity);
                }
            }
            return cityList;  
        }
    }
    
    public List<StateSecondDealer> dealerSecondDealer{
        get{
            List<StateSecondDealer> dealerList = new List<StateSecondDealer>();
            Set<String> dealerSet = new Set<String>();
            for(Account dealer : dealerAccountList){
                if(String.isNotEmpty(dealer.BillingCity) && String.isNotEmpty(citySelected)
                   && citySelected.equals(dealer.BillingCity) && !dealerSet.contains(dealer.Id)
                  ){
                    StateSecondDealer instance = new StateSecondDealer();
                    instance.accountName = dealer.Name;
                    instance.accountId = dealer.DealerCode__c; 
                    dealerList.add(instance);
                    dealerSet.add(dealer.Id);
                }
            }
            return dealerList; 
        }
    }
    
    public void setDealer(){}
    
}