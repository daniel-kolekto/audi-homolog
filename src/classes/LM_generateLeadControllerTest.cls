@isTest
public class LM_generateLeadControllerTest {
	
	@testSetup static void createTestData(){
        Account dealerAcct01 = AccountBuild.createDealer('9055');
        Account dealerAcct02 = AccountBuild.createDealer('9066');
        Account customer01 = AccountBuild.createCustomer('PF');
        Database.insert(new List<Account>{dealerAcct01, dealerAcct02, customer01});
        
        Product2 vehicleProduct = ProductBuild.createVehicle('6FG9RT');
        Database.insert(vehicleProduct);
        
        Vehicle__c vehicle = VehicleBuild.createVehicle('1GTDC14N2FJ506808', 'EMT3194', vehicleProduct.Id);
        Database.insert(vehicle);
        
        Database.insert(new Ownership__c(Name = 'test', Vehicle__c = vehicle.Id, Customer__c = customer01.Id, Status__c = 'ACTIVE'));
        
        
        Case case01 = CaseBuild.createCustomerCareCase();
        case01.AccountId = customer01.Id;
        case01.Dealer__c = dealerAcct01.Id;
        case01.Vehicle__c = vehicle.Id;
        
        Database.insert(case01);
    }
    
	
	@isTest static void testGenerateLeadController01(){
		List<Case> lstCase = [SELECT Id, CaseNumber FROM Case];
		Case caseTest = !lstCase.isEmpty() ? lstCase.get(0) : null;
		System.assert(caseTest != null);
		
        Test.setCurrentPageReference(new PageReference('/apex/LM_generateLead'));
        System.currentPageReference().getParameters().put('caseId', caseTest.Id);
        System.currentPageReference().getParameters().put('header', 'true');
        
        Test.startTest();
        
        LM_generateLeadController cont = new LM_generateLeadController();
        
        System.assert(cont.dealerAccountList != null);
        System.assert(!cont.dealerAccountList.isEmpty());
        System.assert(String.isNotBlank(cont.accountPontsInformation));
        
        
        Boolean isSales = cont.isSales;
        Boolean isCorporateSales = cont.isCorporateSales;
        Boolean isAfterSales = cont.isAfterSales;
        
        cont.recordTypeDeveloperNameSelected = '';
        cont.dealerSelectedId = null;
        cont.portalCode = '';
        cont.street = '';
        cont.city = '';
        
        String customerAddress = cont.customerAddress;
        cont.leadLastName = 'Smith';
        String leadLastName = cont.leadLastName;
        
        List<SelectOption> productList = cont.productList;
        List<SelectOption> rtOptions = cont.rtOptions;
        
        List<LM_generateLeadController.StateSecondDealer> stateSecondDealer = cont.stateSecondDealer;
        List<LM_generateLeadController.StateSecondDealer> citySecondDealer = cont.citySecondDealer;
        List<LM_generateLeadController.StateSecondDealer> dealerSecondDealer = cont.dealerSecondDealer;
        
        cont.setDealer();
        
        Test.stopTest();
    }
	
	@isTest static void testGenerateLeadController02(){
        List<Case> lstCase = [SELECT Id, CaseNumber FROM Case];
        Case caseTest = !lstCase.isEmpty() ? lstCase.get(0) : null;
        System.assert(caseTest != null);
        
        Test.setCurrentPageReference(new PageReference('/apex/LM_generateLead'));
        System.currentPageReference().getParameters().put('caseId', caseTest.Id);
        System.currentPageReference().getParameters().put('header', 'true');
        
        String leadCpf = Utils.generateCpf();
        Account leadDealer01 = AccountDAO.getDealerByCode('9055');
        Account leadDealer02 = AccountDAO.getDealerByCode('9066');
        Product2 leadProduct = ProductDAO.getProductByCode('6FG9RT');
        
        Test.startTest();
        
        Lead leadSave = LeadBuild.createLead(leadCpf, '9055', '9066', '6FG9RT');
        LM_generateLeadController.saveLead(JSON.serialize(new List<Lead>{leadSave}));
        
        Test.stopTest();
    }
    
}