/**
*Project: Lead Management
*/
global with sharing class LM_searchDealerController {
	
	private Account customerAccount;
	public Case caso{get;set;}
	public String portalCode{get;set;}
	public String street{get;set;}
	public String city{get;set;}
	public String accountPontsInformation {get{return getAccList();} set;}
	public String customerAddress{
		get{
			if(caso == null || (caso != null && String.isEmpty(caso.AccountId)))
				return '';

			Account customerAccount = AccountDAO.getPersonAccountAddress(caso.AccountId); 

			String customerAddress = String.isNotEmpty(customerAccount.BillingStreet ) ? customerAccount.BillingStreet + ',': ',';	
			customerAddress += String.isNotEmpty(customerAccount.BillingCity ) ? customerAccount.BillingCity + ',': ',';	
			customerAddress += String.isNotEmpty(customerAccount.BillingState ) ? customerAccount.BillingState + ',': ',';	
			customerAddress += String.isNotEmpty(customerAccount.BillingPostalCode ) ? customerAccount.BillingPostalCode  + ',': ',';	
			customerAddress += 'Brasil';	
			
			return customerAddress;	
		}set;
	}
	

	public LM_searchDealerController(ApexPages.StandardController stdController) {
		this.caso = CaseDAO.getCaseById(stdController.getId(), true);
	}



	public String getAccList(){
		List<Account> dealerAccountList = AccountDAO.getDealerAccountForMap();
		 

		if(dealerAccountList.isEmpty())
			return '';

		String accountToString = writeJsonForDealers(dealerAccountList);
		return accountToString;
	}

	private String writeJsonForDealers(List<Account> dealerAccountList){
		JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeFieldName('Account');
        gen.writeStartArray();
        
        for(Account acc : dealerAccountList){
        	gen.writeString('new');
            gen.writeStartObject();
            gen.writeNumberField('BillingLatitude', acc.BillingLatitude != null ?acc.BillingLatitude:0);
            gen.writeNumberField('BillingLongitude', acc.BillingLongitude != null ?acc.BillingLongitude:0);
            gen.writeStringField('BillingCity', String.isNotEmpty(acc.BillingCity)?acc.BillingCity:'');
            gen.writeStringField('BillingState', String.isNotEmpty(acc.BillingState)?acc.BillingState:'');
            gen.writeStringField('BillingStreet', String.isNotEmpty(acc.BillingStreet)?acc.BillingStreet:'');
            gen.writeStringField('BillingPostalCode', String.isNotEmpty(acc.BillingPostalCode)?acc.BillingPostalCode:'');
            gen.writeStringField('Phone', String.isNotEmpty(acc.Phone)?acc.Phone:'');
            gen.writeStringField('Name', String.isNotEmpty(acc.Name)?acc.Name:'');
            gen.writeStringField('Id', String.isNotEmpty(acc.Id)?acc.Id :'');

            gen.writeEndObject();
        }
        
        gen.writeEndArray();
        gen.writeEndObject();
        String jsonValue = gen.getAsString();
        
        if(jsonValue.contains('\n'))
        	jsonValue = jsonValue.replace('\n', '');

		return jsonValue;
	}


	@RemoteAction
    global static String sendSMS(String accountId, String message){
    	
    	message = changeSpecialCharacter(message);
    	System.debug('### accountId  '+accountId);
    	System.debug('### message  '+message);
    	if(message.contains('Erro') || !numberCharacteres(message))
    		return Label.InvalidSMSMessage;

    	Account customer = AccountDAO.getAccountById( (id)accountId, true );
    	if(customer == null) return Label.InvalidCustomer;
    	System.debug('### customer: ' + customer);
    	
    	String mobilePhone = validateMobilePhone(customer.PersonMobilePhone);
    	System.debug('### mobilePhone '+mobilePhone);
    	if(String.isEmpty(mobilePhone) || (String.isNotEmpty(mobilePhone) && mobilePhone.contains('Error')))
    		return mobilePhone;

    	String response = SFMC_MobileConnect.sendSMS(mobilePhone,message);

		return response;
	}


	public static String changeSpecialCharacter(String smsText){
		smsText = smsText.contains('á') ? smsText.replaceAll('á', 'a') : smsText;
		smsText = smsText.contains('é') ? smsText.replaceAll('é', 'e') : smsText;
		smsText = smsText.contains('í') ? smsText.replaceAll('í', 'i') : smsText;
		smsText = smsText.contains('ó') ? smsText.replaceAll('ó', 'o') : smsText;
		smsText = smsText.contains('ú') ? smsText.replaceAll('ú', 'u') : smsText;
		smsText = smsText.contains('à') ? smsText.replaceAll('à', 'a') : smsText;
		smsText = smsText.contains('è') ? smsText.replaceAll('è', 'e') : smsText;
		smsText = smsText.contains('ì') ? smsText.replaceAll('ì', 'i') : smsText;
		smsText = smsText.contains('ò') ? smsText.replaceAll('ò', 'o') : smsText;
		smsText = smsText.contains('ù') ? smsText.replaceAll('ù', 'u') : smsText;
		smsText = smsText.contains('â') ? smsText.replaceAll('â', 'a') : smsText;
		smsText = smsText.contains('ê') ? smsText.replaceAll('ê', 'e') : smsText;
		smsText = smsText.contains('î') ? smsText.replaceAll('î', 'i') : smsText;
		smsText = smsText.contains('ô') ? smsText.replaceAll('ô', 'o') : smsText;
		smsText = smsText.contains('û') ? smsText.replaceAll('û', 'u') : smsText;
		smsText = smsText.contains('ã') ? smsText.replaceAll('ã', 'a') : smsText;
		smsText = smsText.contains('õ') ? smsText.replaceAll('õ', 'o') : smsText;
		smsText = smsText.contains('ç') ? smsText.replaceAll('ç', 'c') : smsText;
		
		Matcher myMatcher = Pattern.compile('^[a-zA-Z0-9!,.?%()\\-:$ ]*$').matcher(smsText);
		if(!myMatcher.matches())
			return 'Erro: Invalid message';
		
		return 	smsText;
	}

	public static boolean numberCharacteres(String value){
		if(String.isEmpty(value)) return  false;
		return value.length() <= 255;
	}

	public static String validateMobilePhone(String mobilePhone){
		System.debug('### step 1 - '+mobilePhone);
		if(String.isEmpty(mobilePhone) || (String.isNotEmpty(mobilePhone) && mobilePhone.length() < 11))
			return 'Error: '+Label.InvalidMobilePhone;
		System.debug('### step 2');	
		if(mobilePhone.substring(0,2).contains('55') && mobilePhone.length() == 13)
			return mobilePhone;
		System.debug('### step 3');	
		if(mobilePhone.substring(0,3).contains('+55') && mobilePhone.length() == 14)
			return mobilePhone.replace('+55','55');
		System.debug('### step 4');
		return '55'+mobilePhone;
	}

}