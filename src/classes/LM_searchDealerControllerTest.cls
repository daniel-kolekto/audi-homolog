@isTest
public class LM_searchDealerControllerTest {
	
	@testSetup static void createTestData(){
		Database.insert(new MarketingCloudConfiguration__c(
			Name = 'default',
			ClienteId__c = 't6thssn7p8uk0dzs9s82jrp6',
			ClientSecret__c = 'Wpqp8Ra5aXAOaVyjAyHghSaS',
			MessageId__c = 'Mjo3ODow'
		));
		
        Account dealerAcct01 = AccountBuild.createDealer('9055');
        Account dealerAcct02 = AccountBuild.createDealer('9066');
        Account customer01 = AccountBuild.createCustomer('PF');
        Database.insert(new List<Account>{dealerAcct01, dealerAcct02, customer01});
        
        Product2 vehicleProduct = ProductBuild.createVehicle('6FG9RT');
        Database.insert(vehicleProduct);
        
        Vehicle__c vehicle = VehicleBuild.createVehicle('1GTDC14N2FJ506808', 'EMT3194', vehicleProduct.Id);
        Database.insert(vehicle);
        
        Database.insert(new Ownership__c(Name = 'test', Vehicle__c = vehicle.Id, Customer__c = customer01.Id, Status__c = 'ACTIVE'));
        
        
        Case case01 = CaseBuild.createCustomerCareCase();
        case01.AccountId = customer01.Id;
        case01.Dealer__c = dealerAcct01.Id;
        case01.Vehicle__c = vehicle.Id;
        
        Database.insert(case01);
    }
    
	
	@isTest static void testSearchDealerController01(){
		List<Case> lstCase = [SELECT AccountId, CaseNumber FROM Case];
		Case caseTest = !lstCase.isEmpty() ? lstCase.get(0) : null;
		System.assert(caseTest != null);
        
        Test.startTest();
        
        LM_searchDealerController cont = new LM_searchDealerController(new ApexPages.StandardController(caseTest));
        
        String accountPontsInformation = cont.accountPontsInformation;
        String customerAddress = cont.customerAddress;
        
        LM_searchDealerController.sendSMS(String.valueOf(caseTest.AccountId), 'Oí cômo vãi vócê?');
        
        
        Test.stopTest();
    }
	
}