public without sharing class LeadBO {
    
    public static void setLeadAccountsAndProduct(List<Lead> triggerNew){
        List<Account> listAccountUpsert = new List<Account>();
        Set<String> orphanLeadSet = new Set<String>();
        
        // Dealer Maps
        Map<String, Account> dealerMapByCode = AccountDAO.getDealerMapByCode( SetUtils.getLeadDealerCodeSet(triggerNew) );
        Map<Id, Account> dealerMapById = AccountDAO.getAccountMapById( SetUtils.getLeadDealerIdSet(triggerNew) );
        
        // Product Maps
        Map<String, Product2> productMapByCode = ProductDAO.getProductMapByCode( SetUtils.getLeadProductCodeSet(triggerNew) );
        Map<Id, Product2> productMapById = ProductDAO.getProductMapById( SetUtils.getLeadProductIdSet(triggerNew) );
        
        // Customer Map
        Map<String, Account> customerAcctMapByCpfCnpj = AccountDAO.getAccountMapByCpfCnpj(SetUtils.getLeadCpfCnpjSet(triggerNew));
        Map<Id, Account> customerAcctMapById = AccountDAO.getAccountMapById(SetUtils.getLeadCustomerIdSet(triggerNew));
        
        for(Lead lead : triggerNew){
            Account newCustomerAcct = customerAcctMapByCpfCnpj.containsKey(lead.CpfCnpj__c) ? customerAcctMapByCpfCnpj.get(lead.CpfCnpj__c) : null;
            Account oldCustomerAcct = customerAcctMapById.containsKey(lead.AccountLead__c) ? customerAcctMapById.get(lead.AccountLead__c) : null;
            Boolean isCustomerChanged = oldCustomerAcct != null && oldCustomerAcct.CpfCnpj__c != lead.CpfCnpj__c;
            
            Account leadMajorDealer = dealerMapByCode.containsKey(lead.MajorDealerCode__c) ? dealerMapByCode.get(lead.MajorDealerCode__c) : dealerMapById.containsKey(lead.MajorDealer__c) ? dealerMapById.get(lead.MajorDealer__c) : null;
            Account leadOptDealer = dealerMapByCode.containsKey(lead.OptionalDealerCode__c) ? dealerMapByCode.get(lead.OptionalDealerCode__c) : dealerMapById.containsKey(lead.OptionalDealer__c) ? dealerMapById.get(lead.OptionalDealer__c) : null;
            Product2 leadProduct = productMapById.containsKey(lead.ProductInterest__c) ? productMapById.get(lead.ProductInterest__c) : productMapByCode.containsKey(lead.ProductCode__c) ? productMapByCode.get(lead.ProductCode__c) : null;
            
            if(leadMajorDealer != null){
                lead.MajorDealerCode__c = leadMajorDealer.DealerCode__c;
                lead.MajorDealer__c = leadMajorDealer.Id;
                lead.ScheduledDealer__c = lead.ScheduledDealer__c == null ? leadMajorDealer.Id : lead.ScheduledDealer__c;
                lead.LeadManagerEmail__c = lead.RecordTypeId == Utils.getRecordTypeId('Lead', 'AfterSales') ? leadMajorDealer.DealerAfterSalesManager__r.Email : leadMajorDealer.DealerSalesManager__r.Email;
            }
            
            if(leadOptDealer != null){
                lead.OptionalDealerCode__c = leadOptDealer.DealerCode__c;
                lead.OptionalDealer__c = leadOptDealer.Id;
            }
            
            if(leadProduct != null){
                lead.ProductCode__c = leadProduct.ProductCode;
                lead.ProductInterest__c = leadProduct.Id;
            }
            
            if(newCustomerAcct != null){
                lead.AccountLead__c = newCustomerAcct.Id;
                
            } else if('Buy'.equalsIgnoreCase(lead.Interest__c) && String.isNotEmpty(lead.CpfCnpj__c)) {
                newCustomerAcct = LeadUtils.createAccountForLead(lead); // Gera nova conta para o Lead com o mesmo CPF/CNPJ
                listAccountUpsert.add(newCustomerAcct);
                orphanLeadSet.add(lead.CpfCnpj__c);
                lead.AccountLead__c = null;
            }
            
            if(isCustomerChanged){
                listAccountUpsert.add( new Account(Id = oldCustomerAcct.Id, VerifyAccount__c = true) );
            }
        }
        
        
        System.debug('@@@ setLeadAccountsAndProduct listAccountUpsert: \n' + JSON.serializePretty(listAccountUpsert));
        
        if(!listAccountUpsert.isEmpty())
            Database.upsert(listAccountUpsert);
        
        if(!orphanLeadSet.isEmpty()){
            customerAcctMapByCpfCnpj = AccountDAO.getAccountMapByCpfCnpj(orphanLeadSet);
            
            for(Lead lead : triggerNew){
                if(lead.AccountLead__c == null && customerAcctMapByCpfCnpj.containsKey(lead.CpfCnpj__c)){
                    lead.AccountLead__c = customerAcctMapByCpfCnpj.get(lead.CpfCnpj__c).Id;
                }
            }
        }
        
        
        List<Contact> listContactInsert = new List<Contact>();
        customerAcctMapById = AccountDAO.getAccountMapById(SetUtils.getLeadCustomerIdSet(triggerNew));
        
        for(Lead lead : triggerNew){
            if(!customerAcctMapById.containsKey(lead.AccountLead__c)) continue;
            Account acctLead = customerAcctMapById.get(lead.AccountLead__c);
            Boolean hasContact = !acctLead.Contacts.isEmpty();
            
            if(hasContact){
                for(Contact cont : acctLead.Contacts){
                    if(cont.Email == lead.Email) {
                        hasContact = true;
                        break;
                    } else {
                        hasContact = false;
                    }
                }
            }
            
            if(!hasContact && acctLead.RecordTypeId == Utils.getRecordTypeId('Account', 'LegalPerson'))
                listContactInsert.add( LeadUtils.createContactForLegalPersonAccountLead(lead) );
        }
        
        if(!listContactInsert.isEmpty())
            Database.insert(listContactInsert);
    }
    
    public static void leadInsertTreatment(List<Lead> triggerNew){
        Set<String> leadDuplicateSet = new Set<String>();
        Id businessHoursId = LeadUtils.getBusinessHoursIdTreated();
        Integer numberOfDays = CustomMetadataDAO.getDuplicateLeadLimitDays();
        List<String> slaNurturingLimits = CustomMetadataDAO.getLeadNurturingLimitSLA().split(';');
        Integer nurturingLimit01 = Integer.valueOf(slaNurturingLimits[0]),
                nurturingLimit02 = Integer.valueOf(slaNurturingLimits[1]),
                nurturingLimit03 = Integer.valueOf(slaNurturingLimits[2]),
                nurturingLimit04 = Integer.valueOf(slaNurturingLimits[3]);
        
        for(Lead lead : LeadDAO.getLeadsByCpfCnpjInLastDays(SetUtils.getLeadCpfCnpjSet(triggerNew), numberOfDays)){
            leadDuplicateSet.add(lead.CpfCnpj__c + lead.MajorDealerCode__c);
        }
        
        
        for(Lead lead : triggerNew){
            lead.CpfCnpj__c = Utils.limparFormatacaoCpfCnpj(lead.CpfCnpj__c);
            lead.IsDuplicate__c = leadDuplicateSet.contains(lead.CpfCnpj__c + lead.MajorDealerCode__c);
            
            if(businessHoursId != null){
                //lead.SlaLimitNurturing01__c = BusinessHours.add( businessHoursId, Datetime.now(), (nurturingLimit01 * 60 * 1000) );
                //lead.SlaLimitNurturing02__c = BusinessHours.add( businessHoursId, Datetime.now(), (nurturingLimit02 * 60 * 1000) );
                //lead.SlaLimitNurturing03__c = BusinessHours.add( businessHoursId, Datetime.now(), (nurturingLimit03 * 60 * 1000) );
                //lead.SlaLimitNurturing04__c = BusinessHours.add( businessHoursId, Datetime.now(), (nurturingLimit04 * 60 * 1000) );
            }
        }
    }
    
    public static void leadUpdateTreatment(List<Lead> triggerNew, Map<Id, Lead> triggerOldMap){
        Id businessHoursId = LeadUtils.getBusinessHoursIdTreated();
        List<String> slaDealerLimits = CustomMetadataDAO.getLeadDealerLimitSLA().split(';');
        Integer dealerLimit01 = Integer.valueOf(slaDealerLimits[0]),
                dealerLimit02 = Integer.valueOf(slaDealerLimits[1]),
                dealerLimit03 = Integer.valueOf(slaDealerLimits[2]),
                dealerLimit04 = Integer.valueOf(slaDealerLimits[3]);
        
        
        for(Lead leadNew : triggerNew){
            if(businessHoursId == null) continue;
            
            if( LeadUtils.isLeadClosing(leadNew, triggerOldMap.get(leadNew.Id)) ){
                leadNew.LeadClosingDate__c = Datetime.now();
                
                if(leadNew.LeadForwardingDate__c != null) {
                    leadNew.SlaDealerTotalTime__c = BusinessHours.diff(businessHoursId, leadNew.LeadForwardingDate__c, Datetime.now()) / 1000 / 60;
                } else {
                    leadNew.SlaNurturingTotalTime__c = BusinessHours.diff(businessHoursId, leadNew.CreatedDate, Datetime.now()) / 1000 / 60;
                }
            }
            
            if(leadNew.LeadForwardingDate__c != null){
                leadNew.SlaLimitDealer01__c = BusinessHours.add( businessHoursId, leadNew.LeadForwardingDate__c, (dealerLimit01 * 60 * 1000) );
                leadNew.SlaLimitDealer02__c = BusinessHours.add( businessHoursId, leadNew.LeadForwardingDate__c, (dealerLimit02 * 60 * 1000) );
                leadNew.SlaLimitDealer03__c = BusinessHours.add( businessHoursId, leadNew.LeadForwardingDate__c, (dealerLimit03 * 60 * 1000) );
                leadNew.SlaLimitDealer04__c = BusinessHours.add( businessHoursId, leadNew.LeadForwardingDate__c, (dealerLimit04 * 60 * 1000) );
            }
        }
    }



    public static void setChagendFieldsSLADate(List<Lead> leadList, Map<Id, Lead> triggerOldMap){
        Map<Id,String> leadOwnerTypeMap = LeadDAO.getOwnerType(leadList);
        Datetime now = System.now();
        for(Lead lead : leadList){
            Lead leadOldMap = triggerOldMap.get(lead.Id);
             
            if(String.isNotEmpty(lead.Status) && lead.Status.equals('New') && String.isNotEmpty(lead.OwnerId) &&
               !lead.OwnerId.equals(leadOldMap.OwnerId) && lead.DateChangeStatusForNew__c == null && 
              	leadOwnerTypeMap != null && leadOwnerTypeMap.containsKey(lead.Id) &&
               String.isNotEmpty(leadOwnerTypeMap.get(lead.Id)) &&
               leadOwnerTypeMap.get(lead.Id).equals('Queue') && !lead.CreatedById.equals(UserInfo.getUserId())
              ){
            	lead.DateChangeStatusForNew__c = now;
                lead.ChangedToUser__c = true;  
            } 

            if(!lead.Status.equals('ContactPostponedOne') && leadOldMap.Status.equals('ContactPostponedOne') && 
                lead.OwnerId.equals(leadOldMap.OwnerId) && lead.DateChangeStatusForContactedDelayed1__c == null){
                lead.DateChangeStatusForContactedDelayed1__c = now;
            }

            if(!lead.Status.equals('ContactPostponedTwo') && leadOldMap.Status.equals('ContactPostponedTwo') && 
                lead.OwnerId.equals(leadOldMap.OwnerId) && lead.DateChangeStatusForContactedDelayed2__c == null){
                lead.DateChangeStatusForContactedDelayed2__c = now;
            }

            if(!lead.Status.equals('NonContact') && leadOldMap.Status.equals('NonContact') && 
                lead.OwnerId.equals(leadOldMap.OwnerId) && lead.DateChangeStatusForNonContact__c == null){
                lead.DateChangeStatusForNonContact__c = now;
            }
        }
    }




    public static void setSLAFields(List<Lead> leadList, Map<Id, Lead> triggerOldMap){
        System.debug('### leadList '+leadList);
        System.debug('### triggerOldMap '+triggerOldMap);

        Id businessHoursId = LeadUtils.getBusinessHoursIdTreated();
        Map<String,SLA_Control__mdt> metadataList =  CustomMetadataDAO.getSLAControl();

        System.debug('### businessHoursId '+businessHoursId);
        System.debug('### metadataList '+metadataList);

        if(String.isNotEmpty(businessHoursId) && metadataList != null && !metadataList.isEmpty()){
            Datetime now = System.now();
            for(Lead lead : leadList){
                System.debug('### lead.Status '+lead.Status);


                if(Trigger.isInsert && String.isNotEmpty(lead.Status) && lead.Status.equals('New') && metadataList.containsKey('New')){  
                    SLA_Control__mdt  metadata = metadataList.get('New');
                    lead.SlaLimitNurturing01__c =  getDateFromBusinessHours(now, metadata.SLAFullGreen__c,  businessHoursId);
                    lead.SlaLimitNurturing02__c =  getDateFromBusinessHours(now, metadata.SLAHalfGreen__c,  businessHoursId);
                    lead.SlaLimitNurturing03__c = getDateFromBusinessHours(now, metadata.SLAFullYellow__c, businessHoursId);
                    lead.SlaLimitNurturing04__c = getDateFromBusinessHours(now, metadata.SLAHalfYellow__c, businessHoursId);

                }else if(Trigger.isUpdate && triggerOldMap != null){
                    Lead oldLead = triggerOldMap.get(lead.Id);
	
                    System.debug('### 0');
                    if(lead.Status.equals('ContactPostponedOne') && !oldLead.Status.equals('ContactPostponedOne') && metadataList.containsKey('Contact postponed one')){
                        System.debug('### 1');
                        SLA_Control__mdt  metadata = metadataList.get('Contact postponed one');
                        lead.SlaLimitNurturing01__c =  getDateFromBusinessHours(now, metadata.SLAFullGreen__c,  businessHoursId);
                        lead.SlaLimitNurturing02__c =  getDateFromBusinessHours(now, metadata.SLAHalfGreen__c,  businessHoursId);
                        lead.SlaLimitNurturing03__c = getDateFromBusinessHours(now, metadata.SLAFullYellow__c, businessHoursId);
                        lead.SlaLimitNurturing04__c = getDateFromBusinessHours(now, metadata.SLAHalfYellow__c, businessHoursId);
                    
                    }else if(lead.Status.equals('ContactPostponedTwo') && !oldLead.Status.equals('ContactPostponedTwo') && metadataList.containsKey('Contact postponed two')){
                        System.debug('### 2');
                        SLA_Control__mdt  metadata = metadataList.get('Contact postponed two');
                        lead.SlaLimitNurturing01__c =  getDateFromBusinessHours(now, metadata.SLAFullGreen__c,  businessHoursId);
                        lead.SlaLimitNurturing02__c =  getDateFromBusinessHours(now, metadata.SLAHalfGreen__c,  businessHoursId);
                        lead.SlaLimitNurturing03__c = getDateFromBusinessHours(now, metadata.SLAFullYellow__c, businessHoursId);
                        lead.SlaLimitNurturing04__c = getDateFromBusinessHours(now, metadata.SLAHalfYellow__c, businessHoursId);
                    }

                }

            }
        }

    }

    private static Datetime getDateFromBusinessHours(Datetime startDate,  Decimal  period, Id businessHoursId){
        Long periodAux = Long.valueOf(String.valueOf(period));
        return BusinessHours.add(businessHoursId, startDate, (periodAux * 60 * 1000));
    }

    
}