@isTest
public class LeadBOTest {
    
    @testSetup static void createTestData(){
        Account dealerAcct01 = AccountBuild.createDealer('9055');
        Account dealerAcct02 = AccountBuild.createDealer('9066');
        Database.insert(new List<Account>{dealerAcct01, dealerAcct02});
        
        Product2 vehicleProduct = ProductBuild.createVehicle('6FG9RT');
        Database.insert(vehicleProduct);
    }
    
    
    @isTest static void shouldTreatLeadOnInsert(){
        String leadCpf = Utils.generateCpf();
        Account leadDealer01 = AccountDAO.getDealerByCode('9055');
        Account leadDealer02 = AccountDAO.getDealerByCode('9066');
        Product2 leadProduct = ProductDAO.getProductByCode('6FG9RT');
        
        Test.startTest();
        
        Database.insert( LeadBuild.createLead(leadCpf, '9055', '9066', '6FG9RT') );
        
        Test.stopTest();
        
        List<Lead> leadList = LeadDAO.getLeadsByCpfCnpj(new Set<String>{leadCpf}, true);
        System.assert(leadList.size() == 1);
        Lead leadWeb = leadList[0];
        
        System.assertEquals(leadDealer01.Id, leadWeb.MajorDealer__c);
        System.assertEquals(leadDealer02.Id, leadWeb.OptionalDealer__c);
        System.assertEquals(leadProduct.Id, leadWeb.ProductInterest__c);
        
        System.assert(AccountDAO.getAccountByCpfCnpj(leadCpf, false) != null);
        
        System.assert(!leadWeb.IsDuplicate__c);
        System.assert(leadWeb.SlaLimitNurturing01__c != null);
        System.assert(leadWeb.SlaLimitNurturing02__c != null);
        System.assert(leadWeb.SlaLimitNurturing03__c != null);
        System.assert(leadWeb.SlaLimitNurturing04__c != null);
    }
    
    @isTest static void shouldTreatLeadOnUpdate(){
        String leadCpf = Utils.generateCpf();
        
        Database.insert( LeadBuild.createLead(leadCpf, '9055', '9066', '6FG9RT') );
        
        List<Lead> leadList = LeadDAO.getLeadsByCpfCnpj(new Set<String>{leadCpf}, false);
        System.assert(leadList.size() == 1);
        Lead leadWeb = leadList[0];
        
        Test.startTest();
        
        leadWeb.LeadForwardingDate__c = Datetime.now().addMinutes(20);
        leadWeb.Status = 'Contacted';
        Database.update(leadWeb);
        
        Test.stopTest();
        
        leadList = LeadDAO.getLeadsByCpfCnpj(new Set<String>{leadCpf}, true);
        System.assert(leadList.size() == 1);
        Lead leadAssert = leadList[0];
        
        System.assert(leadAssert.SlaLimitDealer01__c != null);
        System.assert(leadAssert.SlaLimitDealer02__c != null);
        System.assert(leadAssert.SlaLimitDealer03__c != null);
        System.assert(leadAssert.SlaLimitDealer04__c != null);
    }
    
    @isTest static void shouldFlagVerifyAccount(){
        String leadCpf01 = Utils.generateCpf();
        String leadCpf02 = Utils.generateCpf();
        
        Database.insert( LeadBuild.createLead(leadCpf01, '9055', '9066', '6FG9RT') );
        
        List<Lead> leadList = LeadDAO.getLeadsByCpfCnpj(new Set<String>{leadCpf01}, false);
        System.assert(leadList.size() == 1);
        Lead leadWeb = leadList[0];
        
        Test.startTest();
        
        leadWeb.CpfCnpj__c = leadCpf02;
        Database.update(leadWeb);
        
        Test.stopTest();
        
        leadList = LeadDAO.getLeadsByCpfCnpj(new Set<String>{leadCpf02}, true);
        System.assert(leadList.size() == 1);
        Lead leadAssert = leadList[0];
        
        Account leadAcct01 = AccountDAO.getAccountByCpfCnpj(leadCpf01, false);
        Account leadAcct02 = AccountDAO.getAccountByCpfCnpj(leadCpf02, false);
        
        System.debug('leadAcct01 in test: ' + JSON.serializePretty(leadAcct01));
        System.debug('leadAcct02 in test: ' + JSON.serializePretty(leadAcct02));
        
        System.assertEquals(leadAcct02.Id, leadAssert.AccountLead__c);
        System.assert(!leadAcct01.VerifyAccount__c); // deve estar falso pois o process builder altera após realizar o post no chatter
    }
    
    @isTest static void shouldSetLeadAsDuplicate(){
        String leadCpf = Utils.generateCpf();
        
        Database.insert( LeadBuild.createLead(leadCpf, '9055', '9066', '6FG9RT') );
        
        Test.startTest();
        
        Database.insert( LeadBuild.createLead(leadCpf, '9055', '9066', '6FG9RT') );
        
        Test.stopTest();
        
        List<Lead> leadList = LeadDAO.getLeadsByCpfCnpj(new Set<String>{leadCpf}, true);
        
        System.assert(leadList.size() == 2);
        System.assert(leadList[0].IsDuplicate__c || leadList[1].IsDuplicate__c);
    }
    
    @isTest static void shouldNotCreateAccountForLead(){
        Test.startTest();
        
        Database.insert( LeadBuild.createLead(null, '9055', '9066', '6FG9RT') );
        
        Test.stopTest();
        
        List<Lead> leadList = [ SELECT AccountLead__c FROM Lead ];
        System.assert(leadList.size() == 1);
        Lead leadAssert = leadList[0];
        
        System.assert(leadAssert.AccountLead__c == null);
    }
    
}