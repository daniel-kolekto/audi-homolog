@isTest
public class LeadBuild {
    
    public static final String LEAD_DEFAULT_FIRSTNAME = 'Daniel';
    public static final String LEAD_DEFAULT_LASTNAME = 'Soares';
    public static final String LEAD_DEFAULT_CPFCNPJ = Utils.generateCnpj();
    
    
    public static Lead createLead(String firstName, String lastName, String leadCpfCnpj, String majorDealerCode, String optDealerCode, String prodCode){
        return getNewLead(firstName, lastName, leadCpfCnpj, majorDealerCode, optDealerCode, prodCode);
    }
    
    public static Lead createLead(String leadCpfCnpj, String majorDealerCode, String optDealerCode, String prodCode){
        return getNewLead(LEAD_DEFAULT_FIRSTNAME, LEAD_DEFAULT_LASTNAME, leadCpfCnpj, majorDealerCode, optDealerCode, prodCode);
    }
    
    public static Lead createLead(String majorDealerCode, String optDealerCode, String prodCode){
        return getNewLead(LEAD_DEFAULT_FIRSTNAME, LEAD_DEFAULT_LASTNAME, LEAD_DEFAULT_CPFCNPJ, majorDealerCode, optDealerCode, prodCode);
    }
    
    
    public static Lead getNewLead(String firstName, String lastName, String leadCpfCnpj, String majorDealerCode, String optDealerCode, String prodCode){
        return new Lead(
            RecordTypeId = Utils.getRecordTypeId('Lead', 'CorporateSales'),
            LeadType__c = 'LegalPerson',
            Rating = 'Warm',
            LeadSource = 'WebsiteAudi',
            //SubLeadSource__c = ,
            FirstName = firstName,
            LastName = lastName,
            Company = 'kolekto tecnologia',
            //Salutation = ,
            CpfCnpj__c = leadCpfCnpj,
            //Birthdate__c = ,
            //Company = ,
            Email = 'leadtest@email.com',
            Phone = '11'+Utils.randomNumberAsString(8),
            MobilePhone = '119'+Utils.randomNumberAsString(8),
            OptinEmail__c = true,
            OptinPhone__c = true,
            PreferredContact__c = 'Email',
            //Street = 'Rua Paes Leme, 136',
            //PostalCode = '05424-150',
            //City = 'São Paulo',
            //State = 'SP',
            //Country = 'Brazil',
            Interest__c = String.isNotEmpty(leadCpfCnpj) ? 'Buy' : 'Information',
            MajorDealerCode__c = majorDealerCode,
            OptionalDealerCode__c = optDealerCode,
            ProductCode__c = prodCode,
            ModelWeb__c = 'A1 Sportback Attraction 1.4 TFSI S tronic',
            ModelVersionWeb__c = '(WFM)'
        );
    }
    
}