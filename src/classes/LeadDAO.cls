public class LeadDAO {
	
	public static List<Lead> getLeadsById(Set<Id> leadIdSet, Boolean allFields){
		if(leadIdSet.isEmpty()) return new List<Lead>();
		
		if(allFields){
			String ids = '(';
            for(Id i : leadIdSet)
                ids += '\'' + i + '\',';
            ids = ids.substring(0, ids.length()-1) + ')';
            
            return getAllFields('WHERE Id in ' + ids);
		}
		
		return [
			SELECT 	CpfCnpj__c,
					CreatedDate,
					MajorDealer__c,
					MajorDealerCode__c,
					OptionalDealer__c,
					OptionalDealerCode__c,
					ProductInterest__c,
					ProductCode__c,
					AccountLead__c
			
			FROM 	Lead
			WHERE 	Id in :leadIdSet
		];
	}
	
	public static List<Lead> getLeadsByCpfCnpj(Set<String> cpfCnpjSet, Boolean allFields){
		if(cpfCnpjSet.isEmpty()) return new List<Lead>();
		
		if(allFields){
			String cpfCnpjCondition = '(';
			for(String doc : cpfCnpjSet)
				cpfCnpjCondition += '\'' + doc + '\',';
			cpfCnpjCondition = cpfCnpjCondition.substring(0, cpfCnpjCondition.length()-1) + ')';
			
			return getAllFields('WHERE CpfCnpj__c in ' + cpfCnpjCondition);
		}
		
		return [
			SELECT 	CpfCnpj__c,
					CreatedDate,
					MajorDealer__c,
					MajorDealerCode__c,
					OptionalDealer__c,
					OptionalDealerCode__c,
					ProductInterest__c,
					ProductCode__c,
					AccountLead__c
			
			FROM 	Lead
			WHERE 	CpfCnpj__c in :cpfCnpjSet
		];
	}
	
	public static List<Lead> getLeadsByCpfCnpjInLastDays(Set<String> cpfCnpjSet, Integer lastDays){
		return [
			SELECT 	CpfCnpj__c,
					CreatedDate,
					MajorDealer__c,
					MajorDealerCode__c,
					OptionalDealer__c,
					OptionalDealerCode__c,
					ProductInterest__c,
					ProductCode__c,
					AccountLead__c
			
			FROM 	Lead
			WHERE 	CpfCnpj__c in :cpfCnpjSet
			AND 	CreatedDate >= :Datetime.now().addDays(-lastDays)
		];
	}
	
	
	public static List<Lead> getAllFields(String whereCondition){
		Map<String, Schema.SObjectType> globalMap = Schema.getGlobalDescribe();
		Map<String, Schema.SObjectField> fieldMap = globalMap.get('Lead').getDescribe().fields.getMap();
		
		String query = 'SELECT ';
		
		for(String fieldName : fieldMap.keySet())
            query += fieldName + ',';
        
        query = query.substring(0, query.length()-1) + ' FROM Lead ' + whereCondition;
        
        System.debug('@@@ LeadDAO getAllFields query: ' + query);
        
        return Database.query(query);
	}
	
    public static Map<Id,String> getOwnerType(List<Lead> leadList){
        Map<Id,String> ownerType = new Map<Id,String>();
        for(Lead lead : [select Owner.type, Id from Lead where Id in: leadList]){
            ownerType.put(lead.Id, lead.Owner.type);
        }
        return ownerType;
    }
    
}