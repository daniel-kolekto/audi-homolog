@isTest
private class LeadDAOTest {
	
	@isTest static void shouldQueryLead() {
		Test.startTest();
		
		LeadDAO.getLeadsById(new Set<Id>{null}, false);
		LeadDAO.getLeadsByCpfCnpj(new Set<String>{Utils.generateCpf()}, true);
		LeadDAO.getLeadsByCpfCnpj(new Set<String>{Utils.generateCpf()}, false);
		LeadDAO.getLeadsByCpfCnpjInLastDays(new Set<String>{Utils.generateCpf()}, 10);
		LeadDAO.getLeadsByCpfCnpjInLastDays(new Set<String>{Utils.generateCpf()}, 10);
		
		Test.stopTest();
	}
	
}