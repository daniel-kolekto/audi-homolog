public class LeadUtils {
	
	public static Id getBusinessHoursIdTreated(){
		Id businessHoursId;
		
		try {
			businessHoursId = Utils.getDefaultBusinessHoursId();
			
		} catch(Exception e){
			System.debug('LeadUtils exception - Não foi possível definir o Business Hours: ' + e.getMessage());
		}
		
		return businessHoursId;
	}
    
    public static Boolean isLeadClosing(Lead leadNew, Lead leadOld){
        return leadNew.Status != leadOld.Status &&
            new Set<String>{'ClosedWon', 'ClosedLost', 'AutomaticallyLost', 'InvalidData'} .contains(leadNew.Status) &&
            leadNew.LeadClosingDate__c == null;
    }
	
    public static Account createAccountForLead(Lead lead) {
        Account leadAccount = new Account(
            CpfCnpj__c = lead.CpfCnpj__c,
            Type = 'Prospect',
            BillingStreet = lead.Street,
            BillingPostalCode = lead.PostalCode,
            BillingCity = lead.City,
            BillingState = lead.State,
            BillingCountry = lead.Country,
            AnnualRevenue = lead.AnnualRevenue,
            Description = lead.Description,
            Fax = lead.Fax,
            Industry = lead.Industry,
            //OwnerId = lead.OwnerId,
            NumberOfEmployees = lead.NumberOfEmployees,
            Phone = lead.Phone,
            Rating = lead.Rating,
            Website = lead.Website,
            OptinSms__c = lead.OptinSms__c
        );
        
        if( 'LegalPerson'.equalsIgnoreCase(lead.LeadType__c) || 'RuralProducer'.equalsIgnoreCase(lead.LeadType__c) || 'Fleet'.equalsIgnoreCase(lead.LeadType__c) ) {
            leadAccount.RecordTypeId = Utils.getRecordTypeId('Account', 'LegalPerson');
            leadAccount.Name = lead.Company;
            
        } else { // NaturalPerson - Diplomat - PCD
            leadAccount.RecordTypeId = Utils.getRecordTypeId('Account', 'NaturalPerson');
            leadAccount.FirstName = lead.FirstName;
            leadAccount.LastName = lead.LastName;
            leadAccount.PersonDoNotCall = lead.DoNotCall;
            leadAccount.PersonEmail = lead.Email;
            leadAccount.PersonHasOptedOutOfEmail = lead.HasOptedOutOfEmail;
            leadAccount.PersonHasOptedOutOfFax = lead.HasOptedOutOfFax;
            leadAccount.PersonLeadSource = lead.LeadSource;
            leadAccount.PersonMobilePhone = lead.MobilePhone;
            leadAccount.PersonTitle = lead.Title;
            leadAccount.PersonBirthdate = lead.Birthdate__c;
        }
        
        return leadAccount;
    }
    
    public static Contact createContactForLegalPersonAccountLead(Lead lead){
    	return new Contact(
    		AccountId = lead.AccountLead__c,
    		FirstName = lead.FirstName,
    		LastName = lead.LastName,
    		MobilePhone = lead.MobilePhone,
    		Phone = lead.Phone,
    		Birthdate = lead.Birthdate__c,
    		Email = lead.Email
    	);
    }
    
    public static Lead cloneLead(Lead leadToClone, String majorDealer, String optDealer){
		return new Lead(
	        RecordTypeId = leadToClone.RecordTypeId,
	        LeadType__c = leadToClone.LeadType__c,
	        LeadSource = leadToClone.LeadSource,
	        SubLeadSource__c = leadToClone.SubLeadSource__c,
	        Rating = leadToClone.Rating,
	        FirstName = leadToClone.FirstName,
	        LastName = leadToClone.LastName,
	        Salutation = leadToClone.Salutation,
	        CpfCnpj__c = leadToClone.CpfCnpj__c,
	        Birthdate__c = leadToClone.Birthdate__c,
	        Company = leadToClone.Company,
	        Email = leadToClone.Email,
	        Phone = leadToClone.Phone,
	        MobilePhone = leadToClone.MobilePhone,
	        OptinEmail__c = leadToClone.OptinEmail__c,
	        OptinPhone__c = leadToClone.OptinPhone__c,
	        PreferredContact__c = leadToClone.PreferredContact__c,
	        Street = leadToClone.Street,
	        PostalCode = leadToClone.PostalCode,
	        City = leadToClone.City,
	        State = leadToClone.State,
	        Country = leadToClone.Country,
	        Interest__c = leadToClone.Interest__c,
	        MajorDealerCode__c = majorDealer,
	        OptionalDealerCode__c = optDealer,
	        ProductCode__c = leadToClone.ProductCode__c,
	        ModelWeb__c = leadToClone.ModelWeb__c,
	        ModelVersionWeb__c = leadToClone.ModelVersionWeb__c
		);
	}
	
}