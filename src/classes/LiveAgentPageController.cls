public with sharing class LiveAgentPageController {

    private static final String ENDPOINT = W2CParams__c.getInstance( 'PRD' ).URLW2C__c;

    public Id orgId{
        get{
            return W2CParams__c.getInstance( 'PRD' ).Org_Id__c;
        }
    }
    public Id recordTypeId{
        get{
            return W2CParams__c.getInstance( 'PRD' ).RecordTypeId__c;
        }
    }

    public String serializedForm{get;set;}

    public LiveAgentPageController() {
    }

    public void submitForm(){
        HttpRequest request = new HttpRequest();

        request.setEndpoint(LiveAgentPageController.ENDPOINT);
        request.setMethod('POST');
        request.setBody(this.serializedForm);

        HttpResponse response = new Http().send(request);
        System.debug(response);
    }
    
    public String emailUser {get{
        return UserInfo.getUserEmail();
    }}
    public String userId{
    	get{return UserInfo.getUserId();}
    }

    public String userName{
    	get{return UserInfo.getFirstName() +' '+ UserInfo.getLastName();}
    }

    public String idUserToURL{
    	get{return UserInfo.getUserId();}
    }

    public String userAccount{
    	get{
	      	User usuario =  [SELECT ContactId 
	      					FROM User 
	      					WHERE id =: UserInfo.getUserId()];

	        if(usuario.ContactId != null){
	            Contact contato = 	[SELECT AccountId 
	            					FROM Contact 
	            					WHERE id =: usuario.ContactId];

	            if(contato != null){
	                return 	[SELECT Name 
	                		FROM Account 
	                		WHERE id =: contato.AccountId].get(0).Name;
	            }
	        }
	        return '';
	    }
    }

    public static boolean supportUser{ 
        get{
            System.debug('@@@ CAC_SupportCaseController.supportUser - BEGIN');

            boolean x = false;

            User usuario =  [SELECT ContactId 
            				FROM User 
            				WHERE id =: UserInfo.getUserId()];
            if(usuario.ContactId != null){
                Contact contato = 	[SELECT AccountId 
                					FROM Contact 
                					WHERE id =: usuario.ContactId];
                if(contato!=null){
                    x = [SELECT Name 
                    	FROM Account 
                    	WHERE id =: contato.AccountId] != null;
                }
            }

            System.debug('@@@ CAC_SupportCaseController.supportUser - FINISH ' + x);
            
            return x;
    	}
    }
    
}