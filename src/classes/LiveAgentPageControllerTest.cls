@isTest
private class LiveAgentPageControllerTest {

	@TestSetup public static void setup(){
		W2CParams__c setting = new W2CParams__c();
		setting.Name = 'PRD';
		setting.Org_ID__c = '00Dj0000001mv3u';
		setting.RecordTypeID__c = '012j0000000mFUz';
		setting.URLW2C__c = 'https://login.salesforce.com/servlet/servlet.WebToCase?encoding=UTF-8';
		Database.insert(setting);
	}
	
	@isTest static void shouldCheckSettedValues() {
		User usuario = new User(
		    profileId = Utils.getSystemAdminProfileId(),
		    username = 'teste@kolekto.com',
		    email = 'teste@kolekto.com',
		    emailencodingkey = 'UTF-8',
		    localesidkey = 'en_US',
		    languagelocalekey = 'en_US',
		    timezonesidkey = 'America/Los_Angeles',
		    alias='newTeste',
		    lastname='lastNTeste'
		);
		Database.insert(usuario);

		System.runAs(usuario){
			LiveAgentPageController instance = new LiveAgentPageController();
			System.assertEquals('00Dj0000001mv3u', instance.orgId);
			System.assertEquals('012j0000000mFUz', instance.recordTypeId);
			System.assertEquals(UserInfo.getUserEmail(), instance.emailUser);
			System.assertEquals(UserInfo.getUserId(), instance.userId);
			System.assertEquals((UserInfo.getFirstName() +' '+ UserInfo.getLastName()), instance.userName);
			System.assertEquals(UserInfo.getUserId(), instance.idUserToURL);
		}
	}

	@isTest static void shouldGetUserContact() {
		Set<String> customerUserTypes = new Set<String> {'CSPLiteUser', 'PowerPartner', 'PowerCustomerSuccess',   'CustomerSuccess'};
		Account acc = new Account (
		    Name = 'contaTeste'
		);  
		Database.insert(acc);
		Contact contato = new Contact (
		    AccountId = acc.id,
		    LastName = 'contactUser'
		);
		Database.insert(contato);

		Profile perfil = [select Id,name from Profile where UserType in :customerUserTypes limit 1];

		User usuario = new User(
		    profileId = perfil.id,
		    username = 'teste@kolekto.com',
		    email = 'teste@kolekto.com',
		    emailencodingkey = 'UTF-8',
		    localesidkey = 'en_US',
		    languagelocalekey = 'en_US',
		    timezonesidkey = 'America/Los_Angeles',
		    alias='newTeste',
		    lastname='lastNTeste',
		    contactId = contato.id
		);
		Database.insert(usuario);

		System.runAs(usuario){
			System.assertEquals(true, LiveAgentPageController.supportUser);
		}
	}
	
	@isTest static void shouldGetContactAccount() {
		Set<String> customerUserTypes = new Set<String> {'CSPLiteUser', 'PowerPartner', 'PowerCustomerSuccess',   'CustomerSuccess'};
		Account acc = new Account (
		    Name = 'contaTeste'
		);  
		Database.insert(acc);
		Contact contato = new Contact (
		    AccountId = acc.id,
		    LastName = 'contactUser'
		);
		Database.insert(contato);

		Profile perfil = [select Id,name from Profile where UserType in :customerUserTypes limit 1];

		User usuario = new User(
		    profileId = perfil.id,
		    username = 'teste@kolekto.com',
		    email = 'teste@kolekto.com',
		    emailencodingkey = 'UTF-8',
		    localesidkey = 'en_US',
		    languagelocalekey = 'en_US',
		    timezonesidkey = 'America/Los_Angeles',
		    alias='newTeste',
		    lastname='lastNTeste',
		    contactId = contato.id
		);
		Database.insert(usuario);

		System.runAs(usuario){
			LiveAgentPageController instance = new LiveAgentPageController();
			System.assertEquals('contaTeste', instance.userAccount);
		}
	}

	@isTest static void shouldMakeCallout() {
		Test.setMock(HttpCalloutMock.class, new LiveAgentTestMock());
		LiveAgentPageController instance = new LiveAgentPageController();
		instance.serializedForm = 'Teste';
		Test.startTest();
		instance.submitForm();
		Test.stopTest();
	}
	
}