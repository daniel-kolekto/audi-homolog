public without sharing class OwnershipBO {
	
	public static void ownershipDuplicateRule(List<Ownership__c> triggerNew){
        Set<String> ownershipCnpjCnpjSet = new Set<String>();
        Set<String> ownershipVinSet = new Set<String>();
        
        for(Ownership__c os : triggerNew) {
            if(String.isNotEmpty(os.CustomerCpfCnpj__c))
                ownershipCnpjCnpjSet.add(os.CustomerCpfCnpj__c);
            if(String.isNotEmpty(os.VehicleVin__c))
                ownershipVinSet.add(os.VehicleVin__c);
        }
        
        Set<String> duplicateSet = new Set<String>();
        for(Ownership__c os : [
            SELECT CustomerCpfCnpj__c, VehicleVin__c
            FROM Ownership__c
            WHERE CustomerCpfCnpj__c in :ownershipCnpjCnpjSet
            AND VehicleVin__c in :ownershipVinSet
        ]) {
            duplicateSet.add( (os.CustomerCpfCnpj__c + os.VehicleVin__c).trim() );
        }
        
        for(Ownership__c os : triggerNew) {
            if(duplicateSet.contains( (os.CustomerCpfCnpj__c + os.VehicleVin__c).trim() ))
                os.addError(Label.DR_DuplicateOwnership);
        }
	}
	
}