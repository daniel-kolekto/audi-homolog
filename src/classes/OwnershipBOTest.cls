@isTest
private class OwnershipBOTest {
	
	@testSetup static void createTestData(){
        Account dealerAcct01 = AccountBuild.createDealer('9055');
        Account dealerAcct02 = AccountBuild.createDealer('9066');
        Account customer01 = AccountBuild.createCustomer('PF', '66830433614');
        Database.insert(new List<Account>{dealerAcct01, dealerAcct02, customer01});
        
        Product2 vehicleProduct = ProductBuild.createVehicle('6FG9RT');
        Database.insert(vehicleProduct);
        
        Vehicle__c vehicle = VehicleBuild.createVehicle('1GTDC14N2FJ506808', 'EMT3194', vehicleProduct.Id);
        Database.insert(vehicle);
        
        Database.insert(new Ownership__c(Name = 'test', Vehicle__c = vehicle.Id, Customer__c = customer01.Id, Status__c = 'ACTIVE'));
    }
	
	
	@isTest static void shouldNotCreateOwnership() {
		Account customerTest = AccountDAO.getAccountByCpfCnpj('66830433614', true);
        System.assert(customerTest != null);
        Vehicle__c vehicleTest = VehicleDAO.getVehicleByChassi('1GTDC14N2FJ506808');
        System.assert(vehicleTest != null);
        
        String errorMsg;
        
        Test.startTest();
        
        try {
        	Database.insert(new Ownership__c(Name = 'test', Vehicle__c = vehicleTest.Id, Customer__c = customerTest.Id, Status__c = 'ACTIVE'));
        	
        } catch(Exception e){
        	errorMsg = e.getMessage();
        }
        
        Test.stopTest();
        
        System.assert(errorMsg.contains(Label.DR_DuplicateOwnership));
	}
	
}