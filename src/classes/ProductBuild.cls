@isTest
public class ProductBuild {
	
	public static final String PRODUCT_DEFAULT_NAME = 'A1 Sportback Attraction 1.4 TFSI S tronic';
	public static final String PRODUCT_DEFAULT_CODE = '8XFAJG';
	public static final String PRODUCT_DEFAULT_YEAR = '16/17';
	public static final String PRODUCT_DEFAULT_VERSION = '(WFM)';
	
	
	public static Product2 createVehicle(String prodCode){
		return getNewVehicle('Vehicle', PRODUCT_DEFAULT_NAME, prodCode, PRODUCT_DEFAULT_YEAR, PRODUCT_DEFAULT_VERSION);
	}
	
	public static Product2 createVehicle(){
		return getNewVehicle('Vehicle', PRODUCT_DEFAULT_NAME, PRODUCT_DEFAULT_CODE, PRODUCT_DEFAULT_YEAR, PRODUCT_DEFAULT_VERSION);
	}
	
	public static Product2 createService(){
		return getNewVehicle('Service', '', '', '', '');
	}
	
	
	public static Product2 getNewVehicle(String recordType, String name, String prodCode, String modelYear, String version){
		return new Product2(
			RecordTypeId = Utils.getRecordTypeId('Product2', recordType),
			Name = name,
			ProductCode = prodCode,
			Family = 'None',
			ModelYear__c = modelYear,
			Version__c = version,
			IsActive = true
		);
	}
	
}