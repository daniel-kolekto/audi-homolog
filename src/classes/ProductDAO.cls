public class ProductDAO {
	
	/* Maps */
	public static Map<String, Product2> getProductMapByCode(Set<String> prodCodeSet){
		Map<String, Product2> mapProduct = new Map<String, Product2>();
		
		for(Product2 prod : getProductByCode(prodCodeSet)){
			mapProduct.put(prod.ProductCode, prod);
		}
		
		return mapProduct;
	}
	
	public static Map<Id, Product2> getProductMapById(Set<Id> prodIdSet){
		return new Map<Id, Product2>(getProductById(prodIdSet));
	}
	
	
	/* Lists */
	
	public static List<Product2> getProductByCode(Set<String> prodCodeSet){
		return [
			SELECT 	IsActive,
					ProductCode,
					Description,
					Name,
					RecordTypeId,
					ModelYear__c,
					Version__c
			
			FROM 	Product2
			WHERE 	ProductCode in :prodCodeSet
		];
	}
	
	public static Product2 getProductByCode(String prodCode){
		List<Product2> listProd =  [
			SELECT 	IsActive,
					ProductCode,
					Description,
					Name,
					RecordTypeId,
					ModelYear__c,
					Version__c
			
			FROM 	Product2
			WHERE 	ProductCode = :prodCode
		];
		
		return !listProd.isEmpty() ? listProd[0] : null;
	}
	
	public static List<Product2> getProductById(Set<Id> prodIdSet){
		return [
			SELECT 	IsActive,
					ProductCode,
					Description,
					Name,
					RecordTypeId,
					ModelYear__c,
					Version__c
			
			FROM 	Product2
			WHERE 	Id in :prodIdSet
		];
	}
	
	public static List<Product2> getAllProduct(){
		return [
			SELECT	IsActive,
					ProductCode,
					Description,
					Name,
					RecordTypeId,
					ModelYear__c,
					Version__c
					
			FROM	Product2
			WHERE 	IsActive = true
			order by Name ASC
			limit 200
		];
	}
	
}