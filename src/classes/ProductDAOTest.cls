@isTest
private class ProductDAOTest {
	
	@isTest static void shouldQueryProduct() {
		Test.startTest();
		
		ProductDAO.getProductMapByCode(new Set<String>{'90F90X'});
		ProductDAO.getProductMapById(new Set<Id>{null});
		ProductDAO.getProductByCode('90F90X');
		ProductDAO.getAllProduct();
		
		Test.stopTest();
	}
	
}