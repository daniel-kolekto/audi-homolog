public class SFMC_MobileConnect {
	private static String accessToken;
	private static String client_id;
	private static String client_secret;
	private static String messageId;


	public static String sendSMS(String phone, String message){
		setConfiguration();
		String messageLogin = login();

		if(!messageLogin.equals('Success'))
			return messageLogin;

		String sendMessage = sendMessage(phone,message);

		if(sendMessage.contains('Success'))
			return Label.SMS_SuccessMessage;

		return sendMessage;
	}


	

	private static void setConfiguration(){
		MarketingCloudConfiguration__c marketingCloudSettings = MarketingCloudConfiguration__c.getAll().get('default');
		messageId = marketingCloudSettings.MessageId__c;
		client_id = marketingCloudSettings.ClienteId__c;
		client_secret = marketingCloudSettings.ClientSecret__c;
	}

	private static String login(){
        try{
         	accessToken = '';
            HttpRequest req = new HttpRequest();
            HttpResponse res = new HttpResponse();
            Http http = new Http();
            
            req.setEndpoint('https://auth.exacttargetapis.com/v1/requestToken');
            req.setMethod('POST');
            req.setHeader('Content-Type', 'application/json');
           
            req.setBody('{"clientId": "' + client_id + '","clientSecret": "' + client_secret + '"}');
            
            String bodyResp = '';
            if(!Test.isRunningTest()){
                res = http.send(req);
                bodyResp = res.getBody();
            } else {
                bodyResp = '{"accessToken":"1HFDIfkGElNuAtsakNOpEZvb","expiresIn":3479}';
            }
            System.debug('@@@ login bodyResp: \n\n' + bodyResp + '\n\n');
            
            if(!bodyResp.contains('accessToken'))
            	return bodyResp;

            JSONParser parser = JSON.createParser(bodyResp);
            
            while (parser.nextToken() != JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                    
                    String text = parser.getText();
                    
                    parser.nextToken();
                    if (text == 'accessToken') {
                        accessToken = parser.getText();
                        System.debug('accessToken&&' + accessToken);
                        return 'Success';
                        break;
                    } else {
                        System.debug(LoggingLevel.WARN, 'JsonClassifier consuming unrecognized property: '+text);
                        
                    }
                }
            }
            
        } catch(Exception e){
           return e.getMessage();
        }   
       return 'Fail';
    }
    
    @testVisible
	private static String sendMessage(String phone, String message){
		String urlRequest = 'https://www.exacttargetapis.com/sms/v1/messageContact/'+messageId+'/send';

		String body = '';
		body = '{ '; 
    	body += ' "mobileNumbers": [';
    	body += ' "'+ phone +'" ';
    	body += '],';
    	body += '"Subscribe": true,';
    	body += '"Resubscribe": true,';
    	body += '"keyword": "AUDI",'; 
    	body += '"Override": true,';
   		body += ' "messageText": "'+message+'" ';
        body += ' }';

        HttpRequest req 	= new HttpRequest();
        HTTPResponse res 	= new HTTPResponse();
        req.setEndPoint(urlRequest);
        req.setHeader('Content-Type', 'application/json');
        req.setHeader('Authorization', 'Bearer ' + accessToken);
        
        req.setMethod('POST');
        req.setCompressed(true);
        req.setBody(body);
        
        try{	
        	Http http = new Http();
            String bodyResp = '';
            if(!Test.isRunningTest()){
                res = http.send(req);
                bodyResp = res.getBody();
            } else {
                bodyResp = '{"tokenId":"UVVadlByaEQza0NIdHU0c1hVRkpXUTo3Njox"}';
            }
            System.debug('@@@ sendMessage bodyResp: \n\n' + bodyResp + '\n\n');
            if(bodyResp.contains('tokenId')){
        	   return 'Success';
            }else{
                return bodyResp;
            }
    	}catch(Exception e){
            return e.getMessage();
    	}
        

        return 'Fail';
	}

}