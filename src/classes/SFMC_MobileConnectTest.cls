@isTest
private class SFMC_MobileConnectTest {
	
	@testSetup static void createTestData(){
		Database.insert(new MarketingCloudConfiguration__c(
			Name = 'default',
			ClienteId__c = 't6thssn7p8uk0dzs9s82jrp6',
			ClientSecret__c = 'Wpqp8Ra5aXAOaVyjAyHghSaS',
			MessageId__c = 'Mjo3ODow'
		));
	}
	
	
	@isTest static void testMobileConnect01() {
		Test.startTest();
		
		SFMC_MobileConnect.sendSMS('11955550000', 'Ola cliente');
		
		Test.stopTest();
	}
	
	@isTest static void testMobileConnect02() {
		Test.startTest();
		
		SFMC_MobileConnect.sendMessage('11955550000', 'Ola cliente');
		
		Test.stopTest();
	}
	
}