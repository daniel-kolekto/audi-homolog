global class SLACalculationBatch implements Database.Batchable<sObject>, Schedulable {
	
	global void execute(SchedulableContext SC){
		Database.executeBatch(this, 100);
	}
	
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator([
			SELECT 	RecordTypeId,
					CpfCnpj__c,
					Status,
					CreatedDate,
			        LeadForwardingDate__c,
			        SlaNurturingTotalTime__c,
			        SlaDealerTotalTime__c,
			        LeadForwarded__c,
			        LeadAutomaticallyForwarded__c
					
			FROM 	Lead
			WHERE 	Status not in ('ClosedWon', 'ClosedLost', 'AutomaticallyLost', 'InvalidData')
		]);
	}
	
    global void execute(Database.BatchableContext BC, List<Lead> scope) {
    	System.debug('@@@ SLACalculationBatch scope @@@\n' + JSON.serializePretty(scope));
    	
    	Id businessHoursId = LeadUtils.getBusinessHoursIdTreated();
    	List<Lead> listLeadUpsert = new List<Lead>();
    	
    	
        for(Lead lead : scope){
        	if(businessHoursId != null){
        		
        		if(lead.LeadForwardingDate__c == null){
        			lead.SlaNurturingTotalTime__c = BusinessHours.diff(businessHoursId, lead.CreatedDate, Datetime.now()) / 1000 / 60;
        			
        		} else {
        			lead.SlaNurturingTotalTime__c = BusinessHours.diff(businessHoursId, lead.CreatedDate, lead.LeadForwardingDate__c) / 1000 / 60;
        			lead.SlaDealerTotalTime__c = BusinessHours.diff(businessHoursId, lead.LeadForwardingDate__c, Datetime.now()) / 1000 / 60;
        		}
        		
        		listLeadUpsert.add(lead);
        	}
		}
        
        System.debug('@@@ SLACalculationBatch listLeadUpsert: \n' + JSON.serializePretty(listLeadUpsert));
        
        if(!listLeadUpsert.isEmpty())
        	Database.upsert(listLeadUpsert);
    }
	
	global void finish(Database.BatchableContext BC) {
		Datetime fiveAhead = Datetime.now().addMinutes(5);
		
		String minute = String.valueOf( fiveAhead.minute() );
		String hour = String.valueOf( fiveAhead.hour() );
		String day = String.valueOf( fiveAhead.day() );
		String month = String.valueOf( fiveAhead.month() );
		String year = String.valueOf( fiveAhead.year() );
		
		String jobDay = day.length() == 1 ? '0'+day : day;
		String jobMonth = month.length() == 1 ? '0'+month : month;
		String jobHour = hour.length() == 1 ? '0'+hour : hour;
		String jobMinute = minute.length() == 1 ? '0'+minute : minute;
		
        String scheduleTime = '0 '+minute+' '+hour+' '+day+' '+month+' ? '+year;
        String jobName = 'Atualizar SLA ' + jobDay+'/'+jobMonth+'/'+year+' - '+jobHour+':'+jobMinute;
        
        System.debug('@@@ SLACalculationBatch next jobName: ' + jobName);
        System.debug('@@@ SLACalculationBatch next scheduleTime: ' + scheduleTime);
        
        if(!Test.isRunningTest()) System.schedule(jobName, scheduleTime, new SLACalculationBatch());
	}
	
}