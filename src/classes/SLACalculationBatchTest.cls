@isTest
private class SLACalculationBatchTest {
	
	@testSetup static void createTestData(){
		Account dealerAcct01 = AccountBuild.createDealer('9055');
		Account dealerAcct02 = AccountBuild.createDealer('9066');
		Database.insert(new List<Account>{dealerAcct01, dealerAcct02});
		
		Product2 vehicleProduct = ProductBuild.createVehicle('6FG9RT');
		Database.insert(vehicleProduct);
	}
	
	
	@isTest static void shouldCalculateNurturingTotalTime() {
		Account leadDealer01 = AccountDAO.getDealerByCode('9055');
		String leadCpf = Utils.generateCpf();
		
		Lead leadWeb = LeadBuild.createLead(leadCpf, '9055', '9066', '6FG9RT');
		Database.insert(leadWeb);
		
		Test.setCreatedDate(leadWeb.Id, Datetime.now().addDays(-2));
		
		Test.startTest();
		
		Database.executeBatch(new SLACalculationBatch(), 100);
		
		Test.stopTest();
		
		List<Lead> leadList = LeadDAO.getLeadsByCpfCnpj(new Set<String>{leadCpf}, true);
		System.assert(leadList.size() == 1);
		Lead leadAssert = leadList[0];
		
		System.assert(leadAssert.SlaNurturingTotalTime__c != null && leadAssert.SlaDealerTotalTime__c == null);
	}
	
	@isTest static void shouldCalculateDealerTotalTime() {
		Account leadDealer01 = AccountDAO.getDealerByCode('9055');
		String leadCpf = Utils.generateCpf();
		
		Lead leadWeb = LeadBuild.createLead(leadCpf, '9055', '9066', '6FG9RT');
		Database.insert(leadWeb);
		
		Test.setCreatedDate(leadWeb.Id, Datetime.now().addDays(-3));
		leadWeb.LeadForwardingDate__c = Datetime.now().addDays(-2);
		Database.update(leadWeb);
		
		Test.startTest();
		
		Database.executeBatch(new SLACalculationBatch(), 100);
		
		Test.stopTest();
		
		List<Lead> leadList = LeadDAO.getLeadsByCpfCnpj(new Set<String>{leadCpf}, true);
		System.assert(leadList.size() == 1);
		Lead leadAssert = leadList[0];
		
		System.assert(leadAssert.SlaNurturingTotalTime__c != null && leadAssert.SlaDealerTotalTime__c != null);
	}
	
	@isTest static void shouldScheduleBatch() {
		Test.startTest();
		
		System.schedule('Test job', '20 30 8 10 2 ?', new SLACalculationBatch());
		
		Test.stopTest();
	}
	
}