public class SetUtils {
	
    
	// LEAD METHODS
	
    
	public static Set<String> getLeadCpfCnpjSet(List<Lead> leadList){
        Set<String> leadCpfCnpjSet = new Set<String>();
        
        for(Lead l : leadList) {
            if(String.isNotEmpty(l.CpfCnpj__c))
                leadCpfCnpjSet.add(l.CpfCnpj__c);
        }
        
        return leadCpfCnpjSet;
    }
    
    public static Set<Id> getLeadCustomerIdSet(List<Lead> leadList){
        Set<Id> leadCustomerIdSet = new Set<Id>();
        
        for(Lead l : leadList) {
            if(l.AccountLead__c != null)
                leadCustomerIdSet.add(l.AccountLead__c);
        }
        
        return leadCustomerIdSet;
    }
    
    public static Set<String> getLeadDealerCodeSet(List<Lead> leadList){
        Set<String> leadDealerCodeSet = new Set<String>();
        
        for(Lead l : leadList) {
            if(String.isNotEmpty(l.MajorDealerCode__c))
                leadDealerCodeSet.add(l.MajorDealerCode__c);
            if(String.isNotEmpty(l.OptionalDealerCode__c))
                leadDealerCodeSet.add(l.OptionalDealerCode__c);
        }
        
        return leadDealerCodeSet;
    }
    
    public static Set<Id> getLeadDealerIdSet(List<Lead> leadList){
        Set<Id> leadDealerIdSet = new Set<Id>();
        
        for(Lead l : leadList) {
            if(l.MajorDealer__c != null)
                leadDealerIdSet.add(l.MajorDealer__c);
            if(l.OptionalDealer__c != null)
                leadDealerIdSet.add(l.OptionalDealer__c);
            if(l.ScheduledDealer__c != null)
                leadDealerIdSet.add(l.ScheduledDealer__c);
        }
        
        return leadDealerIdSet;
    }
    
    public static Set<String> getLeadProductCodeSet(List<Lead> leadList){
        Set<String> leadProductCodeSet = new Set<String>();
        
        for(Lead l : leadList) {
            if(String.isNotEmpty(l.ProductCode__c))
                leadProductCodeSet.add(l.ProductCode__c);
        }
        
        return leadProductCodeSet;
    }
    
    public static Set<Id> getLeadProductIdSet(List<Lead> leadList){
        Set<Id> leadProductIdSet = new Set<Id>();
        
        for(Lead l : leadList) {
            if(l.ProductInterest__c != null)
                leadProductIdSet.add(l.ProductInterest__c);
        }
        
        return leadProductIdSet;
    }
    
    
    
    
    // CASE METHODS
    
    
    public static Set<Id> getCaseIdSet(List<Case> caseList){
        Set<Id> caseIdSet = new Set<Id>();
        
        for(Case c : caseList) {
            caseIdSet.add(c.Id);
        }
        
        return caseIdSet;
    }
    
    public static Set<Id> getCaseUserOwnerIdSet(List<Case> caseList){
        Set<Id> caseUserOwnerIdSet = new Set<Id>();
        
        for(Case c : caseList) {
            if(c.OwnerId.getSObjectType() == Schema.User.SObjectType)
                caseUserOwnerIdSet.add(c.OwnerId);
        }
        
        return caseUserOwnerIdSet;
    }
    
    public static Set<Id> getCaseQueueOwnerIdSet(List<Case> caseList){
        Set<Id> caseQueueOwnerIdSet = new Set<Id>();
        
        for(Case c : caseList) {
            if(c.OwnerId.getSObjectType() == Schema.Group.SObjectType)
                caseQueueOwnerIdSet.add(c.OwnerId);
        }
        
        return caseQueueOwnerIdSet;
    }
    
    public static Set<Id> getCaseDealerIdSet(List<Case> caseList){
        Set<Id> caseDealerIdSet = new Set<Id>();
        
        for(Case c : caseList) {
            if(c.Dealer__c != null)
                caseDealerIdSet.add(c.Dealer__c);
        }
        
        return caseDealerIdSet;
    }
    
    public static Set<Id> getCaseCustomerIdSet(List<Case> caseList){
        Set<Id> caseCustomerIdSet = new Set<Id>();
        
        for(Case c : caseList) {
            if(c.AccountId != null)
                caseCustomerIdSet.add(c.AccountId);
        }
        
        return caseCustomerIdSet;
    }
    
    public static Set<Id> getCaseVehicleIdSet(List<Case> caseList){
        Set<Id> caseVehicleIdSet = new Set<Id>();
        
        for(Case c : caseList) {
            if(c.Vehicle__c != null)
                caseVehicleIdSet.add(c.Vehicle__c);
        }
        
        return caseVehicleIdSet;
    }
    
    public static Set<String> getCaseCpfCnpjSet(List<Case> caseList){
        Set<String> caseCpfCnpjSet = new Set<String>();
        
        for(Case c : caseList) {
            if(String.isNotEmpty(c.CustomerCpfCnpj__c))
                caseCpfCnpjSet.add(c.CustomerCpfCnpj__c);
        }
        
        return caseCpfCnpjSet;
    }
    
    
    
    
    // CASE COMMENT METHODS
    
    
    public static Set<Id> getCaseCommentCreatedByIdSet(List<CaseComment> caseCommentList){
        Set<Id> caseCommentCreatedByIdSet = new Set<Id>();
        
        for(CaseComment cc : caseCommentList) {
            caseCommentCreatedByIdSet.add(cc.CreatedById);
        }
        
        return caseCommentCreatedByIdSet;
    }
    
    /*public static Set<Id> getCaseCommentCaseIdSet(List<CaseComment> caseCommentList){
        Set<Id> caseCommentCaseIdSet = new Set<Id>();
        
        for(CaseComment cc : caseCommentList) {
            caseCommentCaseIdSet.add(cc.ParentId);
        }
        
        return caseCommentCaseIdSet;
    }*/
    
    
    
    
    // ACCOUNT METHODS
    
    
    public static Set<Id> getAccountPersonContactIdSet(List<Account> acctList){
        Set<Id> accountPersonContactSet = new Set<Id>();
        
        for(Account ac : acctList) {
            if(Utils.isPersonRecordType(ac.RecordTypeId) && ac.PersonContactId != null)
                accountPersonContactSet.add(ac.PersonContactId);
        }
        
        return accountPersonContactSet;
    }
    
    public static Set<Id> getAccountLegalPersonIdSet(List<Account> acctList){
        Set<Id> accountIdSet = new Set<Id>();
        
        for(Account ac : acctList) {
            if(!Utils.isPersonRecordType(ac.RecordTypeId))
                accountIdSet.add(ac.Id);
        }
        
        return accountIdSet;
    }
    
    
    
    
    // TASK METHODS
    
    
    public static Set<Id> getTaskOwnerIdSet(List<Task> taskList){
        Set<Id> taskOwnerIdSet = new Set<Id>();
        
        for(Task t : taskList) {
            taskOwnerIdSet.add(t.OwnerId);
        }
        
        return taskOwnerIdSet;
    }
    
    public static Set<Id> getTaskCreatedByIdSet(List<Task> taskList){
        Set<Id> taskCreatedByIdSet = new Set<Id>();
        
        for(Task t : taskList) {
            taskCreatedByIdSet.add(t.CreatedById);
        }
        
        return taskCreatedByIdSet;
    }
	
}