public without sharing class TaskBO {
	
	public static void notifyAnalystTaskClosed(List<Task> triggerNew, Map<Id, Task> oldMap){
		Set<Id> createdByAndOwnerIdSet = new Set<Id>();
		createdByAndOwnerIdSet.addAll(SetUtils.getTaskOwnerIdSet(triggerNew));
		createdByAndOwnerIdSet.addAll(SetUtils.getTaskCreatedByIdSet(triggerNew));
		
		Map<Id, User> mapUser = UserDAO.getUserMapById(createdByAndOwnerIdSet);
		List<Case> caseListUpdate = new List<Case>();
		
        for(Task taskNew : triggerNew) {
            try {
                Task taskOld = oldMap.get(taskNew.Id);
                Boolean isCase = taskNew.WhatId.getSObjectType() == Schema.Case.SObjectType;
                Boolean isOwnerDealer = mapUser.get(taskNew.OwnerId).ProfileId == Utils.getProfileId('Audi Dealer');
                Boolean isCreatedByAnalyst = mapUser.get(taskNew.CreatedById).ProfileId == Utils.getProfileId(Label.CRAProfileName);
                Boolean isCompleted = taskNew.Status != taskOld.Status && 'Completed'.equalsIgnoreCase(taskNew.Status);
                
                if(isCase && isOwnerDealer && isCreatedByAnalyst && isCompleted) {
                    caseListUpdate.add(new Case(
                        Id = taskNew.WhatId,
                        NotifyTaskClosedToAnalyst__c = true
                    ));
                }
                
            } catch(Exception e) {
                System.debug('@@@ TaskBO.notifyAnalystTaskClosed exception on task ' + taskNew.Id + 
                	'\nError: ' + e.getMessage() + ' in line: ' + e.getLineNumber());
            }
        }
		
		if(!caseListUpdate.isEmpty())
			Database.update(caseListUpdate);
	}
	
}