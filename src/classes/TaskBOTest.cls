@isTest
private class TaskBOTest {
	
	@testSetup static void createTestData(){
        System.runAs(new User(Id = UserInfo.getUserId())) {
            User analystUser = UserBuild.createAnalystCRA('54321');
            Database.insert(analystUser);
            
            try {
                Database.insert(new PermissionSetAssignment(
                    AssigneeId = analystUser.Id,
                    PermissionSetId = Utils.getPermissionSetId('CRA_Analyst')
                ));
            } catch(Exception e) {
                System.debug('Could not assign permission set: ' + e.getMessage());
            }
            
            Database.insert(new List<GroupMember> {
                new GroupMember(
                    UserOrGroupId = analystUser.Id,
                    GroupId = Utils.getQueueId('AudiCustomerCare')
                ), new GroupMember(
                    UserOrGroupId = analystUser.Id,
                    GroupId = Utils.getQueueId('AudiAG')
                ), new GroupMember(
                    UserOrGroupId = analystUser.Id,
                    GroupId = Utils.getQueueId('ReclameAqui')
                )
            });
            
            
            CollaborationGroup chatterGroup = new CollaborationGroup(
                Name = 'Group Test Salabim Salaha Itabum Benibu',
                CollaborationType = 'Private'
            );
            Database.insert(chatterGroup);
            
            Database.insert(new CollaborationGroupMember(
                CollaborationGroupId = chatterGroup.Id,
                CollaborationRole = 'Standard',
                MemberId = analystUser.Id
            ));
            
            Database.insert(new InternalAreas__c(
                Name = 'Group Test',
                GroupId__c = chatterGroup.Id
            ));
        }
        
        
        Account dealerAcct01 = AccountBuild.createDealer('9055');
        Account dealerAcct02 = AccountBuild.createDealer('9066');
        Account customer01 = AccountBuild.createCustomer('PF');
        Database.insert(new List<Account>{dealerAcct01, dealerAcct02, customer01});
        
        Product2 vehicleProduct = ProductBuild.createVehicle('6FG9RT');
        Database.insert(vehicleProduct);
        
        Vehicle__c vehicle = VehicleBuild.createVehicle('1GTDC14N2FJ506808', 'EMT3194', vehicleProduct.Id);
        Database.insert(vehicle);
        
        Database.insert(new Ownership__c(Name = 'test', Vehicle__c = vehicle.Id, Customer__c = customer01.Id, Status__c = 'ACTIVE'));
        
        
        Case case01 = CaseBuild.createCustomerCareCase();
        case01.AccountId = customer01.Id;
        case01.Dealer__c = dealerAcct01.Id;
        case01.Vehicle__c = vehicle.Id;
        
        Case case02 = CaseBuild.createAudiAgCase();
        case02.AccountId = customer01.Id;
        case02.Dealer__c = dealerAcct01.Id;
        case02.Vehicle__c = vehicle.Id;
        
        Case case03 = CaseBuild.createReclameAquiCase();
        case03.AccountId = customer01.Id;
        case03.Dealer__c = dealerAcct01.Id;
        case03.Vehicle__c = vehicle.Id;
        
        Database.insert(new List<Case>{case01, case02, case03});
    }
    
	
    @isTest static void shouldNotifyAnalystTaskClosed() {
    	User dealerUser = [SELECT Id FROM User WHERE IsActive = true AND ProfileId = :Utils.getProfileId('Audi Dealer') LIMIT 1];
    	//User analystUser = [SELECT Id FROM User WHERE IsActive = true AND ProfileId = :Utils.getProfileId(Label.CRAProfileName) LIMIT 1];
    	User analystUser = [SELECT Id FROM User WHERE Username = 'usuario54321@audi.com.teste'];
        
    	List<Case> lstCase = [
			SELECT Id, CaseCommentBody__c, NotifyTaskClosedToAnalyst__c
			FROM Case
			WHERE RecordTypeId = :Utils.getRecordTypeId('Case', 'CustomerCare')
		];
		Case caseTest = !lstCase.isEmpty() ? lstCase.get(0) : null;
		System.assert(caseTest != null);
		Task caseTsk;
		
        System.runAs(analystUser) {
            caseTsk = new Task(
            	OwnerId = dealerUser.Id,
                Subject = 'Solicitação para a concessionária',
                Priority = 'Normal',
                Status = 'Open',
                WhatId = caseTest.Id
            );
            Database.insert(caseTsk);
        }
        
		System.assert(!caseTest.NotifyTaskClosedToAnalyst__c);
		
		Test.startTest();
		
		Task tskUpdate = [SELECT Status FROM Task WHERE Id = :caseTsk.Id];
		tskUpdate.Status = 'Completed';
		Database.update(tskUpdate);
		
		Test.stopTest();
		
		Case caseAssert = CaseDAO.getCaseById(caseTest.Id, true);
		SYstem.assert(caseAssert != null);
		
		System.assert(!caseAssert.NotifyTaskClosedToAnalyst__c);
    }
	
}