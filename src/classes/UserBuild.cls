@isTest
public class UserBuild {
	
	public static final String RANDOM_NUMBER = Utils.randomNumberAsString(5);
	public static final String USER_DEFAULT_FIRSTNAME = 'Usuario';
	public static final String USER_DEFAULT_LASTNAME = 'Teste' + RANDOM_NUMBER;
	public static final String USER_DEFAULT_PROFILEID = Utils.getProfileId('Audi CRM');
	//public static final String USER_DEFAULT_ROLEID = createDefaultRole;
	public static final String USER_DEFAULT_EMAIL = 'usuario'+RANDOM_NUMBER+'@audi.com';
	public static final String USER_DEFAULT_USERNAME = 'usuario'+RANDOM_NUMBER+'@audi.com.teste';
	
	
	public static User createUser(){
        return new User(
            FirstName = USER_DEFAULT_FIRSTNAME,
            LastName =  USER_DEFAULT_LASTNAME,
            ProfileId = USER_DEFAULT_PROFILEID,
            //UserRoleId = USER_DEFAULT_ROLEID,
            Email = USER_DEFAULT_EMAIL,
            Username = USER_DEFAULT_USERNAME,
            Alias = RANDOM_NUMBER,
            CommunityNickname = RANDOM_NUMBER,
            EmailEncodingKey='ISO-8859-1',
            LanguageLocaleKey='pt_BR',
            LocaleSidKey='pt_BR',
            TimeZoneSidKey='America/Sao_Paulo'
        );
	}
    
    public static User createAnalystCRA(String randomNumber){
        return new User(
            FirstName = 'Analyst',
            LastName =  'Test'+randomNumber,
            ProfileId = Utils.getProfileId(Label.CRAProfileName),
            //UserRoleId = USER_DEFAULT_ROLEID,
            Email = 'analyst'+randomNumber+'@audi.com',
            Username = 'usuario'+randomNumber+'@audi.com.teste',
            Alias = randomNumber,
            CommunityNickname = randomNumber,
            EmailEncodingKey='ISO-8859-1',
            LanguageLocaleKey='pt_BR',
            LocaleSidKey='pt_BR',
            TimeZoneSidKey='America/Sao_Paulo'
        );
    }
	
	
	/*public static getNewUser(String firstName, String lastName, Id profileId, Id roleId, String email, String userName, String alias, String nickName){
		return new User(
            FirstName = firstName,
            LastName =  lastName,
            ProfileId = profileId,
            UserRoleId = roleId,
            Email = email,
            Username = userName,
            Alias = alias,
            CommunityNickname = nickName,
            EmailEncodingKey='ISO-8859-1',
            LanguageLocaleKey='pt_BR',
            LocaleSidKey='pt_BR',
            TimeZoneSidKey='America/Sao_Paulo'
        );
	}*/
	
	/*public static Id createDefaultRole(){
        UserRole dealerRole = new UserRole(
            Name = 'Dealer Account',
            PortalType = 'None',
            ParentRoleId = groupRole.Id
        );
	}*/
	
}