public class UserDAO {
	
	public static Map<Id, User> getUserMapById(Set<Id> userIdSet){
		return new Map<Id, User>(getUserById(userIdSet));
	}
	
	
	public static List<User> getUserById(Set<Id> userIdSet){
		return [
            SELECT 	FirstName,
            		LastName,
            		ProfileId,
            		Profile.Name
            		
            FROM 	User
            WHERE 	Id in :userIdSet
        ];
	}
	
}