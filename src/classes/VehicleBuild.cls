@isTest
public class VehicleBuild {
	
	public static Vehicle__c createVehicle(String vin, String plate, Id productId){
		return new Vehicle__c(
			Name = vin,
			LicensePlate__c = plate,
			Product__c = productId,
			ManufactureYear__c = '2016',
			ModelYear__c = '2016',
			Color__c = 'Preto',
			CurrentKM__c = 10000
		);
	}
	
}