public class VehicleDAO {
	
	/* Maps */
	
	public static Map<Id, Vehicle__c> getVehiclesMapById(Set<Id> vehicIdSet){
		return new Map<Id, Vehicle__c>( getVehiclesById(vehicIdSet, false) );
	}
	
	public static Map<String, Vehicle__c> getVehiclesMapByChassi(Set<String> chassiSet){
		Map<String, Vehicle__c> vehicMap = new Map<String, Vehicle__c>();
		
		for(Vehicle__c vehic : getVehiclesByChassi(chassiSet, false)){
            if(String.isNotEmpty(vehic.Vin__c))
                vehicMap.put(vehic.Vin__c, vehic);
            else
            	vehicMap.put(vehic.Name, vehic);
		}
		
		return vehicMap;
	}
	
	public static Map<String, Vehicle__c> getVehiclesMapByPlate(Set<String> plateSet){
		Map<String, Vehicle__c> vehicMap = new Map<String, Vehicle__c>();
		
		for(Vehicle__c vehic : getVehiclesByPlate(plateSet, false)){
			vehicMap.put(vehic.LicensePlate__c, vehic);
		}
		
		return vehicMap;
	}
	
	
	/* Lists */
	
	public static List<Vehicle__c> getVehiclesById(Set<Id> vehicIdSet, Boolean allFields){
		if(vehicIdSet == null || vehicIdSet.isEmpty()) return new List<Vehicle__c>();
		
        if(allFields) {
            return getAllFields('WHERE Id in ' + Utils.buildIdSetAsStringForQuery(vehicIdSet));
            
        } else {
            return [
                SELECT 	Name,
		                LicensePlate__c,
		                Vin__c,
		                Product__c,
		                Model__c
                
                FROM 	Vehicle__c
                WHERE 	Id in :vehicIdSet
            ];
        }
	}
	
	public static List<Vehicle__c> getVehiclesByChassi(Set<String> chassiSet, Boolean allFields){
		if(chassiSet == null || chassiSet.isEmpty()) return new List<Vehicle__c>();
		
        if(allFields) {
            String chassis = Utils.buildStringSetAsStringForQuery(chassiSet);
            return getAllFields('WHERE Vin__c in ' + chassis + ' OR Name in ' + chassis);
            
        } else {
            return [
                SELECT 	Name,
		                LicensePlate__c,
		                Vin__c,
		                Product__c,
		                Model__c
                
                FROM 	Vehicle__c
                WHERE 	Vin__c in :chassiSet OR Name in :chassiSet
            ];
        }
	}
	
	public static List<Vehicle__c> getVehiclesByPlate(Set<String> plateSet, Boolean allFields){
		if(plateSet == null || plateSet.isEmpty()) return new List<Vehicle__c>();
		
        if(allFields) {
            return getAllFields('WHERE LicensePlate__c in ' + Utils.buildStringSetAsStringForQuery(plateSet));
            
        } else {
            return [
                SELECT 	Name,
		                LicensePlate__c,
		                Vin__c,
		                Product__c,
		                Model__c
                
                FROM 	Vehicle__c
                WHERE	LicensePlate__c in :plateSet
            ];
        }
	}
	
	
	/* Single Records */
	
	public static Vehicle__c getVehicleById(Id vehicId){
		if(String.isEmpty(vehicId)) return null;
		List<Vehicle__c> lst = getVehiclesById(new Set<Id>{vehicId}, true);
		try {
			return lst.get(0);
		} catch(Exception e){
			System.debug('VehicleDAO.getVehicleById - Could not retrieve Vehicle__c: ' + e.getMessage());
			return null;
		}
	}
	
	public static Vehicle__c getVehicleByChassi(String chassi){
		if(String.isEmpty(chassi)) return null;
		List<Vehicle__c> lst = getVehiclesByChassi(new Set<String>{chassi}, true);
		try {
			return lst.get(0);
		} catch(Exception e){
			System.debug('VehicleDAO.getVehicleByChassi - Could not retrieve Vehicle__c: ' + e.getMessage());
			return null;
		}
	}
	
	public static Vehicle__c getVehicleByPlate(String licensePlate){
		if(String.isEmpty(licensePlate)) return null;
		List<Vehicle__c> lst = getVehiclesByPlate(new Set<String>{licensePlate}, true);
		try {
			return lst.get(0);
		} catch(Exception e){
			System.debug('VehicleDAO.getVehicleByPlate - Could not retrieve Vehicle__c: ' + e.getMessage());
			return null;
		}
	}
	
	
	/* Get all fields */
	
	public static List<Vehicle__c> getAllFields(String whereCondition){
        Map<String, Schema.SObjectType> globalMap = Schema.getGlobalDescribe();
        Map<String, Schema.SObjectField> fieldMap = globalMap.get('Vehicle__c').getDescribe().fields.getMap();
        
        String query = 'SELECT ';
        
       for(String fieldName : fieldMap.keySet())
            query += fieldName + ',';
        
        query = query.substring(0, query.length()-1) + ' FROM Vehicle__c ' + whereCondition;
        
        System.debug('@@@ getAllFields query: ' + query);
        
        return Database.query(query);
    }
	
}