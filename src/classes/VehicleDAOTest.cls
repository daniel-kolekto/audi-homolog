@isTest
private class VehicleDAOTest {
	
	@testSetup static void createTestData(){
        Account dealerAcct01 = AccountBuild.createDealer('9055');
        Account dealerAcct02 = AccountBuild.createDealer('9066');
        Account customer01 = AccountBuild.createCustomer('PF');
        Database.insert(new List<Account>{dealerAcct01, dealerAcct02, customer01});
        
        Product2 vehicleProduct = ProductBuild.createVehicle('6FG9RT');
        Database.insert(vehicleProduct);
        
        Vehicle__c vehicle01 = VehicleBuild.createVehicle('1GTDC14N2FJ506808', 'EMT3194', vehicleProduct.Id);
        Vehicle__c vehicle02 = VehicleBuild.createVehicle('2D8GP44L45R405783', 'EEX5230', vehicleProduct.Id);
        Database.insert(new List<Vehicle__c>{vehicle01, vehicle02});
        
        Database.insert(new List<Ownership__c>{
            new Ownership__c(Name = 'test1', Vehicle__c = vehicle01.Id, Customer__c = customer01.Id, Status__c = 'ACTIVE'),
            new Ownership__c(Name = 'test2', Vehicle__c = vehicle02.Id, Customer__c = customer01.Id, Status__c = 'ACTIVE')
        });
    }
    
	
	@isTest static void test_method_one() {
		Test.startTest();
		
		Vehicle__c vehicle01 = VehicleDAO.getVehicleByChassi('1GTDC14N2FJ506808');
		Vehicle__c vehicle02 = VehicleDAO.getVehicleByPlate('EEX5230');
		
		System.assert(vehicle01 != null);
		System.assert(vehicle02 != null);
		
		Vehicle__c vehicleTest = VehicleDAO.getVehicleById(vehicle01.Id);
		
		VehicleDAO.getVehiclesMapById(new Set<Id>{vehicle01.Id, vehicle02.Id});
		VehicleDAO.getVehiclesMapByChassi(new Set<String>{'1GTDC14N2FJ506808', 'EMT3194'});
		VehicleDAO.getVehiclesMapByPlate(new Set<String>{'1GTDC14N2FJ506808', 'EEX5230'});
		
		Test.stopTest();
	}
	
}