<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Contact postponed one</label>
    <protected>false</protected>
    <values>
        <field>SLAFullGreen__c</field>
        <value xsi:type="xsd:double">240.0</value>
    </values>
    <values>
        <field>SLAFullYellow__c</field>
        <value xsi:type="xsd:double">720.0</value>
    </values>
    <values>
        <field>SLAHalfGreen__c</field>
        <value xsi:type="xsd:double">480.0</value>
    </values>
    <values>
        <field>SLAHalfYellow__c</field>
        <value xsi:type="xsd:double">960.0</value>
    </values>
</CustomMetadata>
