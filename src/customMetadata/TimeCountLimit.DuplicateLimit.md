<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>DuplicateLimit</label>
    <protected>false</protected>
    <values>
        <field>Description__c</field>
        <value xsi:type="xsd:string">Número de dias definidos da data de criação, para considerar como duplicado um Lead que entrar com o mesmo CPF/CNPJ e concessionária preferida</value>
    </values>
    <values>
        <field>HoursOfDay__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>NumberOfDays__c</field>
        <value xsi:type="xsd:double">40.0</value>
    </values>
    <values>
        <field>SlaLeadLimit01__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>SlaLeadLimit02__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>SlaLeadLimit03__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>SlaLeadLimit04__c</field>
        <value xsi:nil="true"/>
    </values>
</CustomMetadata>
