<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>SLA Limit Dealer</label>
    <protected>false</protected>
    <values>
        <field>Description__c</field>
        <value xsi:type="xsd:string">Definição dos tempos de SLA da concessionária a partir do momento em que o Lead for encaminhado a ela</value>
    </values>
    <values>
        <field>HoursOfDay__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>NumberOfDays__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>SlaLeadLimit01__c</field>
        <value xsi:type="xsd:double">60.0</value>
    </values>
    <values>
        <field>SlaLeadLimit02__c</field>
        <value xsi:type="xsd:double">90.0</value>
    </values>
    <values>
        <field>SlaLeadLimit03__c</field>
        <value xsi:type="xsd:double">120.0</value>
    </values>
    <values>
        <field>SlaLeadLimit04__c</field>
        <value xsi:type="xsd:double">180.0</value>
    </values>
</CustomMetadata>
