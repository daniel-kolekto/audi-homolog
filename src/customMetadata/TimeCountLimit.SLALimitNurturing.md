<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>SLA Limit Nurturing</label>
    <protected>false</protected>
    <values>
        <field>Description__c</field>
        <value xsi:type="xsd:string">Definição dos tempos de SLA do Dev Partners a partir do momento em que o Lead for gerado</value>
    </values>
    <values>
        <field>HoursOfDay__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>NumberOfDays__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>SlaLeadLimit01__c</field>
        <value xsi:type="xsd:double">20.0</value>
    </values>
    <values>
        <field>SlaLeadLimit02__c</field>
        <value xsi:type="xsd:double">40.0</value>
    </values>
    <values>
        <field>SlaLeadLimit03__c</field>
        <value xsi:type="xsd:double">60.0</value>
    </values>
    <values>
        <field>SlaLeadLimit04__c</field>
        <value xsi:type="xsd:double">90.0</value>
    </values>
</CustomMetadata>
