trigger AccountTrigger on Account (before insert, before update) {
    
    for(Account acct : Trigger.new) {
        if(acct.RecordTypeId == Utils.getRecordTypeId('Account', 'NaturalPerson')) {
            if(String.isEmpty(acct.PersonEmail))
                acct.PersonEmail.addError(Label.VR_RequiredEmail);
                
            if(String.isNotEmpty(acct.PersonMobilePhone) && (!acct.PersonMobilePhone.isNumeric() || acct.PersonMobilePhone.length() < 11))
                acct.PersonMobilePhone.addError(Label.VR_InvalidMobilePhone);
        }
    }

}