trigger CaseCommentTrigger on CaseComment (after insert) {
    
    CaseCommentBO.notifyAnalystCaseComment(Trigger.new);
    
}