trigger CaseTrigger on Case (before insert, before update, after insert, after update) {
	
	if(Trigger.isBefore && (Trigger.isInsert || Trigger.isUpdate)){
		CaseBO.setCaseContactAndSLA(Trigger.new);
		CaseBO.calculateKundentischTime(Trigger.new, (Trigger.isUpdate ? Trigger.oldMap : null));
		
		if(Trigger.isUpdate){
			CaseBO.closeMilestones(Trigger.new, Trigger.oldMap);
			CaseBO.syncContactEmailsForAlert(Trigger.new, Trigger.oldMap);
			CS_EmailAlertSchedule.scheduleCaseQueue(Trigger.new);
		}
	}
	
	if(Trigger.isAfter && (Trigger.isInsert || Trigger.isUpdate)){
		CaseBO.cartaEncerramentoTask(Trigger.new, (Trigger.isUpdate ? Trigger.oldMap : null));
	}
	
}