trigger LeadTrigger on Lead (before insert, before update, after insert, after update) {
	
	if(!Trigger.isDelete) System.debug('@@@ LeadTrigger Trigger.new INICIO @@@\n' + JSON.serializePretty(Trigger.new));

    if(Trigger.isBefore && (Trigger.isInsert || Trigger.isUpdate)) {
        
        LeadBO.setLeadAccountsAndProduct(Trigger.new);
        
        if(Trigger.isInsert){
        	LeadBO.leadInsertTreatment(Trigger.new);
            System.debug('Trigger.isInser');
            LeadBO.setSLAFields(Trigger.new, null);
        }
        
        if(Trigger.isUpdate){
        	LeadBO.leadUpdateTreatment(Trigger.new, Trigger.oldMap);
            LeadBO.setSLAFields(Trigger.new, Trigger.oldMap);
            LeadBO.setChagendFieldsSLADate(Trigger.new, Trigger.oldMap);
        }
        
    }
    
    if(!Trigger.isDelete) System.debug('@@@ LeadTrigger Trigger.new FIM @@@\n' + JSON.serializePretty(Trigger.new));
    
}