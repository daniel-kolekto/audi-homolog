trigger OwnershipTrigger on Ownership__c (before insert, before update) {
	
	OwnershipBO.ownershipDuplicateRule(Trigger.new);
	
}