<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>SendEmailApprovalAlert</fullName>
        <description>SendEmailApprovalAlert</description>
        <protected>false</protected>
        <recipients>
            <recipient>Coordination</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>Supervision</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>audi@kolekto.com.br</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Alertas/CourtesyApprovalRequest</template>
    </alerts>
    <fieldUpdates>
        <fullName>UpdateStatusApprovedCourtesy</fullName>
        <field>Status__c</field>
        <literalValue>Approved</literalValue>
        <name>UpdateStatusApprovedCourtesy</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateStatusPendingCourtesy</fullName>
        <field>Status__c</field>
        <literalValue>Pending</literalValue>
        <name>UpdateStatusPendingCourtesy</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateStatusRejectedCourtesy</fullName>
        <field>Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>UpdateStatusRejectedCourtesy</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
