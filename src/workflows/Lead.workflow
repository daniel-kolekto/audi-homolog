<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>AlertaLeadNaoContatado</fullName>
        <description>Alerta para leads com status de &quot;nao contatado&quot;</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Modelo_Lead_Nao_Contatado</template>
    </alerts>
    <alerts>
        <fullName>AlertaLeadNaoContatadoInformacao</fullName>
        <description>Alerta para leads com status de &quot;nao contatado&quot; e interesse = informação</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>AudiInterno/Modelo_Lead_Nao_Contatado_Informacao</template>
    </alerts>
    <alerts>
        <fullName>DuplicateLeadEmailLeadManager</fullName>
        <description>Duplicate Lead Email to Lead Manager</description>
        <protected>false</protected>
        <recipients>
            <field>LeadManagerEmail__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>audi@kolekto.com.br</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>AudiInterno/DuplicateLeadDealer</template>
    </alerts>
</Workflow>
